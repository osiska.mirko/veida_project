<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/* 
Este controlador maneja el sistema de logueo y ABM de usuarios, interactuando con la tabla de
usuarios estrictamente utilizada por la librería Authme pero también interactuando con la tabla
de personas que contiene información de usuarios (dueños y empleados) y clientes.
*/

class Auth extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		
		$this->load->library('authme');
		$this->load->helper('authme');
		$this->config->load('authme');
		$this->load->helper('url');
		
	}
	
	//index, lleva a la página por defecto, auth/login
	public function index()
	{
		if(!logged_in()) redirect('auth/login');
		redirect('auth/dash');
	}

	//login, loguea a usuarios de tipo empleado y también dueños; en un futuro también podrían ser los clientes
	public function login()
	{
	
		if(logged_in()) redirect('auth/dash');
		 
		$this->load->library('form_validation');
		$this->load->helper('form');
		$data['error'] = false;
		 
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email');
		$this->form_validation->set_rules('password', 'Password', 'required');
		
		if($this->form_validation->run()){
			if($this->authme->login(set_value('email'), set_value('password'))){
				// Redirect to your logged in landing page here
				redirect('auth/dash');
			} else {
				$data['error'] = 'El email y/o la contraseña ingresados no son válidos. Intente nuevamente.';
			}
		}

		//$data['content'] = '/auth/index.html';
		$this->load->view('/auth/newLogin.php',$data);
		//$this->load->view('/includes/template_login',$data);
	}

	public function logout()
	{
		if(!logged_in()) redirect('auth/login');

		$this->authme->logout('/');
	}

	public function dash()
	{
		if(!logged_in()) redirect('auth/login');
		
		redirect('Welcome');
	}

	public function mail_recuperacion_enviado()
	{	
		$data['content'] = '/auth/mail_recuperacion_enviado';
		$this->load->view('/includes/template_login',$data);
	}

	public function ventana_de_clave_recuperada()
	{	
		$data['content'] = '/auth/ventana_de_clave_recuperada';
		$this->load->view('/includes/template_login',$data);
	}
	
	//forgot, se utiliza para el cambio de contraseñas vía "olvidé mi contraseña" usando como smtp_user una cuenta de gmail
	public function forgot()
	{
		if(logged_in()) redirect('auth/dash');
		 
		$this->load->library('form_validation');
		$this->load->helper('form');
		$data['success'] = false;
		 
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email|callback_email_exists');
		
		if($this->form_validation->run()){

			$config = array(
		     'protocol' => 'smtp',
		     'smtp_host' => 'smtp.googlemail.com',
		     'smtp_user' => 'distribuidora.veida.sistema@gmail.com', //Su Correo de Gmail Aqui
		     'smtp_pass' => '36835714', // Su Password de Gmail aqui
		     'smtp_port' => '465',
		     'smtp_crypto' => 'ssl',
		     'mailtype' => 'html',
		     'wordwrap' => TRUE,
		     'charset' => 'utf-8'
		     );

			$email = $this->input->post('email');
			$this->load->model('Authme_model');
			$user = $this->Authme_model->get_user_by_email($email);
			$slug = md5($user->id_usuario . $user->email . date('Ymd'));

			$this->load->library('email', $config);
			$this->email->set_newline("\r\n");
			$this->email->from('DISTRI-VEIDA-SYSTEM');
			$this->email->subject('Recuperar contraseña en DISTRIBUIDORA VEIDA');		
			$this->email->message('Para reiniciar tu contraseña para el sistema de DISTRIBUIDORA VEIDA hace click en el siguiente link:
      
				'. site_url('auth/reset/'. $user->id_usuario .'/'. $slug) .'

				.Si no pediste cambiar tu contraseña, si no fuiste vos, simplemente ignora este email y no ocurrirá ningún cambio.

				Nota: tener presente que el link para hacer el cambio de contraseña caduca después del '. date('j M Y') .'.');	
			$this->email->to($email); 
			$this->email->send();
				
			if($this->email->send(FALSE)){
		         echo "enviado<br/>";
		         echo $this->email->print_debugger(array('headers'));
		     }else {
		         echo "fallo <br/>";
		         echo "error: ".$this->email->print_debugger(array('headers'));

			$data['success'] = true;
			}	
		}

		$data['content'] = 'auth/forgot_password';
		$this->load->view('/includes/template_login',$data);
	}

	public function email_exists($email)
	{
		$this->load->model('Authme_model');
		 
		if($this->Authme_model->get_user_by_email($email)){
			return true;
		} else {
			$this->form_validation->set_message('email_exists', 'No pudimos encontrar este email en la base de datos.');
			return false;
		}
	}
	
	public function reset()
	{
		if(logged_in()) redirect('auth/dash');
		 
		$this->load->library('form_validation');
		$this->load->helper('form');
		$data['success'] = false;
		 
		$user_id = $this->uri->segment(3);
		if(!$user_id) show_error('Invalid reset code.');
		$hash = $this->uri->segment(4);
		if(!$hash) show_error('Invalid reset code.');
		
		$this->load->model('Authme_model');
		$user = $this->Authme_model->get_user($user_id);
		if(!$user) show_error('Invalid reset code.');
		$slug = md5($user->id_usuario . $user->email . date('Ymd'));
		if($hash != $slug) show_error('Invalid reset code.');
	 
		$this->form_validation->set_rules('password', 'Password', 'required|min_length['. $this->config->item('authme_password_min_length') .']');
		$this->form_validation->set_rules('password_conf', 'Confirm Password', 'required|matches[password]');
		
		if($this->form_validation->run()){
			$this->authme->reset_password($user->id_usuario, $this->input->post('password'));
			$data['success'] = true;
		}
		
		$data['content'] = 'auth/reset_password';
		$this->load->view('/includes/template_login',$data);
	}

	//cliente_nuevo, sirve para dar de alta un cliente en la tabla personas, no en la tabla users. Si se envían datos como geoubicación, se ignoran los de calle, dirección, número, etc... sino, se ignoran los de geoubicación pero se guardan los otros. En ambos casos , alguno de los dos casos quedará siempre como null (a menos que luego se modifiquen manualmente esos datos porque tal vez se los quiera corregir)
	public function cliente_nuevo()
	{
		if(!logged_in())
		{
			redirect('auth/login');
		}
			 
		$this->load->library('form_validation');
		$this->load->helper('form');
		$data['error'] = '';		
		
		$nombre=$this->input->post('nombre_modal');
		$apellido=$this->input->post('apellido_modal');
		$calle_dir=$this->input->post('calle_dir_modal'); 
		$numero_dir=$this->input->post('num_direccion_modal');   
		
		/*
		$ciudad=$this->input->post('ciudad_modal');   
		$provincia=$this->input->post('provincia_modal');  
		*/
		$provincia = "null";
		$id_localidad = $this->input->post('id_localidad');

		$area_tel=$this->input->post('codigo_tel_modal');
		$tel=$this->input->post('tel_modal');
		$geolat=$this->input->post('lat_modal');   
		$geolong=$this->input->post('long_modal');
		$dni=$this->input->post('dni_modal');
		$tipo_usuario = 3;          
		
		//esta variable llena el nuevo campo en base de datos tabla personas llamado cargado_por
		$cargado_por = user('id_usuario');
		//llama a database en agregar nueva persona y se da de alta una persona, pero no un usuario en la tabla users 
        $this->Authme_model->agregar_nueva_persona($nombre,$apellido,$calle_dir,$numero_dir,$id_localidad,$provincia,$area_tel,$tel,$geolat,$geolong,$dni,$tipo_usuario,$cargado_por);
	    $this->session->set_flashdata('exito_user_nuevo', '<i class="far fa-check-circle"></i> El nuevo cliente se ha cargado en la base de datos.');

	    $user_actual = $this->Authme_model->tipo_usuario_por_id_user(user('id_usuario'));

	    if ($user_actual == 1) 
		{
			redirect('auth/gestionar_clientes_preventista');
		}
		elseif ($user_actual == 0)
		{
			redirect('auth/gestionar_clientes');
		}
		//asi estaba antes, ahora hay dos controladores de gestionar_clientes
        //redirect('auth/gestionar_clientes');	
	}

	//empleado_nuevo, sirve para dar de alta un nuevo empleado de tipo preventista o repartidor (1 o 2 respectivamente. Los usuarios tienen 4 roles, o tipos: 0-admin,1-preventista,2-repartidor,3-cliente). En este caso se tocan ambas tablas, users y personas
	public function empleado_nuevo()
	{
		if(!logged_in())
		{
			redirect('auth/login');
		};

		//chequea que el usuario usando el sistema en esta instancia sea tipo admin = 0
		if($this->Authme_model->tipo_usuario_por_id_user(user('id_usuario')) != 0) 
		{
			redirect(base_url(''));
		};
			 
		$this->load->library('form_validation');
		$this->load->helper('form');
		$data['error'] = '';		
		
		//valid email is unique chequea que en la tabla users el mail no se repita
		$this->form_validation->set_rules('email_modal', 'Email', 'required|valid_email|is_unique['. $this->config->item('authme_users_table') .'.email]');

		if($this->form_validation->run())
		{
			//si se cumple form_validation, se da de alta el mail y contraseña en users
			if($this->authme->signup(set_value('email_modal'), set_value('contra_modal')))
			{	
				$cargado_por = user('id_usuario');
				
				//se recopilan por post nombre, apellido, dni (puede ser null) y rol (puede ser 1 o 2) y se la da de alta en la tabla personas
				$nombre=$this->input->post('nombre_modal');
				$apellido=$this->input->post('apellido_modal');
				$dni=$this->input->post('dni_modal');
				$tipo_usuario = $this->input->post('rol_usuario_modal');
				$this->Authme_model->agregar_nueva_persona($nombre,$apellido,"vacio","0","vacio","vacio",$area_tel,$tel,"","",$dni,$tipo_usuario,$cargado_por);

				//se toma el ultimo id de users y el ultimo id personas y se guarda el id_user con respecto al id_persona recién creado
				$usuario_recien_creado = $this->Authme_model->devolverUltimoID_de_users();
				$id_persona_recien_creada = $this->Authme_model->devolverUltimoID_de_personas();
				$this->Authme_model->actualizar_id_usuario_con_id_persona($id_persona_recien_creada,$usuario_recien_creado);

			    $this->session->set_flashdata('exito_user_nuevo', '<i class="far fa-check-circle"></i> El empleado se ha registrado en el sistema.');
		        redirect('auth/gestionar_empleados');
			};
		}
		else 
		{	
			$this->session->set_flashdata('error', '<i class="fas fa-times"></i> | El email ingresado ya existe en el sistema asociado a un usuario');
			redirect('auth/gestionar_empleados');
		}        	
	}

	public function eliminarUsuario()
	{
		$id_persona = $this->input->post('idPersonaEliminarUsuario');

		if (!empty($id_persona))
		{
			$this->Authme_model->deleteUser($id_persona);
		};

		$user_actual = $this->Authme_model->tipo_usuario_por_id_user(user('id_usuario'));

	    if ($user_actual == 1) 
		{
			redirect('auth/gestionar_clientes_preventista');
		}
		elseif ($user_actual == 0)
		{
			redirect('auth/gestionar_clientes');
		};

	}		

	//gestionar clientes, abre simplemente la página en donde se ven y se tienen las opciones de modificar (y en un futuro se puede agregar eliminar) clientes (personas tipo 3 si fueran users)
	public function gestionar_clientes()
    {	
    	$data['botonMenuActivado'] = '#clientes_menu';

		if(!logged_in())
		{
			redirect('auth/login');
		};

		//solo puede acceder el admin tipo 0
		if($this->Authme_model->tipo_usuario_por_id_user(user('id_usuario')) != 0) 
		{
			redirect(base_url(''));
		};

		$data['mapa_argentina'] = $this->Ventas_model->traer_mapa_argentina();
		$data['content'] = 'auth/gestionar_clientes';
		$data['users'] = $this->authme_model->listar_clientes();
		$data['roles_de_users'] = $this->authme_model->listar_roles_de_usuarios();		
		$this->load->view('/includes/template',$data);
	}

	//igual que gestionar clientes para admin pero con la diferencia de que este es solo para preventistas, se agrego en database, personas, un campo que dice "cargado_por" y cada uno carga, cada cliente cargado por cada preventista es lo que ve cada preventista, se filtran los clientes... y tmb el guardar cliente nuevo se veria modificado para que d eahora en mas siempre se guarde el user logueado en ese campito de cargado_por = user_id actual en sesión (Teoricamente) pero en los admins se ve TODO y en controlador gestionar clientes preventistas se ve todo loque ese preventista cargo
	public function gestionar_clientes_preventista()
    {	
    	$data['botonMenuActivado'] = '#clientes_menu';
    	
		if(!logged_in())
		{
			redirect('auth/login');
		};

		//solo puede acceder el admin tipo 0
		if($this->Authme_model->tipo_usuario_por_id_user(user('id_usuario')) != 1) 
		{
			redirect(base_url(''));
		};

		$id_preventista = user('id_usuario');

		$data['mapa_argentina'] = $this->Ventas_model->traer_mapa_argentina();
		$data['content'] = 'auth/gestionar_clientes';
		$data['users'] = $this->authme_model->listarClientesBy($id_preventista);
		$data['roles_de_users'] = $this->authme_model->listar_roles_de_usuarios();		
		$this->load->view('/includes/template',$data);
	}

	//gestionar empleados, sirve para llevar a la página o vista donde se ven todos los empleados y tmb se puede acceder a 1) modificar sus datos de sesión (nuevo mail y/o contraseña) 2) datos de usuario (tabla personas)
	public function gestionar_empleados()
    {	
    	$data['botonMenuActivado'] = '#empleados_menu';

		if(!logged_in())
		{
			redirect('auth/login');
		};

		//solo puede acceder el admin tipo 0
		if($this->Authme_model->tipo_usuario_por_id_user(user('id_usuario')) != 0) 
		{
			redirect(base_url(''));
		};
		
		$data['content'] = 'auth/gestionar_empleados';
		$data['users'] = $this->authme_model->listar_empleados();
		$data['roles_de_users'] = $this->authme_model->listar_roles_de_usuarios();		
		$this->load->view('/includes/template',$data);
	}

	//cambiar_rol_de_usuario, sirve para, modal mediante, cambiar el rol del empleado. El rol de los usuarios de users está en la tabla de personas
	public function cambiar_rol_de_usuario()
	{
		if(!logged_in())
		{
			redirect('auth/login');
		};
		if($this->Authme_model->tipo_usuario_por_id_user(user('id_usuario')) != 0) 
		{
			redirect(base_url(''));
		};
			 
		$this->load->library('form_validation');
		$this->load->helper('form');
		$data['error'] = '';		
		
		$rol=$this->input->post('rol_usuario_modal');
		$id_persona = $this->input->post('id_persona_modal_rol');        
		
        $this->Authme_model->cambiar_rol($id_persona,$rol);
	    $this->session->set_flashdata('exito_user_nuevo', '<i class="far fa-check-circle"></i> El rol de usuario ha sido actualizado.');
        redirect('auth/gestionar_empleados');	
	}

	//actualizar_datos_de_usuario,actualizar datos de nombres, y todos esos detalles que tienen que ver con la persona, da igual e tipo de usuario. Sirve para actualizar esos datos de todo tipo de usuarios
	public function actualizar_datos_de_usuario()
	{
		if(!logged_in())
		{
			redirect('auth/login');
		};
		if($this->Authme_model->tipo_usuario_por_id_user(user('id_usuario')) != 0) 
		{
			redirect(base_url(''));
		};
			 
		$this->load->library('form_validation');
		$this->load->helper('form');
		$data['error'] = '';		
		
		$id_persona = $this->input->post('id_persona_modal_act');
		$nombre=$this->input->post('nombre_modal_act');
		$apellido = $this->input->post('apellido_modal_act'); 
		$area = $this->input->post('area_modal_act'); 
		$telefono = $this->input->post('tel_modal_act'); 
		$DNI = $this->input->post('DNI_modal_act');   

		$dato = $this->input->post('cliente_o_empleado_dato');     
		
        $this->Authme_model->actualizar_datos($id_persona,$nombre,$apellido,$area,$telefono,$DNI);
	    $this->session->set_flashdata('exito_user_nuevo', '<i class="far fa-check-circle"></i> Los datos del usuario han sido actualizados.');

	    if ($dato == 1)
	    {
	    	redirect('auth/gestionar_clientes');
	    }
        else
        {
        	redirect('auth/gestionar_empleados');
        };	
	}	

	public function actualizar_ubicacion()
	{
		if(!logged_in())
		{
			redirect('auth/login');
		};
		if($this->Authme_model->tipo_usuario_por_id_user(user('id_usuario')) != 0) 
		{
			redirect(base_url(''));
		};
			
		$id_persona = $this->input->post('id_persona');
		
		//$calle=$this->input->post('calle');

		//$altura = $this->input->post('altura');

		$id_localidad = $this->input->post('id_localidad'); 
				
        $this->Authme_model->actualizar_ciudad($id_persona,$id_localidad);
	    $this->session->set_flashdata('exito_user_nuevo', '<i class="far fa-check-circle"></i> Los datos del usuario sobre su ciudad y provincia han sido actualizados correctamente.');

	    redirect('auth/gestionar_clientes');
	}	

	public function actualizar_direccion()
	{
		if(!logged_in())
		{
			redirect('auth/login');
		};
		if($this->Authme_model->tipo_usuario_por_id_user(user('id_usuario')) != 0) 
		{
			redirect(base_url(''));
		};
			
		$id_persona = $this->input->post('id_persona');
		
		$calle=$this->input->post('calle');

		$altura = $this->input->post('altura');

		//$id_localidad = $this->input->post('id_localidad'); 
				
        $this->Authme_model->actualizar_direccion($id_persona,$calle,$altura);
	    $this->session->set_flashdata('exito_user_nuevo', '<i class="far fa-check-circle"></i> Los datos sobre la dirección del cliente se actualizaron de forma exitosa! :) ');

	    redirect('auth/gestionar_clientes');
	}	

	//modificar datos de sesion, sirve para cambiar el usuario y contraseña de una persona. Lo que hace es 1) elimina el usuario (que lo trae a su id por post) 2) crea uno nuevo, 3) lo guarda, actualiza ese id user en la persona.
	public function modificar_datos_de_sesion()
	{
		if(!logged_in())
		{
			redirect('auth/login');
		};
		if($this->Authme_model->tipo_usuario_por_id_user(user('id_usuario')) != 0) 
		{
			redirect(base_url(''));
		};
			 
		$this->load->library('form_validation');
		$this->load->helper('form');
		$data['error'] = '';		
		
		$id_user = $this->input->post('id_user_modal_rol');
		$mail = $this->input->post('email_modal');
		$contra = $this->input->post('contra_modal');

		$id_persona = $this->input->post('id_persona_modal_modificar');

		$this->form_validation->set_rules('email_modal', 'Email', 'required|valid_email|is_unique['. $this->config->item('authme_users_table') .'.email]');

		if($this->form_validation->run())
		{		
			$this->Authme_model->delete_user($id_user);
			$this->authme->signup($mail,$contra);
			$usuario_actual = $this->Authme_model->devolverUltimoID_de_users();
			$this->Authme_model->actualizar_id_usuario_con_id_persona($id_persona,$usuario_actual);
			$this->session->set_flashdata('exito_user_nuevo', '<i class="far fa-check-circle"></i> Los datos de inicio de sesión del empleado se han actualizado.');
		        redirect('auth/gestionar_empleados');			
		}
		else 
		{	
			$this->session->set_flashdata('error', '<i class="fas fa-times"></i> | El email ingresado ya existe en el sistema asociado a un usuario');
			redirect('auth/gestionar_empleados');
		} 	
	}	
}