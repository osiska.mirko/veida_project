<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Categorias_de_productos extends CI_Controller {

	public function __construct()
	{
		parent::__construct();	
	}

	//modal_nueva_categoria, agrega una categoria, es un A del ABM! simple, tomando por posts datos del modal
	public function modal_nueva_categoria()
	{
		$this->load->library('form_validation');
		$this->load->helper('form');
		$data['error'] = '';
		if($this->input->post())
		{
			$this->form_validation->set_error_delimiters('<div class="alert alert-light">', '</div>'); 
			$this->form_validation->set_rules('nombre_catego', 'fecha_alta_catego', 'required');
			
			if($this->form_validation->run())
			{			
				$nombre_catego=$this->input->post('nombre_catego');   
				$fecha_catego=$this->input->post('fecha_alta_catego'); 

		        $this->Categorias_de_productos_model->guardar_categoria($nombre_catego,$fecha_catego);
	        	$this->session->set_flashdata('ok', '<i class="fas fa-check-circle"></i> Categoría agregada con éxito.');
			        redirect(base_url().'Productos');
			}
			else
			{
				$this->session->set_flashdata('error', '<i class="fas fa-times"></i> Error, el campo de la nueva categoría es obligatorio.');
			        redirect(base_url().'Productos');
			}
		}
		$this->load->view('/includes/template',$data);
	}
	
}