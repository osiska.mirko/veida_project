 <?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Productos extends CI_Controller {

	public function __construct()
	{
		parent::__construct();	
	}
	
	//index, el ABM, librería mediante editablegrid, es invocada por este controlador. La libreria simplememente se llama en la vista "productos/editablegrid_veida"
	public function index()
	{
		if(!logged_in())
		{
			redirect('auth/login');
		};
		if($this->Authme_model->tipo_usuario_por_id_user(user('id_usuario')) != 0) 
		{
			redirect(base_url(''));
		};

		$data['botonMenuActivado'] = '#productos_menu';
		$data['content'] = "/productos/editablegrid_veida";
		$this->load->view('/includes/template',$data);
	}

	public function listar_productos()
    {	
    	$data['botonMenuActivado'] = '#listar_productos_menu';

    	//si es user tipo 3= cliente o tipo 2 = repartidor. HOME
		if(!logged_in())
		{
			redirect('auth/login');
		};
		if($this->Authme_model->tipo_usuario_por_id_user(user('id_usuario')) == 3 OR $this->Authme_model->tipo_usuario_por_id_user(user('id_usuario')) ==2) 
		{
			redirect(base_url(''));
		};

		if($this->Authme_model->tipo_usuario_por_id_user(user('id_usuario')) == 1)
		{
			$data['content'] = 'productos/ver_productos';
			$categoria = $this->uri->segment(3);
			$data['productos'] = $this->Productos_model->ver_productos_db($categoria);
			$this->load->view('/includes/template',$data);
		}
		else
		{
			$data['content'] = 'productos/ver_productos';
			$categoria = $this->uri->segment(3);
			$data['productos'] = $this->Productos_model->ver_productos_db($categoria);
			$this->load->view('/includes/template',$data);
		};
	}
	
}