<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ventas extends CI_Controller {
    
	public function __construct()
	{
		parent::__construct();		
	}

	//index, es, por defecto, el lugar para hacer ventas. El usuario debe ser de tipo 1 o 0 (vendedor o admin). Se listan todos los productos con ventas_model listar productos. Si el user es tipo 1 la vista cargada es la mobile, sino es la normal, modo web para pc.
	public function index()
	{	
		$data['botonMenuActivado'] = '#realizar_pedido_menu';

		if(!logged_in())
		{
			redirect('auth/login');
		}
		if($this->Authme_model->tipo_usuario_por_id_user(user('id_usuario')) == 3 OR $this->Authme_model->tipo_usuario_por_id_user(user('id_usuario')) ==2) 
		{
			redirect(base_url(''));
		};

		$productos_lista = $this->Ventas_model->listar_productos();
		$data['productos_lista'] = $productos_lista;

		if($this->Authme_model->tipo_usuario_por_id_user(user('id_usuario')) == 1)
		{
			$data['content'] = '/ventas/realizar_venta_mobile';		
		}
		else
		{	
			$data['content'] = '/ventas/realizar_venta_mobile';		
		};

		$this->load->view('/includes/template',$data);
	}

	//confirmar_venta, luego de index, los productos se seleccionan y se envían por post, los cuales son recibidos en esta función. Detalles comentados por dentro de la función.-
	public function confirmar_venta()
	{	
		$data['botonMenuActivado'] = '#realizar_pedido_menu';

		if(!logged_in())
		{
			redirect('auth/login');
		};
		if($this->Authme_model->tipo_usuario_por_id_user(user('id_usuario')) == 3 OR $this->Authme_model->tipo_usuario_por_id_user(user('id_usuario')) ==2) 
		{
			redirect(base_url(''));
		};

		$this->load->library('form_validation');
		$this->load->helper('form');
		$data['error'] = '';

		//se fija si llegan varios o 1 producto/s, y las cantidades (1 o varias)
		if ($this->input->post('productos[]') && $this->input->post('cantidad'))
		{	
			//se guardan los productos en array
			$ids_productos = $this->input->post('productos[]');
			//se guardan las cantidades de cada producto en array
			$cantidad = $this->input->post('cantidad[]');

			//se declara array para hacer un push mas adelante
		    $data_productos = array();
		    // creo que este está demás: $data_cantidad = array(); si después de julio el sistema funciona igual se puede borrrar

		    //se hace un array push en data productos de todos los detalles de los productos, tomando las caracteristicas de los productos desde DB con Ventas_model->mostrar_producto_por_id y el row que se pasa se los saca del foreach de post('productos[]' que viene en el ids_productos. EN los ids productos se guardan los ids de los productos seleccionados y aqui se guardan esos ids juntos a sus caracteristicas en un array
			foreach ($ids_productos as $row) 
		    {
		        array_push($data_productos, $this->Ventas_model->mostrar_producto_por_id($row));
		    };

		    //la cantidad se pasa a la siguiente vista
		    $data['cantidad'] = $cantidad;
		    //los productos se pasan a la siguiente vista mediante el array
		    $data['productos'] = $data_productos;
		    //los clientes se pasan a la vista
		    $data['lista_clientes'] = $this->Authme_model->listar_clientes();
		    //pasamos ciudades y provincias, para obtener id_localidad
		    $data['mapa_argentina'] = $this->Ventas_model->traer_mapa_argentina();

		    //se da de alta una nueva venta, $id_venta tiene el valor de la tabla ventas y tmb en la tabla carritos de ventas se realizan actualizaciones (Todo detallado en el modelo en generar_nueva_venta)
			$id_venta = $this->Ventas_model->generar_venta_nueva($ids_productos,$cantidad);
			//se pasa a la siguiente vista tmb el id_venta actual
			$data['id_venta'] = $id_venta;
			//comprobar venta es para chequear y dar el sí a la venta, confirmar que es correcta
			$data['content'] = '/ventas/comprobar_venta';

			//se chequea si la vista será mobile o no segun tipo user
			if($this->Authme_model->tipo_usuario_por_id_user(user('id_usuario')) == 1)
			{
				$data['content'] = '/ventas/comprobar_venta_mobile';		
			}
			else
			{	
				$data['content'] = '/ventas/comprobar_venta';		
			};

			$this->load->view('/includes/template',$data);
		}
		else
		{	
			//si no se seleccionaron productos, da "error"
			$this->session->set_flashdata('mensaje_ventas', '<i class="fas fa-window-close"></i> Ningún producto se ha seleccionado');
			redirect(base_url().'ventas');
		}
	}

	//pagar, es la función que define que un carrito de ventas es concretamente una venta. Descripciones de distintas líneas en la función.-
	public function pagar()
	{	
		$data['botonMenuActivado'] = '#realizar_pedido_menu';

		if(!logged_in())
		{
			redirect('auth/login');
		};
		if($this->Authme_model->tipo_usuario_por_id_user(user('id_usuario')) == 3 OR $this->Authme_model->tipo_usuario_por_id_user(user('id_usuario')) ==2) 
		{
			redirect(base_url(''));
		};

		$this->load->library('form_validation');
		$this->load->helper('form');
		$data['error'] = '';

		//trae por input post el id de la vista anterior
		$id_venta = $this->input->post('id_venta');

		//primero se chequea si existe STOCK de esos productos y cantidades en cuestión. SINO, rechaza toda la venta. 
		if ($this->Ventas_model->chequear_stock_de_un_pedido($id_venta))
		{		
			//Si es correcto la existencia de stock en todos los productos, se procede a dar de alta el cliente nuevo o bien tomar el cliente que ya se seleccionó.
			//en función de si el cliente se carga por 1) geoposición o 2) datos de dirección de forma manual, se guardan o unas u otras (se guardan unas y las demas null)
			$cod_area = $this->input->post('cod_area_tel_cliente');
			$telefono = $this->input->post('tel_cliente');
			$nombre = $this->input->post('nombre_cliente');
			$apellido = $this->input->post('apellido_cliente');
			//el dni puede demorar la transacción, es algo que se puede agregar luego
			$dni = 0;

			//datos geoposición para google maps
			$latitud = $this->input->post('geo_lat_cliente');
			$longitud = $this->input->post('geo_long_cliente');

			//o bien, datos geográficos de forma manual
			$calle = $this->input->post('calle_dir_modal');
			$numero = $this->input->post('num_direccion_modal');
			$ciudad = $this->input->post('id_localidad');
			
			//provincia se deja de usar en base de datos y solo se guarda a partir de ahora el id de ciudad
			$provincia = "sin_uso"; 

			//a continuación se ve que de cada campo llega siempre algo, imposible que llegue vacío, pero ya sea que elija una u otra opción, desde la vista se presetean y llenan esos campos que no se usan
			if ( ( (!empty($latitud)) && (!empty($longitud)) && (!empty($apellido)) && (!empty($nombre)) && (!empty($cod_area)) && (!empty($telefono)) && (!empty($ciudad)) ) OR ( (!empty($calle)) && (!empty($numero)) && (!empty($ciudad)) && (!empty($apellido)) && (!empty($nombre)) && (!empty($cod_area)) && (!empty($telefono)) ) )
			{	
				//se da de alta al nuevo cliente tipo 3
				$cargado_por = user('id_usuario');
				$this->Authme_model->agregar_nueva_persona($nombre,$apellido,$calle,$numero,$ciudad,$provincia,$cod_area,$telefono,$latitud,$longitud,$dni,"3",$cargado_por);
				//se trae el id_comprador porque se lo guarda en la venta ya se que fue creado como recién
				$id_comprador = $this->Authme_model->devolverUltimoID_de_personas();
	    		$this->session->set_flashdata('exito_user_nuevo', '<i class="far fa-check-circle"></i> El nuevo cliente se ha cargado en la base de datos.');
			}
			else
			{	
				//se trae el id_comprador porque se lo guarda en la venta, o ya sea que fue seleccionado porque ya estaba creado (Se trae por post)
				$id_comprador = $this->input->post('clientes_lista');
			}

			//se calcula precio final a guardar en tabla ventas
			$precio_final = $this->Ventas_model->sumar_precio_final($id_venta);
			//se guarda el precio final, el id de la venta q se trajo por post, el usuario que hizo la venta y el comprador
			$this->Ventas_model->actualizar_estado_pago($id_venta,$precio_final,user('id_usuario'),$id_comprador);

			//venta exitosa y se invoca nuevamente a la vista de nueva venta, con la lista de productos
			//la vista puede ser mobile o no segun user type
			$this->session->set_flashdata('exito_venta', '<i class="far fa-check-circle"></i> La venta se ha realizado exitosamente');
			$productos_lista = $this->Ventas_model->listar_productos();
			$data['productos_lista'] = $productos_lista;

			if($this->Authme_model->tipo_usuario_por_id_user(user('id_usuario')) == 1)
			{
				//$data['content'] = '/ventas/realizar_venta_mobile';
				redirect(base_url().'ventas');
			}
			else
			{
				//$data['content'] = '/ventas/realizar_venta';	
				redirect(base_url().'ventas');
			};
			
			//$this->load->view('/includes/template',$data);
		}
		else
		{	
			//la venta se puede cancelar por falta de stock, esto tmb se registra en tabla ventas en DB
			$this->Ventas_model->venta_cancelada_x_falta_stock($id_venta);
			$this->session->set_flashdata('mensaje_ventas', '<i class="fab fa-stack-overflow"></i> El pedido ha sido cancelado por falta de stock en alguno de los productos seleccionados. Vuelva a intentarlo');
			redirect(base_url().'ventas');
		}
	}

	//ver preventas, son las ventas hechas pero no pagadas por el cliente, no cobradas digamos... Recordemos todos juntos que hasta aquí la empresa hasta ahora cobra cuando el repartidor reparte recién si? okey... seguimos, continuamos.-
	public function ver_preventas()
    {	
    	$data['botonMenuActivado'] = '#ver_pedidos_menu';

    	//si es user tipo 3= cliente o tipo 2 = repartidor. HOME
		if(!logged_in())
		{
			redirect('auth/login');
		};
		if($this->Authme_model->tipo_usuario_por_id_user(user('id_usuario')) == 3 OR $this->Authme_model->tipo_usuario_por_id_user(user('id_usuario')) ==2) 
		{
			redirect(base_url(''));
		};

		//si es tipo user 1, entonces carga vista mobile pero sino la de tipo 0 o sea ADMIN. ACA, en el futuro, si hay un tipo user de otro nivel, otro número, el ELSE es un problema porque también puede entrar, podría entrar por acá.. por este else de aquí abajo...
		if($this->Authme_model->tipo_usuario_por_id_user(user('id_usuario')) == 1)
		{	
			//print_r('hola');exit();
			$data['content'] = 'ventas/ver_preventas_mobile';
			//print_r($id_vendedor);exit();
			//no está trayendo los pedidos por acá sino que a través de la vista
			//$data['pedidos'] = $this->Ventas_model->listar_preventas_x_vendedor($id_vendedor);
			$this->load->view('/includes/template',$data);
		}
		else
		{	
			$data['ciudad'] = "todos";
			$data['preventista'] = "todos";
			$data['content'] = 'ventas/ver_preventas';
			$data['pedidos'] = $this->Ventas_model->listar_preventas();
			$data['preventistas'] = $this->Authme_model->listar_empleados_mas_admins();
			$data['mapa_argentina'] = $this->Ventas_model->traer_mapa_argentina();
			$this->load->view('/includes/template',$data);
		};
	}

	public function showPreventasByCityClient()
    {	
    	$data['botonMenuActivado'] = '#ver_pedidos_menu';
    	//si es user tipo 3= cliente o tipo 2 = repartidor. HOME
		if(!logged_in())
		{
			redirect('auth/login');
		};
		if($this->Authme_model->tipo_usuario_por_id_user(user('id_usuario')) == 3 OR $this->Authme_model->tipo_usuario_por_id_user(user('id_usuario')) ==2 OR $this->Authme_model->tipo_usuario_por_id_user(user('id_usuario')) ==1) 
		{
			redirect(base_url(''));
		};

		$data['content'] = 'ventas/ver_preventas';
		//$data['pedidos'] = $this->Ventas_model->listar_preventas();
		$seller = $this->input->post('seller');
		$city = $this->input->post('id_city');
		$province = $this->input->post('id_city');

		$data['pedidos'] = $this->Ventas_model->showPreventasBy($seller,$city,$province);
 
		$data['preventistas'] = $this->Authme_model->listar_empleados_mas_admins();
		$data['mapa_argentina'] = $this->Ventas_model->traer_mapa_argentina();
		
		$data['ciudad'] = $this->input->post('id_city');
		$data['preventista'] = $this->input->post('seller');
		$this->load->view('/includes/template',$data);		
	}
	
	//entregas_pendientes, es la vista exclusivamente armada para el repartidor, se ven las entregas pendientes que tienen en estados_de_ventas table el numero 3. Puede ser vista mobile o vista admin no mobile
    public function entregas_pendientes()
    {
    	$data['botonMenuActivado'] = '#entregas_pendientes_menu';

		if(!logged_in())
		{
			redirect('auth/login');
		};
		if($this->Authme_model->tipo_usuario_por_id_user(user('id_usuario')) == 3 OR $this->Authme_model->tipo_usuario_por_id_user(user('id_usuario')) == 1) 
		{
			redirect(base_url(''));
		};

		if($this->Authme_model->tipo_usuario_por_id_user(user('id_usuario')) == 2)
		{

			$data['content'] = '/ventas/entregas_mobile';
			//estoy usando una consulta al modelo en la vista, entregas pendientes por fecha y ciudad
			//$data['pedidos'] = $this->Ventas_model->listar_entregas_pendientes();
			$data['mapa_argentina'] = $this->Ventas_model->traer_mapa_argentina();
			$this->load->view('/includes/template',$data);
		}
		else
		{	
			//volver a poner entregas no mobile
			$data['content'] = '/ventas/entregas_mobile';
			//estoy usando una consulta al modelo en la vista, entregas pendientes por fecha y ciudad
			//$data['pedidos'] = $this->Ventas_model->listar_entregas_pendientes();
			$data['mapa_argentina'] = $this->Ventas_model->traer_mapa_argentina();
			$this->load->view('/includes/template',$data);
		}	
    }

    public function todas_las_entregas_pendientes()
    {	
    	$data['botonMenuActivado'] = '#entregas_pendientes_menu';

		if(!logged_in())
		{
			redirect('auth/login');
		};
		if($this->Authme_model->tipo_usuario_por_id_user(user('id_usuario')) == 3 OR $this->Authme_model->tipo_usuario_por_id_user(user('id_usuario')) == 1) 
		{
			redirect(base_url(''));
		};

		if($this->Authme_model->tipo_usuario_por_id_user(user('id_usuario')) == 2)
		{
			$data['content'] = '/ventas/todas_las_entregas_mobile';
			$data['pedidos'] = $this->Ventas_model->listar_entregas_pendientes_mobile();
			$this->load->view('/includes/template',$data);
		}
		else
		{	
			//agregar en esta vista la fecha, ordenar por fecha y poner el boton "ver detalles" para los productos
			$data['content'] = '/ventas/entregas';
			$data['pedidos'] = $this->Ventas_model->listar_entregas_pendientes();
			$this->load->view('/includes/template',$data);
		}	
    }

    //actualizar estado venta, pasa el id de la venta y el valor puede ir del 1 al 5 esta en estados de las ventas table en database
    public function actualizar_estado_venta()
	{	
		$id_venta = $this->input->post('id_venta');
		$valor = $this->input->post('valor');

		$this->Ventas_model->updateEstadoVenta($id_venta,$valor);
	}

	//eliminar preventa, se le da al admin y al vendedor la libertad de borrar una venta que aún no fue cobrada por el repartidor, ni por nadie
	public function eliminar_preventa()
	{	
		$id_venta = $this->input->post('id_venta');
		$valor = $this->input->post('valor');
		$user = $this->input->post('user');

		$this->Ventas_model->eliminarPreventa($id_venta,$valor,$user);
	}

	//ver ventas, se ven las ventas de tipo 4 , finalizadas, en estado de ventas dice 4 finalizadas, este valor de ver ventas toma como referencia, obviamente , no esa tabla sino de la tabla ventas, que estado tienen las compras hechas por los clientes. Es el repartidor cuando entrega y cobra que hace que esta venta pase de 3 a 4. De preparado por los admins a entregada y cobrada (ese interín, de 3 a 4, es ambas cosas, entregar y cobrar)
	public function ver_ventas() 
	{	
		$data['botonMenuActivado'] = '#ver_ventas_menu';

		if(!logged_in())
		{
			redirect('auth/login');
		};
		if($this->Authme_model->tipo_usuario_por_id_user(user('id_usuario')) != 0) 
		{
			redirect(base_url(''));
		};

		$data['content'] = '/ventas/ver_ventas';
		//$data['pedidos'] = $this->Ventas_model->listar_ventas_finalizadas();
		$this->load->view('/includes/template',$data);
	}

	public function filtrar_ventas_finalizadas_por_fechas() 
	{	
		if(!logged_in())
		{
			redirect('auth/login');
		};
		if($this->Authme_model->tipo_usuario_por_id_user(user('id_usuario')) != 0) 
		{
			redirect(base_url(''));
		};

		$desde = $this->input->post('desde');
		$hasta = $this->input->post('hasta');
		redirect('Ventas/ver_ventas/'.$desde.'/'.$hasta);
	}

	public function filtrar_entregas_mobile_x_ciudad()
	{
		$id_ciudad = $this->input->post('id_localidad');
		redirect('Ventas/entregas_pendientes/'.$id_ciudad);
	}
	
}

