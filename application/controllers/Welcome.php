<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	//index, página de inicio de la Distribuidora Veida
	public function index()
	{	
		if(!logged_in())
			{
				redirect('auth/login');
			}
		else
			{	
				$data['content'] = '/mensaje_de_bienvenida';
				$this->load->view('/includes/template',$data);
			};
	}
}
