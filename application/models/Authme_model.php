<?php

class Authme_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();

		$this->load->database();
		$this->config->load('authme');

		$this->users_table = $this->config->item('authme_users_table');
	}

	public function deleteUser($id_persona)
	{
		$users = $this->load->database("default", TRUE);
		$query =  $users->query("
			UPDATE personas
			SET usuario_eliminado = 1
			WHERE personas.id_persona = $id_persona");

		return true;
	}

	public function get_user($user_id)
	{
		$query = $this->db->get_where($this->users_table, array('id_usuario' => $user_id));
		if($query->num_rows()) return $query->row();
		return false;
	}

	public function get_user_by_email($email)
	{
		$query = $this->db->get_where($this->users_table, array('email' => $email));
		if($query->num_rows()) return $query->row();
		return false;
	}
	
	public function get_users($order_by = 'id_usuario', $order = 'asc', $limit = 0, $offset = 0)
	{
		$this->db->order_by($order_by, $order); 
		if($limit) $this->db->limit($limit, $offset);
		$query = $this->db->get($this->users_table);
		return $query->result();
	}

	public function get_user_count()
	{
		return $this->db->count_all($this->users_table);
	}

	public function create_user($email,$password)
	{
		$data = array(
			'email' => filter_var($email, FILTER_SANITIZE_EMAIL),
			'password' => $password, // Should be hashed
			'dt_registro' => date('Y-m-d H:i:s')
		);
		$this->db->insert($this->users_table, $data);
		return $this->db->insert_id();
	}

	public function update_user($user_id, $data)
	{
		$this->db->where('id_usuario', $user_id);
		$this->db->update($this->users_table, $data); 
	}
	
	public function delete_user($user_id)
	{
		$users_lista_tbl = $this->load->database("default", TRUE);
		$query = $users_lista_tbl->query("DELETE FROM users where users.id_usuario = '$user_id'");
		return true;
	}
	
	private function create_users_table()
	{
		$this->load->dbforge();
		$this->dbforge->add_field('id INT(11) NOT NULL AUTO_INCREMENT');
		$this->dbforge->add_field('email VARCHAR(200) NOT NULL');
		$this->dbforge->add_field('password VARCHAR(200) NOT NULL');
		$this->dbforge->add_field('created DATETIME NOT NULL');
		$this->dbforge->add_key('id', true);
		$this->dbforge->create_table($this->users_table);
	}

	//todas las funciones anteriores son propias de la librería AUTHME, pero agregar nueva persona no... su nombre indica la función claramente. LA PERSONA puede ser de tipo cliente, admin o empleado
	public function agregar_nueva_persona($nombre,$apellido,$calle_dir,$numero_dir,$ciudad,$provincia,$area_tel,$tel,$geolat,$geolong,$dni,$tipo_usuario,$cargado_por)
	{
		$nombre_localidad = $this->getCiudadByIdLocalidad($ciudad);
		$nombre_provincia = $this->getProvinciaByIdLocalidad($ciudad);
		$personas_tbl = $this->load->database("default", TRUE);
		$query = $personas_tbl->query("
			INSERT INTO personas (Nombres,Apellido,calle_direccion,numero_direccion,ciudad,provincia,area_telefono,telefono,latitud,longitud,DNI,tipo_usuario,cargado_por) 
			VALUES ('$nombre','$apellido','$calle_dir','$numero_dir','$nombre_localidad','$nombre_provincia','$area_tel','$tel','$geolat','$geolong','$dni','$tipo_usuario','$cargado_por');");

		return true;
	}

	public function getCiudadByIdLocalidad($id_localidad)
	{
		//hacer esto y al actualizar ubicación en clientes, o al dar de alta, hacer sin geoubicación tal vez, sino que simplemente , ir viendo... pero agregar esto al dar de alta y al actualizar los datos del cliente, para no tener que cambiar nada en las muestras, que no se guarde en database el id sino los nombres , pero a partir de este getCiudadProvinciaById($id_localidad)
		$mapa_argentina = $this->load->database("default", TRUE);
		$query =  $mapa_argentina->query("
			SELECT localidades.id as id_localidad, provincias.nombre as nombre_provincia, localidades.nombre as nombre_ciudad, localidades.codigo_postal as codigo_postal
			FROM localidades, provincias
			WHERE localidades.id_provincia = provincias.id && provincias.id = 9 && localidades.id = '$id_localidad'
			")->row()->nombre_ciudad;

		return $query;
	}

	public function getProvinciaByIdLocalidad($id_localidad)
	{
		//hacer esto y al actualizar ubicación en clientes, o al dar de alta, hacer sin geoubicación tal vez, sino que simplemente , ir viendo... pero agregar esto al dar de alta y al actualizar los datos del cliente, para no tener que cambiar nada en las muestras, que no se guarde en database el id sino los nombres , pero a partir de este getCiudadProvinciaById($id_localidad)
		$mapa_argentina = $this->load->database("default", TRUE);
		$query =  $mapa_argentina->query("
			SELECT localidades.id as id_localidad, provincias.nombre as nombre_provincia, localidades.nombre as nombre_ciudad, localidades.codigo_postal as codigo_postal
			FROM localidades, provincias
			WHERE localidades.id_provincia = provincias.id && provincias.id = 9 && localidades.id = '$id_localidad'
			")->row()->nombre_provincia;

		return $query;
	}

	//Devuelve el ultimo ID de la persona en tabla de PERSONAS 
	public function devolverUltimoID_de_personas()
	{
		$personas_tbl = $this->load->database("default", TRUE);
		$query = $personas_tbl->query("
			SELECT MAX(id_persona) AS id 
			FROM personas");
		$aux = $query->result();
		$id = $aux[0]->id;
		return $id;
	}

	//Devuelve el ultimo ID de la persona en tabla de USERS
	public function devolverUltimoID_de_users()
	{
		$personas_tbl = $this->load->database("default", TRUE);
		$query = $personas_tbl->query("SELECT MAX(id_usuario) AS id FROM users");
		$aux = $query->result();
		$id = $aux[0]->id;
		return $id;
	}

	//de ahora en más, funciones simples que no necesitan ser explicadas
	public function actualizar_id_usuario_con_id_persona($id_persona,$id_user_nuevo)
	{
		$users_lista_tbl = $this->load->database("default", TRUE);
		$query = $users_lista_tbl->query("
			UPDATE personas 
			SET id_usuario = '$id_user_nuevo' 
			WHERE personas.id_persona = '$id_persona'");

		return true;
	}

	//queda pendiente eliminar usuario en botones, tanto en vista admin como preventista
	public function listar_clientes()
	{
		$personas_tbl = $this->load->database("default", TRUE);
		$query = $personas_tbl->query("
			SELECT * 
			FROM personas 
			WHERE personas.tipo_usuario = 3
			&& personas.usuario_eliminado is null");

		return $query->result();
	}

	//queda pendiente eliminar usuario en botones, tanto en vista admin como preventista
	public function listarClientesBy($preventista)
	{
		$personas_tbl = $this->load->database("default", TRUE);
		$query = $personas_tbl->query("
			SELECT * 
			FROM personas 
			WHERE personas.tipo_usuario = 3
			&& cargado_por = $preventista
			&& personas.usuario_eliminado is null");

		return $query->result();
	}

	public function listar_empleados()
	{
		$users_lista_tbl = $this->load->database("default", TRUE);
		$query = $users_lista_tbl->query("
			SELECT personas.tipo_usuario,personas.id_persona,users.id_usuario,users.email,personas.DNI,personas.area_telefono,personas.telefono,personas.Nombres,personas.Apellido 
			FROM personas JOIN users 
			WHERE (personas.tipo_usuario = 2 OR personas.tipo_usuario = 1) && personas.id_usuario = users.id_usuario");

		return $query->result();
	}

	public function getPreventistas()
	{
		$users_lista_tbl = $this->load->database("default", TRUE);
		$query = $users_lista_tbl->query("
			SELECT personas.tipo_usuario,personas.id_persona,users.id_usuario,users.email,personas.DNI,personas.area_telefono,personas.telefono,personas.Nombres,personas.Apellido 
			FROM personas JOIN users 
			WHERE (personas.tipo_usuario = 1) && personas.id_usuario = users.id_usuario");

		return $query->result();
	}

	public function listar_empleados_mas_admins()
	{
		$users_lista_tbl = $this->load->database("default", TRUE);
		$query = $users_lista_tbl->query("
			SELECT personas.tipo_usuario,personas.id_persona,users.id_usuario,users.email,personas.DNI,personas.area_telefono,personas.telefono,personas.Nombres,personas.Apellido 
			FROM personas JOIN users 
			WHERE (personas.tipo_usuario = 2 OR personas.tipo_usuario = 1 OR personas.tipo_usuario = 0) && personas.id_usuario = users.id_usuario");

		return $query->result();
	}

	public function listar_roles_de_usuarios()
	{
		$users_lista_tbl = $this->load->database("default", TRUE);
		$query = $users_lista_tbl->query("
			SELECT * 
			FROM roles_de_users");

		return $query->result();
	}

	public function cambiar_rol($id_persona,$nuevo_rol)
	{
		$users_lista_tbl = $this->load->database("default", TRUE);
		$query = $users_lista_tbl->query("
			UPDATE personas 
			SET tipo_usuario = '$nuevo_rol' 
			WHERE personas.id_persona = '$id_persona'");

		return true;
	}

	public function actualizar_datos($id_persona,$nombre,$apellido,$area,$telefono,$DNI)
	{
		$users_lista_tbl = $this->load->database("default", TRUE);
		$query = $users_lista_tbl->query("
			UPDATE personas 
			SET Nombres = '$nombre', Apellido ='$apellido',area_telefono='$area',telefono = '$telefono',DNI = '$DNI' 
			WHERE personas.id_persona = '$id_persona'");

		return true;
	}

	public function mostrar_nombre_x_id($id_persona)
	{
		$users_lista_tbl = $this->load->database("default", TRUE);
		$query = $users_lista_tbl->query("
			SELECT * 
			FROM personas 
			WHERE personas.id_persona = '$id_persona'");

		$persona = $query->result();
		$nombreyapellido = $persona[0]->Apellido.", ".$persona[0]->Nombres;

		return $nombreyapellido;
	}

	public function mostrar_persona($id_persona)
	{
		$users_lista_tbl = $this->load->database("default", TRUE);
		$query = $users_lista_tbl->query("
			SELECT * 
			FROM personas 
			WHERE personas.id_persona = '$id_persona'");

		return $query->result();
	}

	public function mostrar_vendedor_x_id_user($id_user)
	{
		$users_lista_tbl = $this->load->database("default", TRUE);
		$query = $users_lista_tbl->query("
			SELECT personas.Nombres,personas.Apellido 
			FROM personas,users 
			WHERE personas.id_usuario = '$id_user' AND personas.id_usuario = users.id_usuario");

		$persona = $query->result();
		$nombreyapellido = $persona[0]->Apellido.", ".$persona[0]->Nombres;

		return $nombreyapellido;
	}

	public function mostrar_id_persona_x_id_user($id_user)
	{
		$users_lista_tbl = $this->load->database("default", TRUE);
		$query = $users_lista_tbl->query("
			SELECT personas.id_persona 
			FROM personas,users 
			WHERE personas.id_usuario = '$id_user' AND personas.id_usuario = users.id_usuario");

		$persona = $query->result();
		$id_persona_x_user = $persona[0]->id_persona;

		return $id_persona_x_user;
	}

	public function dar_nombre_por_id_user($id_user)
	{
		$users_lista_tbl = $this->load->database("default", TRUE);
		$query = $users_lista_tbl->query("
			SELECT personas.Nombres,personas.Apellido 
			FROM personas,users 
			WHERE personas.id_usuario = '$id_user' AND personas.id_usuario = users.id_usuario");

		$persona = $query->result();
		$nombreyapellido = $persona[0]->Nombres." ".$persona[0]->Apellido;

		return $nombreyapellido;
	}

	public function tipo_usuario_por_id_user($id_user)
	{
		$users_lista_tbl = $this->load->database("default", TRUE);
		$query = $users_lista_tbl->query("
			SELECT personas.tipo_usuario as tipo 
			FROM personas,users 
			WHERE personas.id_usuario = '$id_user' AND personas.id_usuario = users.id_usuario");

		$tipo = $query->result();
		$tipo_user = $tipo[0]->tipo;

		return $tipo_user;
	}

	public function actualizar_ubicacion($id_persona,$calle,$altura,$id_localidad)
	{
		$users_lista_tbl = $this->load->database("default", TRUE);

		$ciudad = $this->getCiudadByIdLocalidad($id_localidad);
		$provincia = $this->getProvinciaByIdLocalidad($id_localidad);

		$query = $users_lista_tbl->query("
			UPDATE personas
			SET calle_direccion = '$calle', numero_direccion = '$altura', ciudad = '$ciudad', provincia = '$provincia', latitud =null,longitud = null
			WHERE personas.id_persona = '$id_persona'");

		return true;
	}

	public function actualizar_direccion($id_persona,$calle,$altura)
	{
		$users_lista_tbl = $this->load->database("default", TRUE);

		//$ciudad = $this->getCiudadByIdLocalidad($id_localidad);
		//$provincia = $this->getProvinciaByIdLocalidad($id_localidad);

		$query = $users_lista_tbl->query("
			UPDATE personas
			SET calle_direccion = '$calle', numero_direccion = '$altura', latitud =null,longitud = null
			WHERE personas.id_persona = '$id_persona'");

		return true;
	}

	public function actualizar_ciudad($id_persona,$id_localidad)
	{
		$users_lista_tbl = $this->load->database("default", TRUE);

		$ciudad = $this->getCiudadByIdLocalidad($id_localidad);
		$provincia = $this->getProvinciaByIdLocalidad($id_localidad);

		$query = $users_lista_tbl->query("
			UPDATE personas
			SET ciudad = '$ciudad', provincia = '$provincia'
			WHERE personas.id_persona = '$id_persona'");

		return true;
	}
	
}
