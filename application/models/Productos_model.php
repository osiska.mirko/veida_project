<?php

class Productos_model extends CI_Model {
	
	public function __construct()
	{
		parent::__construct();

		$this->load->database();
	}
	
	//A, del abm de productos. Me parece que todas estas funciones están sin uso porque para el tratamiento ABM de productos se usa el EDITABLEGRID, la librería JAVASCRIPT para ABM de productos, stock y precios
	public function guardar_producto($nombre,$categoria,$descripcion)
	{
			$productos_tbl = $this->load->database("default", TRUE);
			$query = $productos_tbl->query("
				INSERT INTO productos (nombre_producto,descrip_producto,categoria_producto) 
				VALUES ('$nombre','$descripcion','$categoria');");
			return true;		
	}

	//ver productos, no se si se usa
	public function ver_productos_db($categoria)
	{
		$productos_tbl = $this->load->database("default", TRUE);
		$busqueda = $productos_tbl->query("
			SELECT * 
			FROM productos
			WHERE productos.stock > 0");
		return $busqueda->result();
	}

	//ver precio por id producto, si se usa (creo)
	public function ver_precio_por_id_producto($id_producto)
	{
		$productos_tbl = $this->load->database("default", TRUE);
		$busqueda = $productos_tbl->query("
			SELECT productos.precio as precio 
			FROM productos 
			WHERE productos.id = '$id_producto'");
		return $busqueda->result();
	}

	public function ver_categoria_por_id_categoria($id_categoria)
	{
		$productos_tbl = $this->load->database("default", TRUE);
		$busqueda = $productos_tbl->query("
			SELECT categorias_de_productos.nombre_catego as nombre 
			FROM categorias_de_productos
			WHERE id_categoria = '$id_categoria'");
		return $busqueda->result();
	}

}

