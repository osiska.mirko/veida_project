<?php
//agregar en database un campo que sea id_localidad y qe cada vez q se guarda , guarde el nombre, ya sea por latitud lognitud o por todo completito escribiendo opciones, tomando el nombre , que haga una consulta segun el id localidad recibido y guarde siempre nombre ciudad y nombre provincia, para no interferir con el resto del sistema y que funcione tod en paralelo y esto de id locadliad me sirve para preventas mostrar eligiendo por ciudad jaigurudev y solucionado !! comprobar venta mobile tmb afectar arreglar agregar digamos el select2

class Ventas_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();

		$this->load->database();
	}

	//sumar precio final, se envía como parámetro el id_venta y se hace un select...
	public function sumar_precio_final($id_compra)
	{		
		$tabla_para_sumar_precios = $this->load->database("default", TRUE);
		//este select trae los productos de un carrito de ventas específicamente de la compra en cuestión de id_venta de parámetro
		$aux = $tabla_para_sumar_precios->query("
			SELECT carrito_de_ventas.precio_del_producto, carrito_de_ventas.id_producto as id_producto, carrito_de_ventas.cantidad_producto 
			FROM carrito_de_ventas, ventas, productos 
			WHERE carrito_de_ventas.id_venta_actual = '$id_compra' && carrito_de_ventas.id_venta_actual = ventas.id_venta && carrito_de_ventas.id_producto = productos.id");
		$aux = $aux->result();

		$precio_final = 0;

		//se hace un foreach de esa tabla y en la variable precio final se va guardando el precio de cada producto (Del carrito, de como fue hecho el carrito, da igual si el mismo producto se repite porque fue puesto en el carrito dos veces digamos)
		//se calcula precio producto por su respectiva cantidad
		foreach ($aux as $row) 
		{
		    $precio_final = $precio_final + ($row->precio_del_producto * $row->cantidad_producto);
		                   
		};
			
		return $precio_final;			
	}

	//actualizar estado pago, exclusivamente cambia el estado de la venta de 1 que no significa nada a 2 que es una preventa confirmada. Guarda también qué vendedor realizó la transacción , cual fue el monto final en el carrito de ventas y cual es el comprador (nuevo o seleccionado de db existente previamente)
	public function actualizar_estado_pago($busqueda,$precio_final,$id_vendedor,$id_comprador)
	{
		$this->db->trans_start();
			$guardo_el_pago = $this->load->database("default", TRUE);
			$efectivo = $guardo_el_pago->query("
				UPDATE ventas 
				SET estado_venta = '2',id_vendedor='$id_vendedor', monto_final_cobrado= '$precio_final',id_comprador = '$id_comprador' 
				WHERE ventas.id_venta = '$busqueda';");
		$this->db->trans_complete();
	}

	//venta cancelada por stock, se chequea si efectivamente hay stock de los productos a venderse, sino... por más que se ejecutó actualizar estado pago y guardó todos sus datos, la venta pasa a estado 5 y lo cobrado pasa a ser $0
	public function venta_cancelada_x_falta_stock($busqueda)
	{
		$this->db->trans_start();
			$guardo_el_pago = $this->load->database("default", TRUE);
			$efectivo = $guardo_el_pago->query("UPDATE ventas SET estado_venta = '5', monto_final_cobrado= '0' WHERE ventas.id_venta = '$busqueda';");
		$this->db->trans_complete();
	}

	//traer precio por id producto
	public function traer_precio_por_id_producto($id_producto)
	{	
		$busqueda_id = $this->load->database("default", TRUE);
		$this->db->trans_start();	
			$aux = $busqueda_id->query("
				SELECT productos.precio 
				FROM productos 
				WHERE productos.id = '$id_producto'");
			$precio_producto = $aux->row();
			return $precio_producto->precio;
		$this->db->trans_complete();	
	}

	//generar nueva venta, se traen los ids productos y cantidades
	public function generar_venta_nueva($ids_productos,$cantidad)
	{	
		$fecha = date('Y-m-d H:i:s');
			
		$db_ventas = $this->load->database("default", TRUE);
		
		$this->db->trans_start();	
		
			//insert en ventas, se crea digamos una nueva venta, estado 1 y fecha actual
			$query = $db_ventas->query("
				INSERT INTO ventas (fecha_venta,estado_venta) 
				VALUES ('$fecha','1');");
			
			$busqueda_max_id = $this->load->database("default", TRUE);
			//se trae el ultimo id de la venta (se podría haber hecho con un insert_id pero no sabía)
			$aux = $busqueda_max_id->query("SELECT MAX(id_venta) AS id FROM ventas");
			$busqueda_id = $aux->row();
			//literal, aquí tengo en la variable siguiente el número de id de la ultima venta
			$busqueda_id = $busqueda_id->id;
				
			$precio_producto = '0';

			//se inicializa recorrido cantidad en cero
			$recorrido_cantidad = 0;
			//se chequea que ids_productos no este vacío
            if ($ids_productos != null)
            {	
            	//ids_productos provino literal de los multiples inputs de seleccion de productos de la vista correspondiente en donde sus datos se guardaron en array, se los recorre y 1 a 1 se hace un insert en la tabla que representa el carrito de ventas, okey?
	            foreach ($ids_productos as $row) 
                {	
                	//se trae el precio del producto en cuestión.
                	$precio_producto = $this->Ventas_model->traer_precio_por_id_producto($row);
                	//se puede apreciar, que también viene como parámetro la cantidad, que es un array que se genera en la vista donde se seleccionan los productos por el vendedor y al lado tenemos el input cantidad. Sabemos que si input numero 0 tiene al lado input cantidad tambien ira en el espacio del array suyo 0 ese valor. Es decir, 2 arrays simultaneas, espacio 0 de uno es el producto (el id producto), espacio 0 del otro es la cantidad de ese producto, ahi tenemos 1 producto, espacio 1 de uno y espacio 1 del otro es el segundo producto y así sucesivamente. 
                    $actualizar_carrito_de_ventas= $db_ventas->query("
                    	INSERT INTO carrito_de_ventas (id_venta_actual, id_producto, precio_del_producto, cantidad_producto) 
                    	VALUES ('$busqueda_id','$row','$precio_producto','$cantidad[$recorrido_cantidad]');");
                    //Por eso recorrido cantidad esa variable arranca en 0 y es equivalente a la cantidad el primer producto. Luego , por cada instancia del foreach se le va sumando uno y cuando sale del foreach, como son dos arrays equivalentes... 
                    //ya lo entendiste, todo es correcto
                    $recorrido_cantidad = $recorrido_cantidad + 1;
                }; 
			};

		$this->db->trans_complete();

		return $busqueda_id;
	}

	//chequear stock de producto, es literalmente la función del modelo que revisa que efectivamente haya stock de los productos seleccionados para confirmar la preventa. La preventa se hace, pero esta función no decide si hay o no stock, simplemente arroja 3 valores, puede que haya stock justo , puede que sí haya stock y permanezca más sobrando, quede stock sobrante, la venta sería factible, puede que se haya pedido en la preventa un stock que no existe, stock faltante. 
	//Esta es una función que recibe como parámetros id producto e id venta por lo cual se debe hacer en cada compra, como son varios productos en el carrito, se usa esta función de forma iterativa
	//DEVUELVE STOCK RESTANTE o sea stock actual menos pedido actual, para actualizar el stock REAL
	public function chequear_stock_de_producto($id_producto,$id_venta)
	{
		$chequeo_stock_pedido_db = $this->load->database("default", TRUE);
		
		//se chequea el stock de un producto en cuestión, en tabla productos está todo 
		$stock_query = $chequeo_stock_pedido_db->query("
			SELECT productos.stock from productos 
			WHERE productos.id = '$id_producto'");
		
		//se chequea cuánto, se hace un número de cuanto de cada producto o de un producto en cuestión se pidió
		$pedido_query = $chequeo_stock_pedido_db->query("
			SELECT SUM(carrito_de_ventas.cantidad_producto) as cant_producto_pedido 
			FROM carrito_de_ventas 
			WHERE (carrito_de_ventas.id_producto = '$id_producto') && (carrito_de_ventas.id_venta_actual = '$id_venta')");

		$stock_query = $stock_query->result();
		$pedido_query = $pedido_query->result();

		$stock = $stock_query[0]->stock;
		$cantidad_de_producto_del_pedido = $pedido_query[0]->cant_producto_pedido;

		//aquí se tienen en stock y en cantidad producto del pedido , ejemplo, queda de producto 1 stock = 5 y el pedido de producto 1 es 4... aquí, seguidamente, se dan esas comparaciones y se retorna stockrestante del producto, que puede ser > , = o <

		if ($cantidad_de_producto_del_pedido == $stock)
		{	
			$stock_restante_del_producto = 0; 
			return $stock_restante_del_producto;
		};

		if ($cantidad_de_producto_del_pedido < $stock)
		{	
			$stock_restante_del_producto = $stock - $cantidad_de_producto_del_pedido; 
			return $stock_restante_del_producto;
		};

		if ($cantidad_de_producto_del_pedido > $stock)
		{
			$stock_restante_del_producto = -1;
			return $stock_restante_del_producto;
		};
	}

	//chequear stock de un pedido, de una preventa que se quiere confirmar 'digamos...'. Si falta stock rechaza la venta pero si alcanza el stock, YA ACTUALIZA EL STOCK REAL en BASE DE DATOS, ojo. Aquí es donde se actualiza el stock por cada venta
	public function chequear_stock_de_un_pedido($id_pedido)
	{
		$chequeo_stock_venta_entera_db = $this->load->database("default", TRUE);

		$this->db->trans_start();

			//se traen todos los id productos de un carrito de ventas en funcion de una venta, de un id venta
			$chequeo_venta_stock_query = $chequeo_stock_venta_entera_db->query("
				SELECT DISTINCT carrito_de_ventas.id_producto 
				FROM `carrito_de_ventas` 
				WHERE id_venta_actual = '$id_pedido'");
			//se guardan en vairable chequeo venta stock, este select
			$chequeo_venta_stock = $chequeo_venta_stock_query->result();

			//se hace un foreach para analizar este select, que tiene los id_productos de la preventa,ok?
			//puede ser que el primer foreach esté demás?? probar borrandolo y dejando solo el if y el foreach de adentro
			//REFACTORIZACIÓN PENDIENTE (aunque así funciona,pues..)
			foreach ($chequeo_venta_stock as $row) 
	        {	
	        	//si aunque sea 1 de los productos, en 1... ejemplo, prod 4 tiene stock 3 y se piden 4, da menos 1... eso significa que falta stock, se retorna error, se cancela todo!
	        	if ($this->Ventas_model->chequear_stock_de_producto($row->id_producto,$id_pedido)==(-1))
	        	{	
	        		//venta rechazada por falta en alguno de los productos seleccionados en la lista
	        		$error = "0";
	        		return $error;
	        	}
	        	else
	        	//sino, es un éxito, y se procede a, efectivamente, DECRECENTAR EL STOCK
	        	{	
	        		//se hace un foreach de nuevo (que posiblemente es el único necesario)
	        		foreach ($chequeo_venta_stock as $row)
	        		{	
	        			//aqui se hace efectivamente la actualizacion del stock, se chequea pedido contra stock del producto, se devuelve el stock que QUEDARÍA restante
	        			$stock_a_decrementar_de_producto = $this->Ventas_model->chequear_stock_de_producto($row->id_producto,$id_pedido);
	        			//aqui con el numero del stock que QUEDARÍA restante, se ACTUALIZA EN DATABASE REALMENTE
	        			$this->Ventas_model->actualizarStockDeProducto($row->id_producto,$stock_a_decrementar_de_producto);
	        		}
	        		$exito = "1";
	        		return $exito;
	        	};
	        //SI SE RETORNA 0 ES ERROR SI SE RETORNA 1 ES EXITO
	        };

		$this->db->trans_complete();
	} 

	//actualiza stock
	public function actualizarStockDeProducto($id_producto,$stock_actual)
	{
		$actualizar_stock_de_producto = $this->load->database("default", TRUE);
		$act_stock_query = $actualizar_stock_de_producto->query("
			UPDATE productos 
			SET productos.stock = '$stock_actual' 
			WHERE productos.id = '$id_producto'");
	}   

	//listar_productos, donde productos tengan stock
	public function listar_productos()
	{
		$productos_lista_tbl = $this->load->database("default", TRUE);
		$query = $productos_lista_tbl->query("
			SELECT categorias_de_productos.nombre_catego as categoria_producto,productos.nombre_producto as nombre_producto, productos.stock as stock, productos.precio as precio_producto, productos.id as id_producto, productos.descrip_producto as descrip_producto 
			FROM productos,categorias_de_productos 
			WHERE productos.stock > 0 && productos.categoria_producto = categorias_de_productos.id_categoria");

		return $query->result();
	}

	//listar preventas, o sea estado 2 o 3, preparados o no preparados, son preventas
	public function listar_preventas()
	{
		$preventas_lista_tbl = $this->load->database("default", TRUE);
		$query = $preventas_lista_tbl->query("
			SELECT * 
			FROM `ventas` 
			WHERE ventas.estado_venta = 2 OR ventas.estado_venta=3 ");

		return $query->result();
	}

	public function showPreventasBy($seller,$city,$province)
	{
		$seller_tbl = $this->load->database("default", TRUE);

		if ($seller=="todos")
		{
			$seller_query = "";
		}
		elseif ($seller!="todos")
		{
			$seller_query = " && ventas.id_vendedor = '$seller'";
		};

		if ($city=="todos")
		{
			$city_query = "";
		}
		elseif ($city!="todos")
		{
			$province_q = $this->Authme_model->getProvinciaByIdLocalidad($province);
			$city_q = $this->Authme_model->getCiudadByIdLocalidad($city);
			$city_query = " && (personas.ciudad = '$city_q' && personas.provincia = '$province_q')";
		};

		$query = $seller_tbl->query("
			SELECT * 
			FROM `ventas`,personas
			WHERE (ventas.estado_venta = 2 OR ventas.estado_venta=3)
			&& ventas.id_comprador = personas.id_persona 
			$seller_query $city_query");

		return $query->result();
	}

	public function listar_preventas_x_fecha($fecha_hoy)
	{
		$preventas_lista_tbl = $this->load->database("default", TRUE);

		$fecha1 = $fecha_hoy.' 00:00:00';
		$fecha2 = $fecha_hoy.' 23:59:59';

		$query = $preventas_lista_tbl->query("
			SELECT * 
			FROM `ventas` 
			WHERE (ventas.estado_venta = 2 OR ventas.estado_venta=3) && (ventas.fecha_venta BETWEEN '$fecha1' AND '$fecha2') ");

		return $query->result();
	}

	public function listarPreventasByFechaAndPreventista($fecha_hoy,$preventista)
	{
		$preventas_lista_tbl = $this->load->database("default", TRUE);

		$fecha1 = $fecha_hoy.' 00:00:00';
		$fecha2 = $fecha_hoy.' 23:59:59';

		$query = $preventas_lista_tbl->query("
			SELECT * 
			FROM `ventas` 
			WHERE (ventas.estado_venta = 2 OR ventas.estado_venta=3) 
			&& (ventas.fecha_venta BETWEEN '$fecha1' AND '$fecha2') 
			&& ventas.id_vendedor =  '$preventista'");

		return $query->result();
	}

	//listar preventas pero por su vendedor
	public function listar_preventas_x_vendedor($id_vendedor)
	{
		$preventas_lista_tbl = $this->load->database("default", TRUE);
		$query = $preventas_lista_tbl->query("
			SELECT * 
			FROM `ventas` 
			WHERE (ventas.estado_venta = 2 OR ventas.estado_venta=3) 
			&& ventas.id_vendedor =  '$id_vendedor'");

		return $query->result();
	}

	//listar preventas pero solo en estado 3, o 4 porque fue finalizado
	public function listar_entregas_pendientes()
	{
		$preventas_lista_tbl = $this->load->database("default", TRUE);
		$query = $preventas_lista_tbl->query("
			SELECT * 
			FROM `ventas` 
			WHERE ventas.estado_venta = 3 OR ventas.estado_venta = 4");

		return $query->result();
	}

	public function listar_entregas_pendientes_mobile()
	{
		$preventas_lista_tbl = $this->load->database("default", TRUE);
		$query = $preventas_lista_tbl->query("
			SELECT * 
			FROM `ventas` 
			WHERE ventas.estado_venta = 3 ");

		return $query->result();
	}

	public function listar_entregas_pendientes_x_fecha($fecha_hoy)
	{
		$preventas_lista_tbl = $this->load->database("default", TRUE);
		$fecha1 = $fecha_hoy.' 00:00:00';
		$fecha2 = $fecha_hoy.' 23:59:59';
		$query = $preventas_lista_tbl->query("
			SELECT * 
			FROM `ventas` 
			WHERE (ventas.estado_venta = 3 OR ventas.estado_venta = 4) && (ventas.fecha_venta BETWEEN '$fecha1' AND '$fecha2')");

		return $query->result();
	}

	public function listar_entregas_pendientes_x_fecha_y_x_ciudad($fecha_hoy,$ciudad)
	{
		$localidad_nombre= $this->Authme_model->getCiudadByIdLocalidad($ciudad);
		$provincia_nombre= $this->Authme_model->getProvinciaByIdLocalidad($ciudad);

		$preventas_lista_tbl = $this->load->database("default", TRUE);
		$fecha1 = $fecha_hoy.' 00:00:00';
		$fecha2 = $fecha_hoy.' 23:59:59';
		$query = $preventas_lista_tbl->query("
			SELECT * 
			FROM ventas,personas,localidades,provincias
			WHERE (ventas.estado_venta = 3 OR ventas.estado_venta = 4) && (ventas.fecha_venta BETWEEN '$fecha1' AND '$fecha2') 
			&& (ventas.id_comprador = personas.id_persona)
			&& (personas.ciudad = localidades.nombre && localidades.nombre = '$localidad_nombre')
			&& (personas.provincia = provincias.nombre && provincias.nombre = '$provincia_nombre')");

		return $query->result();
	}

	//lista las ventas estado 4
	public function listar_ventas_finalizadas($desde,$hasta)
	{
		$preventas_lista_tbl = $this->load->database("default", TRUE);
		$hasta_ultima_hora = $hasta." 23:59:59";
		$query = $preventas_lista_tbl->query("
			SELECT * 
			FROM `ventas` 
			WHERE (ventas.estado_venta = 4) && (ventas.fecha_venta BETWEEN '$desde' AND '$hasta_ultima_hora')");

		return $query->result();
	}

	public function calcular_monto_ventas_por_fechas($desde,$hasta)
	{
		$preventas_lista_tbl = $this->load->database("default", TRUE);
		$hasta_ultima_hora = $hasta." 23:59:59";
		$query = $preventas_lista_tbl->query("
			SELECT SUM(ventas.monto_final_cobrado) as monto_final 
			FROM `ventas` 
			WHERE (ventas.estado_venta = 4) && (ventas.fecha_venta BETWEEN '$desde' AND '$hasta_ultima_hora')");

		return $query->result();
	}

	//muestra todo 1 carrito, según id venta
	public function mostrar_carrito($id_venta)
	{
		$preventas_lista_tbl = $this->load->database("default", TRUE);
		$carrito = $preventas_lista_tbl->query("
			SELECT * 
			FROM `carrito_de_ventas` 
			WHERE id_venta_actual = '$id_venta'");

		return $carrito->result();
	}

	//muestra producto, sus características, en función de un parámetro de id producto
	public function mostrar_producto_por_id($id_producto)
	{
		$productos_lista_tbl = $this->load->database("default", TRUE);
		$query =  $productos_lista_tbl->query("
			SELECT productos.nombre_producto as nombre_producto, productos.precio as precio_producto, productos.id as id_producto, productos.descrip_producto as descrip_producto 
			FROM productos 
			WHERE productos.id = '$id_producto'");

		return $query->result();
	}

	//UPDATE estado se usa en function actualizar_estado_venta()
	public function updateEstadoVenta($id,$valor)
    {
        $entregas_lista_tbl = $this->load->database("default", TRUE);
		$query =  $entregas_lista_tbl->query("
			UPDATE ventas 
			SET estado_venta = '$valor' 
			WHERE ventas.id_venta = '$id'");

        return true;
    }

    //elimina una venta
    public function eliminarPreventa($id,$valor,$user_actual)
    {
        $entregas_lista_tbl = $this->load->database("default", TRUE);
 
 		$carrito = $entregas_lista_tbl->query("
 			SELECT * 
 			FROM carrito_de_ventas 
 			WHERE id_venta_actual = '$id'");
 		$carrito = $carrito->result();

 		//se hace foreach para devolver el stock , o sea, sumar cantidad de cada producto del carrito de ventas en cada stock correspondiente a cada producto
 		foreach ($carrito as $id_carrito)
 		{	
 			$id_producto = $id_carrito->id_producto;
 			$cantidad = $id_carrito->cantidad_producto;
 			$this->Ventas_model->actualizar_stock_en_eliminar_preventa($id_producto,$cantidad);
 		};

 		$fecha = date('Y-m-d H:i:s');

 		//se guarad el user que realizó esa cancelación, y su fecha
  		$query =  $entregas_lista_tbl->query("
  			UPDATE ventas 
  			SET estado_venta = '$valor', fecha_cancelacion = '$fecha', id_cancelador = '$user_actual' 
  			WHERE ventas.id_venta = '$id'");
        return true;
    }

    //actualiza el stock, se le pasa el producto y cantidad, pero para dar de baja una venta, eso quiere decir, incrementa los stocks
    public function actualizar_stock_en_eliminar_preventa($id_producto,$cantidad)
    {
    	$busqueda_stock = $this->load->database("default", TRUE);
		$aux = $busqueda_stock->query("SELECT productos.stock AS stock_actual 
			FROM productos 
			WHERE productos.id = '$id_producto'");
		$stock_actual = $aux->row();
		$stock_actual = $stock_actual->stock_actual;

		$nuevo_valor = $stock_actual + $cantidad;

		$eliminar_preventa = $this->load->database("default", TRUE);
		$preventa_query =  $eliminar_preventa->query("
			UPDATE productos 
			SET stock = '$nuevo_valor' 
			WHERE productos.id = '$id_producto'");
        return true;
    }

    //trae todas las provincias y sus localidades, id localidad nombre ciudad, nombre provincia y cod postal
    public function traer_mapa_argentina()
    {
    	$mapa_argentina = $this->load->database("default", TRUE);
		$query =  $mapa_argentina->query("
			SELECT localidades.id as id_localidad, provincias.nombre as nombre_provincia, localidades.nombre as nombre_ciudad, localidades.codigo_postal as codigo_postal
			FROM localidades, provincias
			WHERE localidades.id_provincia = provincias.id && provincias.id = 9
			");

		return $query->result();
    }

}

