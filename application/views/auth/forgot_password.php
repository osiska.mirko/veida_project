<html>
<head>
	<title>Olvidé mi contraseña</title>
</head>
	<body>
		<?php 
		if($success){
			redirect('auth/mail_recuperacion_enviado');
		} else { ?>
		   	<div class="d-flex justify-content-center" style="margin: 150px 0 0 0px;">
	    	<div class="login-box">
			  <div class="login-logo">
			    <a href="#"><b>Distribuidora </b>VEIDA</a>
			  </div>
			  <!-- /.login-logo -->
			  <div class="card">
			    <div class="card-body login-card-body">
			      <p class="login-box-msg">Olvidaste tu contraseña? aquí podrás recuperar tu contraseña fácilmente ingresando tu mail:</p>

			      <form action="<?php echo base_url('auth/forgot')?>" method="post">
			        <div class="input-group mb-3">
			          <input type="email" name="email" class="form-control" placeholder="Email">
			          
			          <div class="input-group-append">
			            <div class="input-group-text">
			              <span class="fas fa-envelope"></span>
			            </div>
			          </div>
			        </div>
			        <?php echo form_error('email'); ?>
			        <div class="row">
			          <div class="col-12">
			            <button type="submit" class="btn btn-primary btn-block">Recuperar contraseña en mi mail <i class="fas fa-unlock-alt"></i></button>
			          </div>
			          <!-- /.col -->
			        </div>
			      </form>

			      <p class="mt-3 mb-1">
			        <a href="<?php echo base_url('auth/login')?>"><i class="fas fa-arrow-left"></i> Volver</a>
			      </p>
			       
			    </div>
			    <!-- /.login-card-body -->
			  </div>
			</div>
			<!-- /.login-box -->
	    <?php 
	    } 
	    ?>
	</body>
</html>
