<div class="Wrapper">
  <div class="content-wrapper margenResponsive">
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-12 pl-3">
            
            <?php if ($this->session->flashdata('exito_user_nuevo')){?>
              <br>
              <div class="alert alert-success"><?php echo $this->session->flashdata('exito_user_nuevo');?></div>
            <?php }?>

            <h1><i class="fas fa-user-tag"></i> Gestionar clientes</h1>
            <h7>Gestionar clientes <i class="fas fa-chevron-right"></i></h7><a type="button" id="agregar_categoria"  class="agregar_categoria" data-toggle="modal" data-target="#modal-lg">  <i class="fas fa-user-plus"></i> Nuevo cliente</a>
          
          </div>    
        </div>
      </div>
    </section> 
    
    <section class="content">
      <div class="container-fluid">
        <div class="invoice p-2 mb-2">
          <div class="row">
            <div class="col-md-12 table-responsive"> 
              <table id="table_id" class="display table-striped">
                
                <thead>
                  <tr>
                    <th><h4><a class='badge badge-light'><i class="fas fa-user-tag"></i> ID</a></h4></th> 
                    <th><h4><a class='badge badge-light'><i class="fas fa-signature"></i> Nombre</a></h4></th>
                    <th><h4><a class='badge badge-light'><i class='fab fa-whatsapp'></i> Teléfono</a></h4></th>  
                    <th><h4><a class='badge badge-light'><i class="fas fa-passport"></i> DNI</a></h4></th>     
                    <th><h4><a class='badge badge-light'><i class="fas fa-search-location"></i> Dirección</a></h4></th>
                    <th><h4><a class='badge badge-light'><i class="fas fa-map-marked-alt"></i> Cambiar ubicación</a></h4></th>
                    <th><h4><a class='badge badge-light'><i class="fas fa-user-edit"></i> Cambiar datos</a></h4></th>
                    <th><h4><a class='badge badge-light'><i class="fas fa-user-slash"></i> Eliminar</a></h4></th>
                  </tr>
                </thead>
                
                <tbody>
                  <?php      
                    foreach ($users as $row) 
                    {              
                      echo "<tr>";
                      
                        echo "<td><strong>".$row->id_persona."</strong></td>";
                        echo "<input type='hidden' class='id_persona' value='".$row->id_persona."'</input>";

                        echo "<td><strong>".$row->Apellido.", ".$row->Nombres."</strong></td>";
                        echo "<input type='hidden' class='id_nombre' value='".$row->Nombres."'</input>";
                        echo "<input type='hidden' class='id_apellido' value='".$row->Apellido."'</input>";

                        echo "<td><strong>".$row->area_telefono.$row->telefono."</strong></td>";
                        echo "<input type='hidden' class='id_area_tel' value='".$row->area_telefono."'</input>";
                        echo "<input type='hidden' class='id_telefono' value='".$row->telefono."'</input>";

                        if ($row->DNI == 0)
                        {
                          $DNI = "no especificado";
                        }
                        else
                        {
                          $DNI = $row->DNI;
                        };
                        echo "<td><strong>".$DNI."</strong></td>";
                        echo "<input type='hidden' class='id_DNI' value='".$row->DNI."'</input>";
                        
                        echo "<td>"."<button style='width: 100%;' class='verMapaGoogle btn btn-primary'>Ver mapa <i class='fas fa-globe-africa'></i></button>"."</td>";
                        
                        echo "<input type='hidden' class='id_latitud' value='".$row->latitud."'</input>";
                        echo "<input type='hidden' class='id_longitud' value='".$row->longitud."'</input>";
                        echo "<input type='hidden' class='id_calle' value='".$row->calle_direccion."'</input>";
                        echo "<input type='hidden' class='id_numero' value='".$row->numero_direccion."'</input>";
                        echo "<input type='hidden' class='id_ciudad' value='".$row->ciudad."'</input>";
                        echo "<input type='hidden' class='id_provincia' value='".$row->provincia."'</input>";

                        echo "<td><button style='width: 100%;' class='actualizarUbicacion btn btn-outline-primary'><i class='fas fa-map-marked-alt'></i> Cambiar ciudad y provincia</button>";
                        ?>
                          <button style='width: 100%;' class='cambiarDireccionManualmente btn btn-outline-primary'> Cambiar dirección <i class="fas fa-map-marker-alt"></i></button></td>
                        <?php

                        echo "<td><button style='width: 100%;' class='actualizarDatos btn btn-warning'><i class='fas fa-user-edit'></i> Cambiar datos</button></td>";

                        ?>
                          <td>
                            <button style='width: 100%;' class='eliminarUsuario btn btn-outline-danger'><i class="fas fa-user-times"></i> Eliminar</button>
                          </td>
                        <?php
                          
                      echo "</tr>"; 
                    }; 
                  ?>      
                </tbody>
              </table>            
            </div>
          </div>
        </div>
      </div>

      <div class="modal fade" id="modal_ver_mapa">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Ver ubicación <i class='fas fa-globe-africa'></i></h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            
            <div class="modal-body" id="modal_body_ver_mapa">
           
            </div>
            
            <div class="modal-footer justify-content">
              <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>            
            </div>
          </div>
        </div>
      </div>

      <?php echo form_open('Auth/cambiar_rol_de_usuario'); ?>
      <div class="modal fade" id="modal_tipo_usuario">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title"><i class='far fa-id-card'></i> Cambiar rol</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            
            <div class="modal-body" id="modal_body_tipo_usuario">
              
            </div>
            
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
              <button type="submit" class="btn btn-primary"><i class="fas fa-edit"></i> Actualizar</button>
            </div>
          </div>
        </div>
      </div>
      <?php echo form_close(); ?>

      <?php echo form_open('Auth/eliminarUsuario'); ?>
      <div class="modal fade" id="modalEliminarUsuario">
        <div class="modal-dialog">
          <div class="modal-content ">
            <div class="modal-header bg-danger">
              <h4 class="modal-title"><i class="fas fa-user-times"></i> Dar de baja persona</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
              
            <div class="modal-body" id="modalBodyEliminarUsuario">
              <h3>¿Está seguro/a que desea dar de baja a esta persona del sistema?</h3>
              <input type="hidden" id='idPersonaEliminarUsuario' name="idPersonaEliminarUsuario">
            </div>
            
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
              <button type="submit" class="btn btn-danger"><i class="fas fa-user-times"></i> Sí, eliminar persona</button>
            </div>
          </div>
        </div>
      </div>
      <?php echo form_close(); ?>

      <?php echo form_open('Auth/actualizar_datos_de_usuario'); ?>
      <div class="modal fade" id="modal_cambiar_datos_usuario">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title"><i class='fas fa-user-edit'></i> Actualizar datos del cliente</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            
            <div class="modal-body" id="modal_body_cambiar_datos_usuario">
              <div class="row">

                <input type="hidden" value='1' class="form-control" name="cliente_o_empleado_dato" placeholder="Nombre">

                <div class="col-md-6">
                  <label for="exampleInputEmail1">Nombre:</label>
                  <input required="" type="text" class="form-control" name="nombre_modal_act" id="nombre_modal_act" placeholder="Nombre">
                </div>
                <div class="col-md-6"> 
                  <label for="exampleInputEmail1">Apellido:</label>
                  <input required="" type="text" class="form-control" name="apellido_modal_act" id="apellido_modal_act" placeholder="Apellido">
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <label for="exampleInputEmail1">Área tel.:</label>
                  <input required="" type="number" class="form-control" name="area_modal_act" id="area_modal_act" placeholder="Area">
                </div>
                <div class="col-md-6"> 
                  <label for="exampleInputEmail1">Teléfono:</label>
                  <input required="" type="number" class="form-control" name="tel_modal_act" id="tel_modal_act" placeholder="Telefono">
                </div>
              </div>
              <div class="row">
                <div class="col-md-3"> 
                </div>
                <div class="col-md-6">
                  <label for="exampleInputEmail1">DNI:</label>
                  <input required="" type="number" class="form-control" name="DNI_modal_act" id="DNI_modal_act" placeholder="DNI">
                </div>
                <div class="col-md-3"> 
                </div>
              </div>
            </div>

            <input type='hidden' id="id_persona_act_modal" name="id_persona_modal_act"></input>

            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
              <button type="submit" class="btn btn-primary"><i class="fas fa-edit"></i> Actualizar</button>
            </div>
          </div>
        </div>
      </div>
      <?php echo form_close(); ?>

      <?php echo form_open('Auth/actualizar_ubicacion'); ?>
      <div class="modal fade" id="modal_cambiar_ubicacion">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title"><i class="fas fa-map-marked-alt"></i> Actualizar ubicación</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            
            <div class="modal-body" id="modal_body_cambiar_ubicacion">
              <?php /*
              <div class="row">

                <div class="col-md-6">
                  <label for="exampleInputEmail1">Calle:</label>
                  <input required="" type="text" class="form-control" name="calle" id="calle" placeholder="Calle">
                </div>
                <div class="col-md-6"> 
                  <label for="exampleInputEmail1">Altura:</label>
                  <input required="" type="number" class="form-control" name="altura" id="altura" placeholder="Altura">
                </div>
              </div>
              */
              ?>

              <div class="row">
                <input type="hidden" class="form-control" id='id_persona_ubic' name="id_persona" placeholder="Nombre">
                <div class="col-md-6">
                  <label for="exampleInputEmail1">Ciudad seleccionada:</label>
                  <input readonly="" type="text" class="form-control" id="ciudad_ubic" placeholder="">
                </div>
                <div class="col-md-6"> 
                  <label for="exampleInputEmail1">Provincia seleccionada:</label>
                  <input readonly="" type="text" class="form-control" id="provincia_ubic" placeholder="">
                </div>
              </div>
              <hr>
              <div class="row">
                <div class="col-12"> 
                  <label name="" for="exampleInputEmail1">Cambiar ciudad y provincia:</label>
                  <div class="form-group">
                    <div name="div_select2" style="" id="div_select2" class="form-group div_select2">
                      <select style='width: 100%;' id='ciudadModificada' class="mapa_argentino custom-select form-control form-control-border" name="id_localidad">
                        <option value='disabled'>⚠ Seleccione ciudad</option>
                        <?php  
                          foreach ($mapa_argentina as $row) 
                          {
                            echo "<option value=".$row->id_localidad.">🌆 ".$row->nombre_ciudad." •  ".$row->nombre_provincia." • "."📪 ".$row->codigo_postal."</option>";
                          }; 
                        ?>                        
                      </select>
                    </div>
                  </div>
                </div> 
              </div>
              
            </div>

            <input type='hidden' id="id_persona_act_modal" name="id_persona_modal_act"></input>

            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
              <button id='btnCambiarCiudadPersona' type="submit" class="btn btn-primary"><i class="fas fa-edit"></i> Actualizar</button>
            </div>
          </div>
        </div>
      </div>
      <?php echo form_close(); ?>

      <?php echo form_open('Auth/actualizar_direccion'); ?>
      <div class="modal fade" id="modal_cambiar_direccion">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title"><i class="fas fa-map-marker-alt"></i> Actualizar dirección</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            
            <div class="modal-body" id="modal_body_cambiar_direccion">
              
              <div class="row">
                <input type="hidden" class="form-control" id='id_persona_ubicacion' name="id_persona" placeholder="Nombre">
                <div class="col-md-6">
                  <label for="exampleInputEmail1">Calle:</label>
                  <input required="" type="text" class="form-control" name="calle" id="calle" placeholder="Calle">
                </div>
                <div class="col-md-6"> 
                  <label for="exampleInputEmail1">Altura:</label>
                  <input required="" type="number" class="form-control" name="altura" id="altura" placeholder="Altura">
                </div>
              </div>
              
            </div>

            <input type='hidden' id="id_persona_act_modal" name="id_persona_modal_act"></input>

            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
              <button type="submit" class="btn btn-primary"><i class="fas fa-edit"></i> Actualizar</button>
            </div>
          </div>
        </div>
      </div>
      <?php echo form_close(); ?>

      <?php echo form_open('Auth/cliente_nuevo'); ?>
        <div class="modal fade" id="modal-lg">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
              
              <div class="modal-header">
                <h4 class="modal-title"><i class="fas fa-user-plus"></i> Nuevo cliente</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              
              <div class="modal-body">

                <div class="row">
                  <div class="col-md-4">
                    <label for="exampleInputEmail1">Nombre:</label>
                    <input required="" type="text" class="form-control" name="nombre_modal" placeholder="Nombre">
                  </div>
                  <div class="col-md-4"> 
                    <label for="exampleInputEmail1">Apellido:</label>
                    <input required="" type="text" class="form-control" name="apellido_modal" placeholder="Apellido">
                  </div>

                  <div style="display: none" id="ubicacion_actual_OFF" class="col-sm-4">
                          
                    <label for="exampleInputEmail1"> Estoy en el local del cliente:</label>
                    <input type="hidden" id="geo_lat_cliente"  class="form-control" name="lat_modal" placeholder="">
                    <input type="hidden" id="geo_long_cliente" class="form-control" name="long_modal" placeholder="">
                    <div name='boton_geo' id='boton_geo' style="width: 77%;" class="btn btn-success" onclick="geoFindMe()">                   
                      Geoubicación OK <i class="fas fa-check-circle"></i>    
                    </div>
                          
                    <div name='deshacer_geo' id='deshacer_geo' style="width: 21%;" class="btn btn-danger" onclick="deshacerGeo()">                   
                      <i class="fas fa-backspace"></i>   
                    </div>

                  </div>

                  <div style="display: visible" id="ubicacion_actual_ON" class="col-sm-4">
                          
                    <label for="exampleInputEmail1"> Estoy en el local del cliente:</label>
                    <div name='boton_geo' id='boton_geo' style="width: 100%;" class="btn btn-default" onclick="geoFindMe()">                   
                      <i class="fas fa-globe-americas"></i> Dar mi ubicación actual    
                    </div>

                  </div>

                </div>

                <div class="row">
                  <div class="col-md-6"> 
                    <label name="label_calle_dir_modal" id="label_calle_direccion_modal" for="exampleInputEmail1">Calle:</label>
                    <input required="" type="text" class="form-control" name="calle_dir_modal" id="calle_dir_modal" placeholder="Calle">
                  </div>
                  <div class="col-md-6"> 
                    <label name="label_num_dir_modal" id="label_num_direccion_modal" for="exampleInputEmail1">Número:</label>
                    <input required="" type="number" class="form-control" name="num_direccion_modal" id="num_direccion_modal" placeholder="Número">
                  </div>
                </div>

                <div class="row">
                  <div class="col-12"> 
                    <label name="" for="exampleInputEmail1">Seleccionar ciudad y provincia:</label>
                    <div class="form-group">
                      <div name="div_select2" style="" id="div_select2" class="form-group div_select2">
                        <select style='width: 100%;' id='ciudadSeleccionada' class="mapa_argentino custom-select form-control form-control-border" name="id_localidad">
                          <option value='disabled'>⚠ Seleccione ciudad</option>
                          <?php  
                            foreach ($mapa_argentina as $row) 
                            {
                              echo "<option value=".$row->id_localidad.">🌆 ".$row->nombre_ciudad." •  ".$row->nombre_provincia." • "."📪 ".$row->codigo_postal."</option>";
                            }; 
                          ?>                        
                        </select>
                      </div>
                    </div>
                  </div> 
                </div>

                <div class="row">
                  <div class="col-md-3">
                    <label for="exampleInputEmail1">Código (sin 0):</label>
                    <input required="" type="number" class="form-control" name="codigo_tel_modal" placeholder="Código">
                  </div>
                  <div class="col-md-5">
                    <label for="exampleInputEmail1">Teléfono (sin 15):</label>
                    <input required="" type="number" class="form-control" name="tel_modal" placeholder="Teléfono">
                  </div>
                  <div class="col-md-4">
                    <label for="exampleInputEmail1">DNI (opcional):</label>
                    <input type="number" class="form-control" name="dni_modal" placeholder="Documento">
                  </div>
                </div>

                <div class="modal-footer justify-content-between">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                  <button type="submit" id='btnGuardarPersona' class="btn btn-primary"><i class="fas fa-plus"></i> Añadir nuevo cliente</button>
                </div>

              </div>
            </div>
          </div>
        </div>
      <?php echo form_close(); ?>

    </section>
  </div>
</div>

<style>
  @media (min-width: 992px)
  {
      .sidebar-mini.sidebar-collapse .content-wrapper 
      {
          margin-left: -1.0rem!important; 
       }
  }
</style>

<script type="text/javascript">
  $(document).ready( function () 
  {
    $('.mapa_argentino').select2();

    const buttonGuardarPersona = $("#btnGuardarPersona");
    buttonGuardarPersona.hide();
    var estado = $("#ciudadSeleccionada").val();
    //alert(estado);
    $("#ciudadSeleccionada").change(function(){
      estado = $("#ciudadSeleccionada").val();
      //alert(estado);

      if (estado =='disabled')
      {
        buttonGuardarPersona.hide();
      }
      else
      {
        buttonGuardarPersona.show();
      }
    });

    const buttonCambiarCiudadPersona = $("#btnCambiarCiudadPersona");
    buttonCambiarCiudadPersona.hide();
    var cambiarCiudad = $("#ciudadModificada").val();
    //alert(estado);
    $("#ciudadModificada").change(function(){
      cambiarCiudad = $("#ciudadModificada").val();
      //alert(estado);

      if (cambiarCiudad =='disabled')
      {
        buttonCambiarCiudadPersona.hide();
      }
      else
      {
        buttonCambiarCiudadPersona.show();
      }
    });

    $('#table_id').DataTable({
        
        language: {

          "decimal": "",
          "emptyTable": "No hay información",
          "info": "Mostrando _START_ a _END_ de un total de _TOTAL_ Entradas",
          "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
          "infoFiltered": "(Filtrado de _MAX_ total entradas)",
          "infoPostFix": "",
          "thousands": ",",
          "lengthMenu": "Mostrar _MENU_ Entradas",
          "loadingRecords": "Cargando...",
          "processing": "Procesando...",
          "search": "Buscar:",
          "zeroRecords": "Sin resultados encontrados",
          "paginate": {
            "first": "Primero",
            "last": "Ultimo",
            "next": "Siguiente",
            "previous": "Anterior"

          }
        }
      });
  });      
</script>

<script>
    $("#table_id").on("click", ".cambiarRol", function(event)
    { 
      var tr = $(this).closest('tr');        
      var id_persona = $(tr).find('.id_persona').val();
      var id_rol = $(tr).find('.id_rol').val()

      var html = "<input type='hidden' name='id_persona_modal_rol' value='"+id_persona+"'>";
      html += "<select id='nuevo_rol_modal' class='nuevo_rol_modal form-control' name='rol_usuario_modal' style='width: 100%'><option value='0'>Administrador</option><option value='1'>Preventista</option><option value='2'>Repartidor</option><option value='3'>Cliente</option></select>";

      $('#modal_body_tipo_usuario').html(html);
      $('#nuevo_rol_modal').select2();
      $('#nuevo_rol_modal').val(id_rol).trigger('change.select2');
      $('#modal_tipo_usuario').modal('show'); 
    });

    $("#table_id").on("click", ".actualizarDatos", function(event)
    { 
      var tr = $(this).closest('tr');        
      var id_persona = $(tr).find('.id_persona').val();
      var area_telefono = $(tr).find('.id_area_tel').val();
      var tel = $(tr).find('.id_telefono').val();
      var nombre = $(tr).find('.id_nombre').val();
      var apellido = $(tr).find('.id_apellido').val();
      var dni = $(tr).find('.id_DNI').val();

      document.getElementById("id_persona_act_modal").value=id_persona;
      document.getElementById("area_modal_act").value=area_telefono;
      document.getElementById("tel_modal_act").value=tel;
      document.getElementById("nombre_modal_act").value=nombre;
      document.getElementById("apellido_modal_act").value=apellido;
      document.getElementById("DNI_modal_act").value=dni;
   
      $('#modal_cambiar_datos_usuario').modal('show'); 
    });

    $("#table_id").on("click", ".eliminarUsuario", function(event)
    { 
      var tr = $(this).closest('tr');        
      var id_persona = $(tr).find('.id_persona').val();
      //var area_telefono = $(tr).find('.id_area_tel').val();
      //var tel = $(tr).find('.id_telefono').val();
      //var nombre = $(tr).find('.id_nombre').val();
      //var apellido = $(tr).find('.id_apellido').val();
      //var dni = $(tr).find('.id_DNI').val();

      document.getElementById("idPersonaEliminarUsuario").value=id_persona;
   
      $('#modalEliminarUsuario').modal('show'); 
    });

    $("#table_id").on("click", ".verMapaGoogle", function(event)
    { 
      var tr = $(this).closest('tr');        
      var latitud = 0;
      var longitud = 0;
      latitud = $(tr).find('.id_latitud').val();
      longitud = $(tr).find('.id_longitud').val();

      var calle = "";
      var numero = "";
      var ciudad = "";
      var provincia = "";
      var pais = "Argentina";
      calle = $(tr).find('.id_calle').val();  
      numero = $(tr).find('.id_numero').val();
      ciudad = $(tr).find('.id_ciudad').val();
      provincia = $(tr).find('.id_provincia').val();

      var html = '';

      if (latitud != "" && longitud != "")
      {
        html += "<div id='map-container-google-1' class='z-depth-1-half map-container' style='width: 100%'><iframe width='100%' height='300' frameborder='0' scrolling='no' marginheight='0' marginwidth='0' src='https://maps.google.com/maps?width=100%25&amp;height=600&amp;hl=es&amp;q="+latitud+","+longitud+"%20+(Ubicacion)&amp;t=&amp;z=14&amp;ie=UTF8&amp;iwloc=B&amp;output=embed'></iframe></div>";
        $('#modal_body_ver_mapa').html(html);
        $('#modal_ver_mapa').modal('show');  
      }
      else 
      { 
        html += "<div id='map-container-google-1' class='z-depth-1-half map-container' style='width: 100%'><iframe width='100%' height='300' frameborder='0' scrolling='no' marginheight='0' marginwidth='0' src='https://maps.google.com/maps?width=100%25&amp;height=600&amp;hl=es&amp;q="+calle+",%20"+numero+"%20"+ciudad+"%20"+provincia+",%20"+pais+"+(Mi%20nombre%20de%20egocios)&amp;t=&amp;z=14&amp;ie=UTF8&amp;iwloc=B&amp;output=embed'></iframe></div>";
        $('#modal_body_ver_mapa').html(html);
        $('#modal_ver_mapa').modal('show'); 
      }
 
    });

    $("#table_id").on("click", ".actualizarUbicacion", function(event)
    { 
      var tr = $(this).closest('tr');        
      var id_persona = $(tr).find('.id_persona').val();

      //var calle = $(tr).find('.id_calle').val(); 
      //var altura = $(tr).find('.id_numero').val();
      
      var ciudad = $(tr).find('.id_ciudad').val();
      var provincia = $(tr).find('.id_provincia').val();
      document.getElementById("ciudad_ubic").value=ciudad;
      document.getElementById("provincia_ubic").value=provincia; 

      //if (calle == "")
      //{ 
        //alert('Los datos de calle y altura no fueron suministrados porque se tienen almacenados datos de latitud y longitud. Si estos datos no son correctos, tenga presente que al ingresar una calle y altura sobre este registro de usuario -y dar click en el botón de actualizar-, se borrarán los datos de latitud y longitud previamente ingresados.');
      //}

      //document.getElementById("calle").value=calle;
      //document.getElementById("altura").value=altura;  
      document.getElementById("id_persona_ubic").value=id_persona;     
   
      $('#modal_cambiar_ubicacion').modal('show'); 
    });

    $("#table_id").on("click", ".cambiarDireccionManualmente", function(event)
    { 
      var tr = $(this).closest('tr');        
      var id_persona = $(tr).find('.id_persona').val();

      var calle = $(tr).find('.id_calle').val(); 
      var altura = $(tr).find('.id_numero').val();
      
      //var ciudad = $(tr).find('.id_ciudad').val();
      //var provincia = $(tr).find('.id_provincia').val();
      //document.getElementById("ciudad_ubic").value=ciudad;
      //document.getElementById("provincia_ubic").value=provincia; 

      if (calle == "")
      { 
        alert('Los datos de calle y altura no fueron suministrados porque se tienen almacenados datos de latitud y longitud. Si estos datos no son correctos, tenga presente que al ingresar una calle y altura sobre este registro de usuario -y dar click en el botón de actualizar-, se borrarán los datos de latitud y longitud previamente ingresados.');
      }

      document.getElementById("calle").value=calle;
      document.getElementById("altura").value=altura;  
      document.getElementById("id_persona_ubicacion").value=id_persona;     
   
      $('#modal_cambiar_direccion').modal('show'); 
    });

    $("#table_id").on("click", ".verMapaGoogle", function(event)
    { 
      var tr = $(this).closest('tr');        
      var latitud = 0;
      var longitud = 0;
      latitud = $(tr).find('.id_latitud').val();
      longitud = $(tr).find('.id_longitud').val();

      var calle = "";
      var numero = "";
      var ciudad = "";
      var provincia = "";
      var pais = "Argentina";
      calle = $(tr).find('.id_calle').val();  
      numero = $(tr).find('.id_numero').val();
      ciudad = $(tr).find('.id_ciudad').val();
      provincia = $(tr).find('.id_provincia').val();

      var html = '';

      if (latitud != "" && longitud != "")
      {
        html += "<div id='map-container-google-1' class='z-depth-1-half map-container' style='width: 100%'><iframe width='100%' height='300' frameborder='0' scrolling='no' marginheight='0' marginwidth='0' src='https://maps.google.com/maps?width=100%25&amp;height=600&amp;hl=es&amp;q="+latitud+","+longitud+"%20+(Ubicacion)&amp;t=&amp;z=14&amp;ie=UTF8&amp;iwloc=B&amp;output=embed'></iframe></div>";
        $('#modal_body_ver_mapa').html(html);
        $('#modal_ver_mapa').modal('show');  
      }
      else 
      { 
        html += "<div id='map-container-google-1' class='z-depth-1-half map-container' style='width: 100%'><iframe width='100%' height='300' frameborder='0' scrolling='no' marginheight='0' marginwidth='0' src='https://maps.google.com/maps?width=100%25&amp;height=600&amp;hl=es&amp;q="+calle+",%20"+numero+"%20"+ciudad+"%20"+provincia+",%20"+pais+"+(Mi%20nombre%20de%20egocios)&amp;t=&amp;z=14&amp;ie=UTF8&amp;iwloc=B&amp;output=embed'></iframe></div>";
        $('#modal_body_ver_mapa').html(html);
        $('#modal_ver_mapa').modal('show'); 
      }
 
    });

    function geoFindMe() {
      var output = document.getElementById("out");

      if (!navigator.geolocation){
        boton_geo.style.backgroundColor= "red";
        boton_geo.style.color = "white";
        var geo_button = document.getElementById('boton_geo'); 
        geo_button.innerHTML = 'Geoubicación no funciona en su tel <i class="fas fa-times"></i>';
        alert("Geoubicación no funciona en su tel.No se pudo obtener la geolocalización, vuelva a intentarlo introduciendo la dirección manualmente");
      }

      function success(position) {
        var latitude  = position.coords.latitude;
        var longitude = position.coords.longitude;
        document.getElementById("geo_lat_cliente").value=latitude; 
        document.getElementById("geo_long_cliente").value=longitude;   

        $("#calle_dir_modal").prop('required',false); 
        $("#num_direccion_modal").prop('required',false); 
        $("#ciudad_modal").prop('required',false); 
        $("#provincia_modal").prop('required',false); 
        $("#calle_dir_modal").hide();
        $("#num_direccion_modal").hide();
        $("#ciudad_modal").hide();
        $("#provincia_modal").hide();
        $("#label_provincia_modal").hide();
        $("#label_ciudad_modal").hide();
        $("#label_num_direccion_modal").hide();
        $("#label_calle_direccion_modal").hide();  

        $("#ubicacion_actual_ON").hide();
        $("#ubicacion_actual_OFF").show();
      };

      function error() {
        boton_geo.style.backgroundColor= "red";
        boton_geo.style.color = "white";
        var geo_button = document.getElementById('boton_geo'); 
        geo_button.innerHTML = 'Debe autorizar la geoubicación <i class="fas fa-exclamation-triangle"></i>';
        alert("Debe autorizar la geoubicación. No se pudo obtener la geolocalización, vuelva a intentarlo");
      };

      navigator.geolocation.getCurrentPosition(success, error);
    }

    function deshacerGeo() 
    {
      $("#calle_dir_modal").prop('required',true); 
      $("#num_direccion_modal").prop('required',true); 
      $("#ciudad_modal").prop('required',true); 
      $("#provincia_modal").prop('required',true); 
      $("#calle_dir_modal").show();
      $("#num_direccion_modal").show();
      $("#ciudad_modal").show();
      $("#provincia_modal").show();
      $("#label_provincia_modal").show();
      $("#label_ciudad_modal").show();
      $("#label_num_direccion_modal").show();
      $("#label_calle_direccion_modal").show(); 

      $("#ubicacion_actual_ON").show();
      $("#ubicacion_actual_OFF").hide(); 

      document.getElementById("geo_lat_cliente").value=""; 
      document.getElementById("geo_long_cliente").value="";
    }
</script>

