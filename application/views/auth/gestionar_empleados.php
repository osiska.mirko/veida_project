<div class="Wrapper">
  <div class="content-wrapper margenResponsive">
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-12 pl-3">
            
            <?php if ($this->session->flashdata('exito_user_nuevo')){?>
              <br>
              <div class="alert alert-success"><?php echo $this->session->flashdata('exito_user_nuevo');?></div>
            <?php }?>

            <?php if ($this->session->flashdata('error')){?>
              <br>
              <div class="alert alert-danger"><?php echo $this->session->flashdata('error');?></div>
            <?php }?>

            <h1><i class="fas fa-user-tie"></i> Gestionar empleados</h1>
            <h7>Gestionar empleados <i class="fas fa-chevron-right"></i></h7><a type="button" id="agregar_categoria"  class="agregar_categoria" data-toggle="modal" data-target="#modal-lg">  <i class="fas fa-user-plus"></i> Nuevo empleado</a>
          
          </div>    
        </div>
      </div>
    </section> 
    
    <section class="content">
      <div class="container-fluid">
        <div class="invoice p-2 mb-2">
          <div class="row">
            <div class="col-md-12 table-responsive"> 
              <table id="table_id" class="display table-striped">
                
                <thead>
                  <tr>

                    <th><h4><a class='badge badge-light'><i class="fas fa-user-tie"></i> ID</a></h4></th> 
                    <th><h4><a class='badge badge-light'><i class="fas fa-signature"></i> Nombre</a></h4></th>
                    <th><h4><a class='badge badge-light'><i class='fab fa-whatsapp'></i> Teléfono</a></h4></th> 
                    <th><h4><a class='badge badge-light'><i class="fas fa-passport"></i> DNI</a></h4></th> 
                    <th><h4><a class='badge badge-light'><i class="fas fa-sign-in-alt"></i> Datos de sesión</a></h4></th>   
                    <th><h4><a class='badge badge-light'><i class="fas fa-user-edit"></i> Datos personales</a></h4></th>
                    <th><h4><a class='badge badge-light'><i class="fas fa-user-cog"></i> Rol</a></h4></th>  

                  </tr>
                </thead>
                
                <tbody>
                  <?php      
                    foreach ($users as $row) 
                    {              
                      echo "<tr> ";
                      
                        echo "<td><strong>".$row->id_persona."</strong></td>";
                        echo "<input type='hidden' class='id_persona' value='".$row->id_persona."'></input>";
                        echo "<input type='hidden' class='id_user' value='".$row->id_usuario."'></input>";
                        echo "<input type='hidden' class='id_user_correo' value='".$row->email."'></input>";                      

                        echo "<td><strong>".$row->Apellido.", ".$row->Nombres."</strong></td>";
                        echo "<input type='hidden' class='id_nombre' value='".$row->Nombres."'></input>";
                        echo "<input type='hidden' class='id_apellido' value='".$row->Apellido."'></input>";

                        echo "<td><strong>".$row->area_telefono.$row->telefono."<strong></td>";
                        echo "<input type='hidden' class='id_area_tel' value='".$row->area_telefono."'></input>";
                        echo "<input type='hidden' class='id_telefono' value='".$row->telefono."'></input>";

                        if ($row->DNI == 0)
                        {
                          $DNI = "no especificado";
                        }
                        else
                        {
                          $DNI = $row->DNI;
                        };
                        echo "<td><strong>".$DNI."</strong></td>";
                        echo "<input type='hidden' class='id_DNI' value='".$row->DNI."'</input>";

                        echo "<td><button style='width: 100%;' class='modificarInicioSesion btn btn-primary'><i class='fas fa-sign-in-alt'></i> Modificar datos</button></td>";

                        echo "<td><button style='width: 100%;' class='actualizarDatos btn btn-primary'><i class='fas fa-user-edit'></i> Cambiar datos</button></td>";

                        echo "<td><button style='width: 100%;' class='cambiarRol btn btn-primary'><i class='far fa-id-card'></i> ".($roles_de_users[$row->tipo_usuario]->nombre_del_rol)."</button></td>";
                        echo "<input type='hidden' class='id_rol' value='".$row->tipo_usuario."'</input>";
                              
                      echo "</tr>"; 
                    }; 
                  ?>      
                </tbody>
              </table>            
            </div>
          </div>
        </div>
      </div>

      <?php echo form_open('Auth/cambiar_rol_de_usuario'); ?>
      <div class="modal fade" id="modal_tipo_usuario">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title"><i class='far fa-id-card'></i> Cambiar rol</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            
            <div class="modal-body" id="modal_body_tipo_usuario">
              
            </div>
            
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
              <button type="submit" class="btn btn-primary"><i class="fas fa-edit"></i> Actualizar</button>
            </div>
          </div>
        </div>
      </div>
      <?php echo form_close(); ?>

      <?php echo form_open('Auth/modificar_datos_de_sesion'); ?>
      <div class="modal fade" id="modal_datos_de_sesion">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title"><i class="fas fa-sign-in-alt"></i> Cambiar datos de inicio de sesión</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            
            <div class="modal-body" id="modal_body_datos_de_sesion">
              
            </div>
            
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
              <button type="submit" class="btn btn-primary"><i class="fas fa-edit"></i> Actualizar</button>
            </div>
          </div>
        </div>
      </div>
      <?php echo form_close(); ?>

      <?php echo form_open('Auth/actualizar_datos_de_usuario'); ?>
      <div class="modal fade" id="modal_cambiar_datos_usuario">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title"><i class='fas fa-user-edit'></i> Actualizar datos del empleado</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            
            <div class="modal-body" id="modal_body_cambiar_datos_usuario">
              <div class="row">

                <input type="hidden" value='2' class="form-control" name="cliente_o_empleado_dato" placeholder="Nombre">

                <div class="col-md-6">
                  <label for="exampleInputEmail1">Nombre:</label>
                  <input required="" type="text" class="form-control" name="nombre_modal_act" id="nombre_modal_act" placeholder="Nombre">
                </div>
                <div class="col-md-6"> 
                  <label for="exampleInputEmail1">Apellido:</label>
                  <input required="" type="text" class="form-control" name="apellido_modal_act" id="apellido_modal_act" placeholder="Apellido">
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <label for="exampleInputEmail1">Área tel.:</label>
                  <input required="" type="number" class="form-control" name="area_modal_act" id="area_modal_act" placeholder="Area">
                </div>
                <div class="col-md-6"> 
                  <label for="exampleInputEmail1">Teléfono:</label>
                  <input required="" type="number" class="form-control" name="tel_modal_act" id="tel_modal_act" placeholder="Telefono">
                </div>
              </div>
              <div class="row">
                <div class="col-md-3"> 
                </div>
                <div class="col-md-6">
                  <label for="exampleInputEmail1">DNI:</label>
                  <input required="" type="number" class="form-control" name="DNI_modal_act" id="DNI_modal_act" placeholder="DNI">
                </div>
                <div class="col-md-3"> 
                </div>
              </div>
            </div>

            <input type='hidden' id="id_persona_act_modal" name="id_persona_modal_act"></input>

            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
              <button type="submit" class="btn btn-primary"><i class="fas fa-edit"></i> Actualizar</button>
            </div>
          </div>
        </div>
      </div>
      <?php echo form_close(); ?>

      <?php echo form_open('Auth/empleado_nuevo'); ?>
        <div class="modal fade" id="modal-lg">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
              
              <div class="modal-header">
                <h4 class="modal-title"><i class="fas fa-user-plus"></i> Empleado nuevo</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              
              <div class="modal-body">

                <div class="row">
                  <label class="badge badge-light"><i class='fas fa-user-edit'></i> Datos personales</label>
                </div>

                <div class="row">
                  <div class="col-md-4">
                    <label for="exampleInputEmail1">Nombre:</label>
                    <input required="" type="text" class="form-control" name="nombre_modal" placeholder="Nombre">
                  </div>
                  <div class="col-md-4"> 
                    <label for="exampleInputEmail1">Apellido:</label>
                    <input required="" type="text" class="form-control" name="apellido_modal" placeholder="Apellido">
                  </div>
                  <div class="col-md-4"> 
                    <label for="exampleInputEmail1">Rol:</label>
                    <select id='rol_modal' class='nuevo_rol_modal form-control' name='rol_usuario_modal' style='width: 100%'><option value='1'>Preventista</option><option value='2'>Repartidor</option></select>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-3">
                    <label for="exampleInputEmail1">Código (sin 0):</label>
                    <input required="" type="number" class="form-control" name="codigo_tel_modal" placeholder="Código">
                  </div>
                  <div class="col-md-5">
                    <label for="exampleInputEmail1">Teléfono (sin 15):</label>
                    <input required="" type="number" class="form-control" name="tel_modal" placeholder="Teléfono">
                  </div>
                  <div class="col-md-4">
                    <label for="exampleInputEmail1">DNI (opcional):</label>
                    <input type="number" class="form-control" name="dni_modal" placeholder="Documento">
                  </div>
                </div>

                <hr>
                
                <div class="row">
                  <label class="badge badge-light"><i class='fas fa-sign-in-alt'></i> Datos de inicio de sesión</label>
                </div>

                <div class="row">
                  <div class="col-md-6">
                    <label for="exampleInputEmail1">Email:</label>
                    <input required="" type="email" class="form-control email" name="email_modal" placeholder="Email">
                  </div>
                  <div class="col-md-6">
                    <label for="exampleInputEmail1">Nueva contraseña:</label>
                    <input required="" minlength="6" type="text" class="form-control" name="contra_modal" placeholder="Nueva contraseña">
                  </div>
                </div>

                <div class="modal-footer justify-content-between">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                  <button type="submit" class="btn btn-primary"><i class="fas fa-plus"></i> Añadir empleado nuevo</button>
                </div>

              </div>
            </div>
          </div>
        </div>
      <?php echo form_close(); ?>

    </section>
  </div>
</div>

<style>
  @media (min-width: 992px)
  {
      .sidebar-mini.sidebar-collapse .content-wrapper 
      {
          margin-left: -1.0rem!important; 
       }
  }
</style>

<script type="text/javascript">
              $(document).ready( function () 
              {
                
                $('#table_id').DataTable({
                    
                    language: {

                      "decimal": "",
                      "emptyTable": "No hay información",
                      "info": "Mostrando _START_ a _END_ de un total de _TOTAL_ Entradas",
                      "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
                      "infoFiltered": "(Filtrado de _MAX_ total entradas)",
                      "infoPostFix": "",
                      "thousands": ",",
                      "lengthMenu": "Mostrar _MENU_ Entradas",
                      "loadingRecords": "Cargando...",
                      "processing": "Procesando...",
                      "search": "Buscar:",
                      "zeroRecords": "Sin resultados encontrados",
                      "paginate": {
                        "first": "Primero",
                        "last": "Ultimo",
                        "next": "Siguiente",
                        "previous": "Anterior"

                      }
                    }
                  });
              });      
</script>

<script>
    $("#table_id").on("click", ".cambiarRol", function(event)
    { 
      var tr = $(this).closest('tr');        
      var id_persona = $(tr).find('.id_persona').val();
      var id_rol = $(tr).find('.id_rol').val();

      var html = "<input type='hidden' name='id_persona_modal_rol' value='"+id_persona+"'>";
      html += "<select id='nuevo_rol_modal' class='nuevo_rol_modal form-control' name='rol_usuario_modal' style='width: 100%'><option value='1'>Preventista</option><option value='2'>Repartidor</option></select>";

      $('#modal_body_tipo_usuario').html(html);
      $('#nuevo_rol_modal').select2();
      $('#nuevo_rol_modal').val(id_rol).trigger('change.select2');
      $('#modal_tipo_usuario').modal('show'); 
    });

    $("#table_id").on("click", ".modificarInicioSesion", function(event)
    { 
      var tr = $(this).closest('tr');        
      var id_persona = $(tr).find('.id_persona').val();

      var id_usuario = $(tr).find('.id_user').val();
      var id_user_correo = $(tr).find('.id_user_correo').val();

      var html = "<input type='hidden' name='id_persona_modal_modificar' value='"+id_persona+"'><input type='hidden' name='id_user_modal_rol' value='"+id_usuario+"'>";
      html += "\
        <div class='row'>\
          <div class='col-md-6'>\
            <label for='exampleInputEmail1'>Email:</label>\
            <input required='' type='email' value='"+id_user_correo+"' class='form-control email' name='email_modal' placeholder='Email'>\
          </div>\
          <div class='col-md-6'>\
            <label for='exampleInputEmail1'>Nueva contraseña:</label>\
            <input required='' minlength='6' type='text' class='form-control' name='contra_modal' placeholder='Nueva contraseña'>\
          </div>\
        </div>";

      $('#modal_body_datos_de_sesion').html(html);
      $('#modal_datos_de_sesion').modal('show'); 
    });

    $("#table_id").on("click", ".actualizarDatos", function(event)
    { 
      var tr = $(this).closest('tr');        
      var id_persona = $(tr).find('.id_persona').val();
      var area_telefono = $(tr).find('.id_area_tel').val();
      var tel = $(tr).find('.id_telefono').val();
      var nombre = $(tr).find('.id_nombre').val();
      var apellido = $(tr).find('.id_apellido').val();
      var dni = $(tr).find('.id_DNI').val();

      document.getElementById("id_persona_act_modal").value=id_persona;
      document.getElementById("area_modal_act").value=area_telefono;
      document.getElementById("tel_modal_act").value=tel;
      document.getElementById("nombre_modal_act").value=nombre;
      document.getElementById("apellido_modal_act").value=apellido;
      document.getElementById("DNI_modal_act").value=dni;
   
      $('#modal_cambiar_datos_usuario').modal('show'); 
    });

</script>

