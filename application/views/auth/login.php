<section class="content">
  <div class="d-flex justify-content-center" style="margin: 100px 0 0 0px;">
    <div class="form-signin">
      <div class="card card-outline card-primary">
        <div class="login-logo">
          <a href="#"><b>Distribuidora </b>VEIDA</a>
        </div>

        <div class="card-body">
          
          <p class="login-box-msg">Ingresá tu cuenta para iniciar sesión</p>

          <form action="<?php echo base_url('auth/login')?>" method="post">
            <div class="input-group mb-3">
              <input type="email" name="email" class="form-control" placeholder="email" value="<?php echo set_value('email'); ?>">
              <div class="input-group-append">
                <div class="input-group-text">
                  <span class="fas fa-envelope"></span>
                </div>
              </div>
            </div>
            
            <div class="input-group mb-3">
              <input type="password" name="password" class="form-control" placeholder="password" value="<?php echo set_value('password'); ?>">
              <div class="input-group-append">
                <div class="input-group-text">
                  <span class="fas fa-lock"></span>
                </div>
              </div>
            </div>
            
            <div class="row">
              <div class="col-12">
                
                <button type="submit"  class="btn btn-primary btn-block"><i class="fas fa-sign-in-alt"></i> Ingresar</button> 
           
                <p style="text-align: center;" class="mt-3 mb-3 text-muted"><a href="<?php echo base_url()?>auth/forgot"><i class="fas fa-unlock"></i> Recuperar mi contraseña</a></p>

                <hr>

                <p style="text-align: center;" class="mt-3 mb-3 text-muted"><a target="_blank" href="<?php echo base_url()?>downloads/Veida.apk"><i class="fas fa-cloud-download-alt"></i> Descargar la app para <i class="fab fa-android"></i> Android  <br><b><i class="fas fa-mobile-alt"></i> Veida app 1.0 </b></a></p>
                
              </div>
            </div>
            
            <?php print_r($error); ?>
          </form>
        </div>
      </div>
    </div>
  </div>
</section>

  