<section class="content">
  <div class="d-flex justify-content-center" style="margin: 150px 0 0 0px;">
    <div class="form-signin">
      <div class="card card-outline card-primary">
        <div class="card-header text-center">
          <a href="#"><b>Distribuidora </b>VEIDA</a>
        </div>
        <div class="card-body">
          
          <p class="login-box-msg">El link para recuperar tu contraseña fue enviado exitosamente a tu mail. Por favor, revisá el correo electrónico ingresado previamente.</p>
          
          <a href="<?php echo site_url('Auth') ?>">  	
          	<p><i class="fas fa-arrow-left"></i> Salir</p>
          </a>
          
        </div>
      </div>
    </div>
  </div>
</section>
