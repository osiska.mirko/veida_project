<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Distribuidora Veida</title>
        <!-- Favicon-->
        <link rel="icon" type="image/x-icon" href="<?php echo site_url('assets/logo_veida.png'); ?>" />
        <!-- Bootstrap icons-->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="<?php echo base_url('/assets/plugins/fontawesome-free/css/all.min.css') ?>">
        <!-- Google fonts-->
        <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css" />
        <!-- Core theme CSS (includes Bootstrap)-->
        <link href="<?php echo site_url('css/styles.css'); ?>" rel="stylesheet" />
    </head>
    <body>
        <!-- Navigation-->
        <nav class="navbar navbar-light bg-light static-top">
            <div class="container">
                <a class="navbar-brand" href="#!"><img width="30px" height="30px" src="<?php echo base_url('/assets/logo_veida.png')?>" alt="" style="opacity: .8;border-radius: 50%"> Distribuidora Veida</a>
                <a class="btn btn-primary" href="<?php echo base_url()?>auth/forgot"><i class="fas fa-unlock"></i> Cambiar contraseña</a>
            </div>
        </nav>
        <!-- Masthead-->
        <header class="masthead">
            <div class="container position-relative">
                <div class="row justify-content-center">
                    <div class="col-xl-6" style="opacity: .8;">
                        <div class="text-center text-white">
                            
                            <h1 class="mb-5"><img width="70px" height="70px" src="<?php echo base_url('/assets/logo_veida.png')?>" alt="" style="opacity: .7;border-radius: 50%"> Distribuidora Veida</h1>
                            
                            <form id="contactForm" class="form-subscribe" action="<?php echo base_url('auth/login')?>" method="post">
                                <!-- Email address input-->
                                <div class="row">
                                    <div class="col">
                                        <div class="input-group mb-3">
                                            <span class="input-group-text" id="basic-addon1"><i class="fas fa-envelope"></i></span>
                                            <input required="El email es requerido" class="form-control form-control-lg" name='email' type="email" placeholder="Email"/>
                                        </div>
                                    </div>  
                                </div>
                                
                                <div class="row">
                                    <div class="col">
                                        <div class="input-group mb-3">
                                            <span class="input-group-text" id="basic-addon1"><i class="fas fa-lock"></i></span>
                                            <input required="La contraseña es requerida" class="form-control form-control-lg" name="password" type="password" placeholder="Contraseña" />
                                        </div>
                                    </div>
                                </div>
   
                                <div>
                                    <div class="col-auto">
                                        <button style='width:46%' class="btn btn-primary btn-lg " type="submit"><i class="fas fa-sign-in-alt"></i> Ingresar</button>
                                    </div>
                                </div>
                                <br>
                                <div>
                                    <a class='btn btn-light' style="opacity: .9;" target="_blank" href="<?php echo base_url()?>downloads/Veida.apk"><i class="fas fa-cloud-download-alt"></i> Descargar la app para Android <i class="fab fa-android"></i></a>
                                </div>
                                
                            </form>
                            <br>
                            <?php 
                            if (!empty($error))
                            {
                            ?>
                            <i class="fas fa-exclamation-circle"></i> 
                            <?php
                                print_r($error);
                            };
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <!-- Icons Grid-->
        <section class="features-icons bg-light text-center">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="features-icons-item mx-auto mb-5 mb-lg-0 mb-lg-3">
                            <div class="features-icons-icon d-flex"><i class="bi bi-phone m-auto text-primary"></i></div>
                            <h3>Fully Responsive</h3>
                            <p class="lead mb-0">Se adapta a la pantalla de cualquier dispositivo.</p>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="features-icons-item mx-auto mb-5 mb-lg-0 mb-lg-3">
                            <div class="features-icons-icon d-flex"><i class="bi bi-hdd-network-fill m-auto text-primary"></i></div>
                            <h3>Centralizado</h3>
                            <p class="lead mb-0">Ventas, Productos, Precios, Stock y Reportes en un solo lugar.</p>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="features-icons-item mx-auto mb-5 mb-0 mb-lg-3">
                            <div class="features-icons-icon d-flex"><i class="bi bi-truck m-auto text-primary"></i></div>
                            <h3>Tiempo Real</h3>
                            <p class="lead mb-0">Mejora la eficiencia en el manejo de la información.</p>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="features-icons-item mx-auto mb-5 mb-0 mb-lg-3">
                            <div class="features-icons-icon d-flex"><i class="bi bi-globe m-auto text-primary"></i></div>
                            <h3>Geoubicación</h3>
                            <p class="lead mb-0">Datos de clientes útiles y precisos.</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Image Showcases-->
        <section class="showcase">
            <div class="container-fluid p-0">
                <div class="row g-0">
                    <?php $url1 = site_url('assets/img/bg-showcase-1.jpg'); ?>
                    <div class="col-lg-6 order-lg-2 text-white showcase-img" style="background-image: url(<?php echo $url1; ?>)"></div>
                    <div class="col-lg-6 order-lg-1 my-auto showcase-text">
                        <h2>Fully Responsive Design</h2>
                        <p class="lead mb-0">Utilice este sistema desde cualquier navegador web en una pc de escritorio, una tablet o un celular. <br>Descargue la App para Android <i class="fas fa-arrow-circle-down"></i> <a style='width: 100%' class='btn btn-light' target="_blank" href="<?php echo base_url()?>downloads/Veida.apk"><i class="fas fa-cloud-download-alt"></i> Descargar la app para Android <i class="fab fa-android"></i></a></p>
                    </div>
                </div>
                <div class="col-lg-6 h-100 text-center text-lg-start my-auto">
                    <p class="text-muted small mb-4 mb-lg-0"><b><i class="fas fa-laptop-code"></i> Desarrollado por Mirko</b> | <?php echo date('Y'); ?></p>
                </div>
            </div>
        </section>
        <!-- Bootstrap core JS-->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js"></script>
        <!-- Core theme JS-->
        <script src="<?php echo site_url('js/scripts.js'); ?>"></script>
        <script src="https://cdn.startbootstrap.com/sb-forms-latest.js"></script>
    </body>
</html>