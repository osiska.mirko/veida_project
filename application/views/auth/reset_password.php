<html>
	<head>
		<title>Resetear contraseña</title>
	</head>

	<body>
		<?php 
			if($success){
				redirect('auth/ventana_de_clave_recuperada');
			} else {
				
			    echo form_open();
			    echo "<section class='content'>
					 <div class='d-flex justify-content-center' style='margin: 150px 0 0 0px;''>
					 <div class='form-signin'>
					 <!-- /.login-logo -->
					  <div class='card card-outline card-primary'>
					  <div class='card-header text-center'>
					  <a href='#'><b>Distribuidora </b>VEIDA</a>
					  </div>
					  <div class='card-body'>
					  <p class='login-box-msg'>Ingresá tu nueva contraseña.</p>"; 
			    echo form_label('Contraseña', 'password') .'<br />';
			    echo form_password(array('name' => 'password', 'value' => set_value('password'))) .'<br />';
			    echo form_error('password');
			    echo form_label('Confirmar Contraseña', 'password_conf') .'<br />';
			    echo form_password(array('name' => 'password_conf', 'value' => set_value('password_conf'))) .'<br />';
			    echo form_error('password_conf');
			    echo form_submit(array('type' => 'submit', 'value' => 'Registrar nueva contraseña'));
			    echo 
			    	"</div>
        			<!-- /.card-body -->
      				</div>
      				<!-- /.card -->
    				</div>
    				<!-- /.login-box -->
  					</section>";
			    echo form_close();
		    }
		?>
	</body>
</html>




