<script src="https://unpkg.com/ionicons@5.2.3/dist/ionicons.js"></script>
<aside class="main-sidebar sidebar-dark-info elevation-4">
  <a href="<?php echo site_url('Welcome') ?>" class="brand-link">
    <img src="<?php echo base_url("/assets/logo_veida.png")?>" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
    <span class="brand-text font-weight-light">Distribuidora Veida</span>
  </a>
  
  <div class="sidebar">

    <div class="user-panel mt-3 pb-3 mb-3 d-flex">

      <div class="image">
        <img src="<?php echo base_url("/assets/avatar.png")?>" class="img-circle elevation-2" alt="User Image">
      </div>
      
      <div class="info">
        <a href="#" class="d-block"><strong><?php echo $this->Authme_model->dar_nombre_por_id_user(user('id_usuario')); ?></strong></a>
      </div>
    
    </div>

    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        
        <li class="nav-item nav-item menu-is-opening menu-open ">
          <a href="#" class="nav-link active">
            <i class="fas fa-cash-register"></i>
            <p>
            Ventas
            <i class="right fas fa-angle-left"></i>
            </p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">

              <?php
                $desde = date('Y-m-d',strtotime("-7 days"));
                $hasta = date('Y-m-d');
              ?>

              <a href="<?php echo site_url('Ventas') ?>" class="nav-link active">
                <i id="realizar_pedido_menu" class=""></i> <i class="fas fa-file-invoice-dollar" ></i>
                <p> Realizar un pedido </p> 
              </a>
            </li>
            <li class="nav-item ">
              <a href="<?php echo site_url('Ventas/ver_preventas/') ?>" class="nav-link active">
                <i id="ver_pedidos_menu" class=""></i> <i class="fas fa-shopping-basket"></i>
                <p>
                Pedidos    
                </p>
              </a>        
            </li>
            <li class="nav-item ">
              <a href="<?php echo site_url('Ventas/entregas_pendientes') ?>" class="nav-link active" >
                <i id="entregas_pendientes_menu" class=""></i> <i class="fas fa-truck-moving"></i>
                <p>
                Entregas      
                </p>
              </a>
            </li>
            <li class="nav-item">
              <a href="<?php echo site_url('Ventas/ver_ventas/'.$desde.'/'.$hasta) ?>" class="nav-link active" >
                <i id="ver_ventas_menu" class=""></i> <i class="fas fa-receipt"></i>
                <p>Ver ventas</p>
              </a>
            </li>
          </ul>    
        </li>
         
        <li class="nav-item nav-item menu-is-opening menu-open "> 
          
          <a href="#" class="nav-link active">
            <i class="fas fa-cubes"></i>
            <p>
              Mercadería
              <i class="right fas fa-angle-left"></i>      
            </p>
          </a>

          <ul class="nav nav-treeview">
            <li class="nav-item ">
              <a href="<?php echo site_url('/Productos') ?>" class="nav-link active" >
                <i id="productos_menu" class=""></i> <i class="fas fa-truck-loading"></i>
                <p>
                  Gestionar productos
                </p>
              </a>      
            </li>

            <li class="nav-item ">
              <a href="<?php echo site_url('Productos/listar_productos') ?>" class="nav-link active">
                <i id="listar_productos_menu" class=""></i> <i class="fas fa-file-pdf"></i>
                <p>
                  Listar productos
                </p>
              </a>      
            </li>
          </ul>
        </li>

        <li class="nav-item nav-item menu-is-opening menu-open ">
          
          <a href="#" class="nav-link active">
            <i class="fas fa-user-friends"></i>
            <p>
              Usuarios
              <i class="right fas fa-angle-left"></i>      
            </p>
          </a>

          <ul class="nav nav-treeview">

            <li class="nav-item">
              <a href="<?php echo site_url('auth/gestionar_clientes') ?>" class="nav-link active">
                <i id="clientes_menu" class=""></i> <i class="fas fa-user-tag"></i>
                <p>Clientes</p>
                </a>
            </li>
            <li class="nav-item">
              <a href="<?php echo site_url('auth/gestionar_empleados') ?>" class="nav-link active">
                <i id="empleados_menu" class=""></i> <i class="fas fa-user-tie"></i>
                <p>Empleados</p>
                </a>
            </li>

          </ul>

        </li>
          
        <li class="nav-item ">
          <a href="<?php echo site_url('auth/logout') ?>" class="nav-link active">
            <i class="fas fa-user-circle"></i>
            <p>
              Cerrar Sesión      
            </p>
          </a>            
        </li>
        
        <li class="nav-item">
          <a data-widget="pushmenu" href="#" role="button" class="nav-link btn-secondary">
            
            <i class="fas fa-compress-arrows-alt"></i>
            <p>
              Esconder menú lateral  
            </p>
          </a>            
        </li>
      
      </ul>
    </nav>
  </div>
</aside>

