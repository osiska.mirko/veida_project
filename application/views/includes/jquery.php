	
	<script src="<?php echo base_url("/assets/plugins/jquery-ui/jquery-ui.min.js") ?>"></script>
	<script>
		$.widget.bridge('uibutton', $.ui.button)
	</script>
	<script src="<?php echo base_url("/assets/plugins/bootstrap/js/bootstrap.bundle.min.js") ?>"></script>
	<script src="<?php echo base_url("/assets/plugins/chart.js/Chart.min.js") ?>"></script>
	<script src="<?php echo base_url("/assets/plugins/sparklines/sparkline.js") ?>"></script>
	<script src="<?php echo base_url("/assets/plugins/jqvmap/jquery.vmap.min.js") ?>"></script>
	<script src="<?php echo base_url("/assets/plugins/jqvmap/maps/jquery.vmap.usa.js") ?>"></script>
	<script src="<?php echo base_url("/assets/plugins/jquery-knob/jquery.knob.min.js") ?>"></script>
	<script src="<?php echo base_url("/assets/plugins/moment/moment.min.js") ?>"></script>
	
	
	<script src="<?php echo base_url("/assets/plugins/summernote/summernote-bs4.min.js") ?>"></script>
	<script src="<?php echo base_url("/assets/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js") ?>"></script>
	<script src="<?php echo base_url("/assets/dist/js/adminlte.js") ?>"></script>
	<script src="<?php echo base_url("/assets/dist/js/demo.js") ?>"></script>
	<script src="<?php echo base_url("/assets/dist/js/pages/dashboard.js") ?>"></script>
