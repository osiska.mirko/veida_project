<!doctype html>
<html lang="en">
<?php 
  date_default_timezone_set('America/Argentina/Buenos_Aires');
?>
  <head> 
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>Distribuidora Veida</title>

      <!-- CKEditor -->
      <script src="https://cdn.ckeditor.com/4.11.2/standard/ckeditor.js"></script>

      <!-- JSPDF -->
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/2.3.0/jspdf.umd.min.js"></script>

      <!-- HTML2CANVAS -->
      <script src="<?php echo base_url("/javascript/node_modules/html2canvas/dist/html2canvas.js") ?>"></script>
      
      <?php //sweet alert: inicio?>
      <script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
      <script src="sweetalert2.all.min.js"></script>
      <script src="//cdn.jsdelivr.net/npm/promise-polyfill@8/dist/polyfill.js"></script>
      <script src="sweetalert2.min.js"></script>
      <link rel="stylesheet" href="sweetalert2.min.css">
      <?php //sweet alert fin?>
      
      <?php /* pace>
      <script src="https://cdn.jsdelivr.net/npm/pace-js@latest/pace.min.js"></script>
      <link rel="stylesheet" href="<?php echo base_url("/assets/center-atom.css") ?>">
      <?php pace fin */ ?>
      
      <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
      <link rel="stylesheet" href="https://adminlte.io/themes/v3/plugins/fontawesome-free/css/all.min.css">
      <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
      <link rel="stylesheet" href="https://adminlte.io/themes/v3/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
      <link rel="stylesheet" href="https://adminlte.io/themes/v3/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
      <link rel="stylesheet" href="https://adminlte.io/themes/v3/plugins/jqvmap/jqvmap.min.css">
      <link rel="stylesheet" href="https://adminlte.io/themes/v3/dist/css/adminlte.min.css">
      <link rel="stylesheet" href="https://adminlte.io/themes/v3/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
      
      <link rel="stylesheet" href="https://adminlte.io/themes/v3/plugins/summernote/summernote-bs4.min.css">
      <link  rel="icon" href="<?php echo base_url("/assets/logo_veida.png")?>" type="image/png" />
      <script
        src="https://code.jquery.com/jquery-3.5.1.js"
        integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc="
        crossorigin="anonymous">
      </script>      
      <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
      <link rel="stylesheet" href="<?php echo base_url("/assets/plugins/fontawesome-free/css/all.min.css") ?>">
      <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
      <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
      <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.css">
      <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.js"></script>

      <!-- Load Leaflet from CDN -->
      <link rel="stylesheet" href="https://unpkg.com/leaflet@1.4.0/dist/leaflet.css" integrity="sha512-puBpdR0798OZvTTbP4A8Ix/l+A4dHDD0DGqYW6RQ+9jxkRFclaxxQb/SJAWZfWAkuyeQUytO7+7N4QKrDh+drA==" crossorigin="" />
      <script src="https://unpkg.com/leaflet@1.4.0/dist/leaflet.js" integrity="sha512-QVftwZFqvtRNi0ZyCtsznlKSWOStnDORoefr1enyq5mVL4tmKB3S/EnC3rRJcxCPavG10IcrVGSmPh6Qw5lwrg==" crossorigin=""></script>

      <!-- Load Esri Leaflet from CDN -->
      <script src="https://unpkg.com/esri-leaflet@2.2.3/dist/esri-leaflet.js" integrity="sha512-YZ6b5bXRVwipfqul5krehD9qlbJzc6KOGXYsDjU9HHXW2gK57xmWl2gU6nAegiErAqFXhygKIsWPKbjLPXVb2g==" crossorigin=""></script>

      <!-- Load Esri Leaflet Geocoder from CDN -->
      <link rel="stylesheet" href="https://unpkg.com/esri-leaflet-geocoder@2.2.13/dist/esri-leaflet-geocoder.css" integrity="sha512-v5YmWLm8KqAAmg5808pETiccEohtt8rPVMGQ1jA6jqkWVydV5Cuz3nJ9fQ7ittSxvuqsvI9RSGfVoKPaAJZ/AQ==" crossorigin="">
      <script src="https://unpkg.com/esri-leaflet-geocoder@2.2.13/dist/esri-leaflet-geocoder.js" integrity="sha512-zdT4Pc2tIrc6uoYly2Wp8jh6EPEWaveqqD3sT0lf5yei19BC1WulGuh5CesB0ldBKZieKGD7Qyf/G0jdSe016A==" crossorigin=""></script>
  </head>

  <body class="layout-fixed">

  
    <?php
      if($this->Authme_model->tipo_usuario_por_id_user(user('id_usuario')) == 0) 
      {
        $this->load->view('includes/aside_admin'); 
      };
      if($this->Authme_model->tipo_usuario_por_id_user(user('id_usuario')) == 1) 
      {
        $this->load->view('includes/aside_preventista'); 
      };
      if($this->Authme_model->tipo_usuario_por_id_user(user('id_usuario')) == 2) 
      {
        $this->load->view('includes/aside_repartidor'); 
      };
    ?>
    
    <header>
      <?php $this->load->view('includes/navbar'); ?>
    </header>

    <main role="main" class="container col-12">
        <?php $this->load->view($content); ?>
    </main>
    
    <?php $this->load->view('includes/footer');?>

    <?php $this->load->view('includes/jquery');?>

    <?php /*
    <style>
      .margenResponsive {
        min-height: 192px;
        margin: 0 0 0 160px; 
        background: transparent;
      }

      @media only screen and (max-width: 992px) {
        .margenResponsive {
          margin: 0px;
        }
      }
    </style>
    */
    ?>

    <style>
      .margenResponsive {
        background: transparent;
      }
    </style>

  </body>
</html>

<?php // $botonMenuActivado = ''; ?>
<input type='hidden' id='getClassActiveMenu' value='<?php echo $botonMenuActivado; ?>'>
<script type="text/javascript">
  $(document).ready(function() 
  {
    var menuActivo = document.getElementById("getClassActiveMenu").value;
    //alert(menuActivo);
    $(menuActivo).addClass('fas fa-circle text-info');
    //$(menuActivo).addClass('');
  });
</script>
