<!doctype html>
<html lang="en">
  <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>Distribuidora Veida</title>
  
      <link rel="stylesheet" href="<?php echo base_url("/assets/plugins/select2/js/select2.full.js") ?>">
      <link rel="stylesheet" href="<?php echo base_url("/assets/plugins/select2/js/select2.js") ?>">
      <link  rel="icon"   href="<?php echo base_url("/assets/logo_veida.png")?>" type="image/png" />
      <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
      <link rel="stylesheet" href="<?php echo base_url("/assets/plugins/fontawesome-free/css/all.min.css") ?>">
      <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
      <link rel="stylesheet" href="<?php echo base_url("/assets/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css") ?>">
      <link rel="stylesheet" href="<?php echo base_url("/assets/plugins/icheck-bootstrap/icheck-bootstrap.min.css") ?>">
      <link rel="stylesheet" href="<?php echo base_url("/assets/plugins/jqvmap/jqvmap.min.css") ?>">
      <link rel="stylesheet" href="<?php echo base_url("/assets/dist/css/adminlte.min.css") ?>">
      <link rel="stylesheet" href="<?php echo base_url("/assets/plugins/overlayScrollbars/css/OverlayScrollbars.min.css") ?>">
      <link rel="stylesheet" href="<?php echo base_url("/assets/plugins/daterangepicker/daterangepicker.css") ?>">
      <link rel="stylesheet" href="<?php echo base_url("/assets/plugins/summernote/summernote-bs4.min.css") ?>">

      <script
        src="https://code.jquery.com/jquery-3.5.1.js"
        integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc="
        crossorigin="anonymous">
      </script>      
      <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
      <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
      
  </head>

  <body>
    <main role="main" class="container">
    <header>  
    </header>
      <?php $this->load->view($content); ?>
      <?php $this->load->view('includes/jquery');?>
    </main>
  </body>
</html>
