<div class="Wrapper">
  <div class="content-wrapper margenResponsive">

    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-12">
            <h1><i class="fas fa-laptop"></i> Sistema de gestión para la Distribuidora Veida</h1>
          </div>
        </div>
      </div>
    </section>

    <section class="content">
      <div class="container-fluid">

        <?php 
        if($this->Authme_model->tipo_usuario_por_id_user(user('id_usuario')) == 0)
          {
        ?>  
            <div class="row">
              <div class="col-md-12">
                <div class="card ">
                  <div class="card-header">
                    <h3 class="card-title"><i class="fas fa-door-open"></i> Bienvenid@ <?php echo $this->Authme_model->dar_nombre_por_id_user(user('id_usuario')); ?> al sistema web de gestión de la Distribuidora Veida | <?php echo  date('F j, Y, g:i a'); ?>
                    </h3>                    
                  </div>
                </div>
              </div>
            </div>
        <?php
         }
        ?>

        <?php 
        if($this->Authme_model->tipo_usuario_por_id_user(user('id_usuario')) == 1)
          {
        ?>  
            <div class="row">
              <div class="col-md-12">
                <div class="card ">
                  <div class="card-header">
                    <h3 class="card-title"><i class="fas fa-door-open"></i> Bienvenid@ <?php echo $this->Authme_model->dar_nombre_por_id_user(user('id_usuario')); ?> al sistema web de gestión de la Distribuidora Veida
                    </h3>
                  </div>
                </div>
              </div>
            </div>

            <div>
              <a href="<?php echo site_url('Ventas') ?>" style='width: 100%;' class='btn btn-default'><i class="fas fa-file-invoice-dollar"></i> Realizar una venta</a>
            </div>

            <br>

            <div>
              <a style='width: 100%;' href="<?php echo site_url('Ventas/ver_preventas') ?>" class='btn btn-default'><i class="fas fa-shopping-basket"></i> Ver ventas realizadas</a>
            </div>

            <br>

            <div>
              <a style='width: 100%;' href="<?php echo site_url('Productos/listar_productos') ?>" class='btn btn-default'><i class="fas fa-file-pdf"></i> Listar productos en stock</a>
            </div>

            <br>

            <div>
              <a style='width: 100%;' href="<?php echo site_url('auth/gestionar_clientes_preventista') ?>" class='btn btn-default'><i class="fas fa-user-tag"></i> Gestionar clientes</a>
            </div>
        <?php
         }
        ?>

        <?php 
        if($this->Authme_model->tipo_usuario_por_id_user(user('id_usuario')) == 2)
          {
        ?>  
            <div class="row">
              <div class="col-md-12">
                <div class="card">
                  <div class="card-header">
                    <h3 class="card-title"><i class="fas fa-door-open"></i> Bienvenid@ <?php echo $this->Authme_model->dar_nombre_por_id_user(user('id_usuario')); ?> al sistema web de gestión de la Distribuidora Veida
                    </h3>
                  </div>
                </div>
              </div>
            </div>

            <br>

            <div>
              <a style='width: 100%;' href="<?php echo site_url('Ventas/entregas_pendientes') ?>" class='btn btn-default'><i class="fas fa-truck-moving"></i> Ver entregas pendientes</a>
            </div>
        <?php
         }
        ?>

      </div>
    </section>

  </div>
</div>

<style>
  @media (min-width: 992px)
  {
      .sidebar-mini.sidebar-collapse .content-wrapper 
      {
          margin-left: -1.0rem!important; 
       }
  }
</style>