<div class="Wrapper">
  <div class="content-wrapper margenResponsive">
    
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-12">
            <?php if ($this->session->flashdata('ok')){?>
                <br>
                <div class="alert alert-success"><?php echo $this->session->flashdata('ok');?></div>
                <?php }?>
                 <?php if ($this->session->flashdata('error')){?>
                <br>
                <div class="alert alert-danger"><?php echo $this->session->flashdata('error');?></div>
              <?php }?> 
            <h1><i class="fas fa-truck-loading"></i> Gestionar productos, precios y stock </h1><h7>Gestionar categorías de productos <i class="fas fa-chevron-right"></i></h7><a type="button" id="agregar_categoria"  class="agregar_categoria" data-toggle="modal" data-target="#modal-lg">  <i class="fas fa-plus-circle"></i> Nueva categoría</a>
          </div>
        </div>
      </div>
    </section>

    <link rel="stylesheet" href="<?php echo base_url('/editablegrid_veida/css/style.css')?>" type="text/css" media="screen">
    <link rel="stylesheet" href="<?php echo base_url('/editablegrid_veida/css/responsive.css')?>" type="text/css" media="screen">
    <link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/themes/smoothness/jquery-ui.css" />
    <link rel="stylesheet" href="<?php echo base_url('/editablegrid_veida/css/font-awesome-4.7.0/css/font-awesome.min.css')?>" type="text/css" media="screen">

    <section class="content">
      
      <div class="">
        
        <div id="message"></div>
        
        <div id="wrap">

            <div id="toolbar">

              <input type="text" id="filter" name="filter" placeholder="Buscar..."  />
              <a id="showaddformbutton" class="btn btn-primary"><i class="fa fa-plus"></i> Añadir nuevo producto</a>
                  
            </div>
          <!-- Grid contents -->
          <div id="tablecontent"></div>
        
          <!-- Paginator control -->
          <div id="paginator"></div>

        </div>  
        
        <script src="<?php echo base_url('/editablegrid_veida/js/jquery-1.11.1.min.js')?>" ></script>
        <script src="<?php echo base_url('/editablegrid_veida/js/editablegrid-2.1.0-49.js')?>"></script>   
        <!-- EditableGrid test if jQuery UI is present. If present, a datepicker is automatically used for date type -->
        <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>
        <script src="<?php echo base_url('/editablegrid_veida/js/demo.js')?>" ></script>

        <script type="text/javascript">
        
          var datagrid; 
                
          window.onload = function() 
          { 
            datagrid = new DatabaseGrid();

            // key typed in the filter field
            $("#filter").keyup(function() {
              datagrid.editableGrid.filter( $(this).val());
              // To filter on some columns, you can set an array of column index 
              //datagrid.editableGrid.filter( $(this).val(), [0,3,5]);
            });

            $("#showaddformbutton").click( function()  {
              showAddForm();
            });

            $("#cancelbutton").click( function() {
              showAddForm();
            });

            $("#addbutton").click(function() {
              datagrid.addRow();
            });
          } 

        </script>

        <div id="addform">

          <div class="row">
            <input type="text" id="name" name="name" placeholder="Nombre del producto" ></input>
          </div>

          <div class="row">
            <input type="text" id="firstname" name="firstname" placeholder="Descripción del producto" ></input>
          </div>

          <div class="row_tright">
            <a id="addbutton" class="btn btn-success" ><i class="fa fa-save"></i> Guardar</a>
            <a id="cancelbutton" class="btn btn-danger">Cancelar</a>
          </div>

        </div>

      </div>

      <?php echo form_open('Categorias_de_productos/modal_nueva_categoria'); ?>
        <div class="modal fade" id="modal-lg">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">

              <div class="modal-header">
                <h4 class="modal-title">Nueva categoría</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>

              <div class="modal-body">
                <label for="exampleInputEmail1">Nombre de la nueva categoría:</label>

                    <input type="text" class="form-control" name="nombre_catego" id="exampleInputEmail1" placeholder="Nombre">
                    <br>
                    <label for="exampleInputEmail1">Fecha de alta:</label>
                    <input type="text" class="form-control" readonly value="<?php echo $fecha = date('Y-m-d H:i:s'); ?>" name="fecha_alta_catego" id="exampleInputEmail1" placeholder="<?php echo $fecha; ?>">
              </div>

              <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="submit" class="btn btn-primary">Guardar categoría nueva</button>
              </div>

            </div>
          </div>
        </div>
      <?php echo form_close(); ?>
    </section>

  </div>
</div>

<style>
  @media (min-width: 992px)
  {
      .sidebar-mini.sidebar-collapse .content-wrapper 
      {
          margin-left: -1.0rem!important; 
       }
  }
</style>

<script type="text/javascript">
  $(document).ready(function() 
  {
    var menuActivo = document.getElementById("getClassActiveMenu").value;
    //alert(menuActivo);
    $(menuActivo).addClass('fa fa-circle text-info');
    //$(menuActivo).addClass('');
  });
</script>