 <?php /* abajo se incluye lo necesario para PDF, EXCEL, ETC EN DATATABLE */ ?>
  <script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.7.0/js/dataTables.buttons.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.html5.min.js"></script>
  <script src="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css"></script>
  <script src="https://cdn.datatables.net/buttons/1.7.0/css/buttons.dataTables.min.css"></script>
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.25/css/jquery.dataTables.min.css">
  <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.7.1/css/buttons.dataTables.min.css">
<?php
  $desde = $this->uri->segment(3);
  $hasta = $this->uri->segment(4);
  $monto_total_entre_fechas = $this->Ventas_model->calcular_monto_ventas_por_fechas($desde,$hasta)[0]->monto_final;
?>

<div class="Wrapper">
  <div class="content-wrapper margenResponsive">
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-12 pl-3">
            
            <?php if ($this->session->flashdata('')){?>
              <br>
              <div class="alert alert-success"><?php echo $this->session->flashdata('');?></div>
            <?php }?>

            <h1><i class="fas fa-file-pdf"></i> Ver lista de productos y precios</h1>
          
          </div>    
        </div>
      </div>
    </section> 
    
    <section class="content">
      <div class="container-fluid">
        <div class="invoice p-2 mb-2">
          <div class="row">
            <div class="col-md-12 table-responsive"> 
              <table id="table_id" class="display table-striped">
                
                <thead>
                  <tr>
                    <th><h4><a class='badge badge-light'><i class="far fa-bookmark"></i> ID</a></h4></th>
                    <th><h4><a class='badge badge-light'><i class="fas fa-shopping-cart"></i> Producto</a></h4></th>
                    <th><h4><a class='badge badge-light'><i class="fab fa-wpforms"></i> Descripción</a></h4></th>
                    <th><h4><a class='badge badge-light'><i class="fas fa-dollar-sign"></i> Precio</a></h4></th>
                    <th><h4><a class='badge badge-light'><i class="fas fa-folder-open"></i> Categoría</a></h4></th>      
                  </tr>
                </thead>
                
                <tbody>
                  <?php 
                       
                    foreach ($productos as $row) 
                    {
                  ?>

                      <tr>

                        <td>
                          <?php
                            echo $row->id;
                          ?>
                        </td>

                        <td>
                          <?php
                            echo $row->nombre_producto;
                          ?>
                        </td>

                        <td>
                          <?php
                            echo $row->descrip_producto;
                          ?>
                        </td>

                        <td>
                          <?php
                            echo "$".$row->precio;
                          ?>
                        </td>

                        <td>
                          <?php
                            //aqui se nota increiblemente la velocidad como ralentiza la carga por esta cantidad de veces que consulta a base de datos                        
                            if ($row->categoria_producto == 0)
                            {
                              echo "sin especificar";
                            }
                            else
                            { 
                              $categoria = $this->Productos_model->ver_categoria_por_id_categoria($row->categoria_producto)[0]->nombre;
                              echo $categoria;
                            }
                          ?>
                        </td>

                      </tr>

                  <?php              
                    }   
                  ?>      
                </tbody>
              </table>            
            </div>
          </div>
        </div>
      </div>

    </section>

  </div>
</div>

<script type="text/javascript">
  $(document).ready( function () 
  {
    
    $('#table_id').DataTable({
        pageLength: 999999999,
        order: [[4,'asc'],[ 1,'asc' ]],

        language: {
        "decimal": "",
        "emptyTable": "No hay información",
        "info": "Mostrando _START_ a _END_ de un total de _TOTAL_ Entradas",
        "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
        "infoFiltered": "(Filtrado de _MAX_ total entradas)",
        "infoPostFix": "",
        "thousands": ",",
        "lengthMenu": "Mostrar _MENU_ Entradas",
        "loadingRecords": "Cargando...",
        "processing": "Procesando...",
        "search": "Buscar:",
        "zeroRecords": "Sin resultados encontrados",
        "paginate": {
          "first": "Primero",
          "last": "Ultimo",
          "next": "Siguiente",
          "previous": "Anterior"
        }
      },
      dom: 'Bfrtip',
        buttons: [
        
        {
          extend: 'excelHtml5',
          text: '<button class="btn btn-link"><i class="fas fa-file-excel"></i> Exportar a Excel</button>'
        },
        {
          extend: 'pdfHtml5',
          text: '<button class=" btn btn-link"><i class="fas fa-file-pdf"></i> Exportar en PDF</button>'
        }
      ]
    });
  });
</script>

