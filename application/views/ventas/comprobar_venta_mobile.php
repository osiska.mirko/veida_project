<?php      
    $precio_final= 0;
?>

<div class="Wrapper">
  <div class="content-wrapper margenResponsive">

    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1><i class="fas fa-clipboard-check"></i> Confirmar compra:</h1>
          </div>   
        </div>
      </div>
    </section>

    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">

            <form form action="<?php echo base_url('Ventas/pagar')?>" method="post">     
              <input type="hidden" name="id_venta" value="<?php echo $id_venta; ?>">                      
              <div class="invoice p-3 mb-3">
                
                <div class="row">
                  <div class="col-12">
                    <h4>
                      <img class="img-thumbnail" width="40" height="25" src="<?php echo base_url("/assets/logo_veida.png") ?>" alt="Mercado Pago"> Dist. VEIDA
                    </h4>
                  </div>
                </div>

                <hr>

                <div class="row">
                  <div class="col-12">
                    <h4>
                      <small>📅 Fecha: <?php echo date('d-m-Y'); ?></small>
                    </h4>
                  </div>
                </div>

                <br>
                
                <div class="row">
                  <div class="col-12">
                    <table class="table table-striped">
                      
                      <thead>
                        <tr>
                          <th style='display: none'></th>
                        </tr>
                      </thead>
                      
                      <tbody style='width=100%'>
                        <?php
                          $recorrido_cantidad = 0;
                          $precio_unitario_de_cada_producto = 0; 
                          if ($productos != null)  
                          {
                            foreach ($productos as $producto) 
                            {    
                              foreach ($producto as $row) 
                              { 
                                ?>

                                <tr>

                                  <td>
                                    <div class='badge badge-info' style='width=100%'>🛒 Producto:
                                    </div>
                                    <br>
                                    <div class='btn btn-light' style='width=100%'>
                                      <?php echo $row->nombre_producto; ?>
                                    </div>
                                    <br>

                                    <div class='badge badge-info' style='width=100%'>
                                      💲 Precio x unidad:
                                    </div>
                                    <br>
                                    
                                    <div class='btn btn-light' style='width=100%'>
                                      <?php echo $row->precio_producto; ?>
                                    </div>
                                    <br>

                                    <div class='badge badge-info' style='width=100%'>
                                      📦 Cantidad:
                                    </div>
                                    <br>
                                
                                    <div class='btn btn-light' style='width=100%'>
                                      <?php echo $cantidad[$recorrido_cantidad]." unidad(es)"; ?>
                                    </div>
                                    <br>

                                    <?php 
                                      $precio_unitario_de_cada_producto = ($row->precio_producto * $cantidad[$recorrido_cantidad]);
                                    ?>
                                    
                                    <div class='badge badge-info' style='width=100%'>
                                    📊💲 Precio final x cantidad:
                                    </div>
                                    <br>
                                  
                                    <div class='btn btn-light' style='width=100%'>
                                      <?php echo $precio_unitario_de_cada_producto; ?>
                                    </div>
                                    <br>

                                  </td>
                                </tr>                            
                                
                                <?php 
                                  $recorrido_cantidad = $recorrido_cantidad + 1;
                                  $precio_final = $precio_final + $precio_unitario_de_cada_producto;          
                                  $precio_unitario_de_cada_producto = 0;
                              
                              }
                            }; 
                          }
                        ?>
                      </tbody>
                    </table>
                  </div>
                </div>

                <hr>

                <div class="row">
                  <div class="col-12">
                    
                    <p onclick="nuevo_cliente()" id="lead_seleccionar" name="lead_seleccionar" class="lead lead_seleccionar">
                      <i class="fas fa-user-tag"></i> 
                      Buscar cliente (click para nuevo cliente ⇆)
                    </p>

                    <p style="display: none" onclick="buscar_cliente()" id="lead_nuevo" name="lead_nuevo" class="lead lead_nuevo">
                      <i class="fas fa-user-plus"></i> 
                      Nuevo cliente (click para buscar cliente ⇆)
                    </p>

                    <p class="text-muted well well-sm shadow-none" style="margin-top: 10px;"></p>

                    <div class="form-group">
                      <div name="row_cliente_nuevo" style="display: none" id="row_cliente_nuevo" class="row row_cliente_nuevo">

                        <div style="display: none" id="ubicacion_actual_OFF" class="col-sm-5">
                          
                          <label for="exampleInputEmail1"> 
                            Estoy en el local del cliente:
                          </label>

                          <input type="hidden" id="geo_lat_cliente"  class="form-control" name="geo_lat_cliente" placeholder="">

                          <input type="hidden" id="geo_long_cliente" class="form-control" name="geo_long_cliente" placeholder="">

                          <div name='boton_geo' id='boton_geo' style="width: 77%;" class="btn btn-success" onclick="geoFindMe()">
                            Geoubicación OK <i class="fas fa-check-circle"></i>
                          </div>
                          
                          <div name='deshacer_geo' id='deshacer_geo' style="width: 21%;" class="btn btn-danger" onclick="deshacerGeo()">                   
                            <i class="fas fa-backspace"></i>   
                          </div>

                        </div>

                        <div style="display: visible" id="ubicacion_actual_ON" class="col-sm-5">
                          
                          <label for="exampleInputEmail1">
                            Estoy en el local del cliente:
                          </label>

                          <div name='boton_geo' id='boton_geo' style="width: 100%;" class="btn btn-default" onclick="geoFindMe()">                   
                            <i class="fas fa-globe-americas"></i> 
                            Dar mi ubicación actual    
                          </div>

                        </div>

                        <div class="col-sm-3">   

                          <label>
                            Cód. de área:
                          </label>

                          <input type="number" class="form-control" name="cod_area_tel_cliente" id="cod_area_tel_cliente" placeholder="Código de área">

                        </div>

                        <div class="col-sm-4">

                          <label>
                            Teléfono con whatsapp (sin 15):
                          </label>

                          <input type="number" class="form-control" name="tel_cliente" id="tel_cliente" placeholder="Teléfono">

                        </div>

                        <div id="div_nombre" class="col-sm-3">   

                          <label>
                            Nombre:
                          </label>

                          <input type="text" class="form-control" name="nombre_cliente" id="nombre_cliente" placeholder="Nombre">

                        </div>

                        <div id="div_apellido" class="col-sm-3">

                          <label>
                            Apellido:
                          </label>

                          <input type="text" class="form-control" name="apellido_cliente" id="apellido_cliente" placeholder="Apellido">

                        </div>

                        <div class="col-md-3"> 
                          
                          <label name="label_calle_dir_modal" id="label_calle_direccion_modal">
                            Calle:
                          </label>

                          <input type="text" class="form-control" name="calle_dir_modal" id="calle_dir_modal" placeholder="Calle">

                        </div>

                        <div class="col-md-3"> 
                          
                          <label name="label_num_dir_modal" id="label_num_direccion_modal">
                            Número:
                          </label>

                          <input type="number" class="form-control" name="num_direccion_modal" id="num_direccion_modal" placeholder="Número">

                        </div>

                       <div class="col-12"> 
                          <label name="label_ciudad_modal" id="label_ciudad_modal" for="exampleInputEmail1">Ciudad y provincia :</label>
                          <div class="form-group">
                            <div name="div_select2" style="" id="div_select2" class="form-group div_select2">
                              <select style='width: 100%;' id='ciudadSeleccionada'  class="mapa_argentino custom-select form-control form-control-border" name="id_localidad">
                                <option value='disabled'>⚠ Seleccione ciudad</option>
                                <?php  
                                  foreach ($mapa_argentina as $row) 
                                  {
                                    echo "<option value=".$row->id_localidad.">🌆 ".$row->nombre_ciudad." •  ".$row->nombre_provincia." • "."📪 ".$row->codigo_postal."</option>";
                                  }; 
                                ?>                        
                              </select>
                            </div>
                          </div>
                        </div>

                      </div>
                    </div>

                    <div class="form-group">

                      <div name="div_select" style="display: visible" id="div_select" class="form-group div_select">

                        <select style='width: 100%' name="clientes_lista" id="clientes_lista" class="custom-select form-control form-control-border" name="clientes_lista">

                          <?php  
                            foreach ($lista_clientes as $row) 
                            {
                              if ($row->DNI == 0)
                              {
                                $DNI = "no especificado";
                              }
                              else
                              {
                                $DNI = $row->DNI;
                              }
                              echo "<option value=".$row->id_persona.">👥".$row->Nombres." ".$row->Apellido." • "."📞 ".$row->area_telefono.$row->telefono." • "."DNI: ".$DNI." • "."ID: ".$row->id_persona."</option>";
                            }; 
                          ?>                        
                        </select>
                      </div>
                    </div>

                  </div> 

                </div>
                
                <hr>

                <div class="row">
                  <div class="col-12">
                    <h2>
                      <small>
                        <i class="fas fa-sort-amount-up"></i> 
                        Monto total: <?php echo "💲".$precio_final; ?>
                      </small>
                    </h2>
                  </div>
                </div>

                <hr>

                <div class="row no-print">
                  <div class="col-12">    
                    <button autofocus="true" id="btnPagar" type="submit" class="btn btn-success" style="width: 100%">
                      <i class="fas fa-file-invoice-dollar"></i>
                      Generar venta
                    </button>  
                  </div>
                </div>

              </div>

            </form>

          </div>
        </div>
      </div>
    </section>

  </div>
</div>

<style>
  @media (min-width: 992px)
  {
      .sidebar-mini.sidebar-collapse .content-wrapper 
      {
          margin-left: -1.0rem!important; 
      }
  }
</style>

<script>
  $(document).ready( function () 
  {
    $('#clientes_lista').select2();
    $('.mapa_argentino').select2();
  });

  const buttonPagar = $("#btnPagar");
  var estado = $("#ciudadSeleccionada").val();
  //alert(estado);
  $("#ciudadSeleccionada").change(function(){
    estado = $("#ciudadSeleccionada").val();
    //alert(estado);

    if (estado =='disabled')
    {
      buttonPagar.hide();
    }
    else
    {
      buttonPagar.show();
    }
  });

  function nuevo_cliente()
  {     
      //$("#ciudadSeleccionada").val() = 'disabled';
      buttonPagar.hide();
      $('.mapa_argentino').val('disabled').trigger('change.select2');

      $("#row_cliente_nuevo").show();
      $('#div_select').hide();
      $("#lead_nuevo").show();
      $("#lead_seleccionar").hide();

      $("#nombre_cliente").prop('required',true);   
      $("#apellido_cliente").prop('required',true); 
      $("#cod_area_tel_cliente").prop('required',true); 
      $("#tel_cliente").prop('required',true); 
      $("#geo_lat_cliente").prop('required',true); 
      $("#geo_long_cliente").prop('required',true);  
      $("#calle_dir_modal").prop('required',true); 
      $("#num_direccion_modal").prop('required',true); 
      $("#ciudad_modal").prop('required',true); 
      $("#provincia_modal").prop('required',true);      
  }

  function buscar_cliente()
  {   
      buttonPagar.show();
      $("#row_cliente_nuevo").hide();
      $('#div_select').show();
      $("#lead_nuevo").hide();
      $("#lead_seleccionar").show();    

      $("#nombre_cliente").prop('required',false);   
      $("#apellido_cliente").prop('required',false); 
      $("#cod_area_tel_cliente").prop('required',false); 
      $("#tel_cliente").prop('required',false); 
      $("#geo_lat_cliente").prop('required',false); 
      $("#geo_long_cliente").prop('required',false);
      $("#calle_dir_modal").prop('required',false); 
      $("#num_direccion_modal").prop('required',false); 
      $("#ciudad_modal").prop('required',false); 
      $("#provincia_modal").prop('required',false);            
  }

  function geoFindMe() 
  {
    if (!navigator.geolocation)
    {
      boton_geo.style.backgroundColor= "red";
      boton_geo.style.color = "white";
      var geo_button = document.getElementById('boton_geo'); 
      geo_button.innerHTML = 'Geoubicación no funciona en su tel <i class="fas fa-times"></i>';
      alert("Geoubicación no funciona en su tel. No se pudo obtener la geolocalización, intente introduciendo la dirección manualmente");
    }

    function success(position) 
    {
      var latitude  = position.coords.latitude;
      var longitude = position.coords.longitude;
      document.getElementById("geo_lat_cliente").value=latitude; 
      document.getElementById("geo_long_cliente").value=longitude;   

      $("#calle_dir_modal").prop('required',false); 
      $("#num_direccion_modal").prop('required',false); 
      $("#ciudad_modal").prop('required',false); 
      $("#provincia_modal").prop('required',false); 
      $("#calle_dir_modal").hide();
      $("#num_direccion_modal").hide();
      $("#ciudad_modal").hide();
      $("#provincia_modal").hide();
      
      var div_nombre = document.getElementById("div_nombre");
      div_nombre.className = "col-sm-6";

      var div_apellido = document.getElementById("div_apellido");
      div_apellido.className = "col-sm-6";

      $("#label_num_direccion_modal").hide();
      $("#label_calle_direccion_modal").hide();  

      $("#ubicacion_actual_ON").hide();
      $("#ubicacion_actual_OFF").show();
    };

    function error() 
    {
      boton_geo.style.backgroundColor= "red";
      boton_geo.style.color = "white";
      var geo_button = document.getElementById('boton_geo'); 
      geo_button.innerHTML = 'Debe autorizar la geoubicación <i class="fas fa-exclamation-triangle"></i>';
      alert("Debe autorizar la geoubicación. No se pudo obtener la geolocalización, intente introduciendo la dirección manualmente");
    };

    navigator.geolocation.getCurrentPosition(success, error);
  }

  function deshacerGeo() 
  {
    $("#calle_dir_modal").prop('required',true); 
    $("#num_direccion_modal").prop('required',true); 
    $("#ciudad_modal").prop('required',true); 
    $("#provincia_modal").prop('required',true); 
    $("#calle_dir_modal").show();
    $("#num_direccion_modal").show();
    $("#ciudad_modal").show();
    $("#provincia_modal").show();
    
    var div_nombre = document.getElementById("div_nombre");
    div_nombre.className = "col-sm-3";

    var div_apellido = document.getElementById("div_apellido");
    div_apellido.className = "col-sm-3";

    $("#label_num_direccion_modal").show();
    $("#label_calle_direccion_modal").show(); 

    $("#ubicacion_actual_ON").show();
    $("#ubicacion_actual_OFF").hide(); 

    document.getElementById("geo_lat_cliente").value=""; 
    document.getElementById("geo_long_cliente").value="";
  }
</script>

