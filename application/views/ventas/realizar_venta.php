<div class="Wrapper">
  <div class="content-wrapper margenResponsive">
    <section class="content-header">
      <div class="container-fluid ">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1><i class="fas fa-file-invoice-dollar"></i> Realizar pedido</h1>
          </div>
        </div>
      </div>
    </section>

    <form id='form_id' form action="<?php echo base_url('Ventas/confirmar_venta')?>" method="post">

      <?php if ($this->session->flashdata('mensaje_ventas')){?>
                <br>
                <div class="alert alert-danger"><?php echo $this->session->flashdata('mensaje_ventas');?></div>
      <?php }?>
      <?php if ($this->session->flashdata('exito_user_nuevo')){?>
              <br>
              <div class="alert alert-success"><?php echo $this->session->flashdata('exito_user_nuevo');?></div>
      <?php }?>
      <?php if ($this->session->flashdata('exito_venta')){?>
                <br>
                <div class="alert alert-success"><?php echo $this->session->flashdata('exito_venta');?></div>
      <?php }?>
      <br>
                    
      <section class="content">
        <div class="card card-solid">
          <div class="card-body">
            <div class="row">
              <div class="col-12 col-sm-12">
                <h4><p class="mb-3 text-uppercase" style="background: #ffffff;border: 1px solid #007bff;color: #007bff;padding: 5px 0 5px 11px;border-radius: 5px;"><i class="fas fa-clipboard mr-2"></i>Productos</p></h4>
                <hr>

                <div style="width: 100%" class="btn-group btn-group-toggle" data-toggle="buttons">

                  <div class="btn1 btn btn-default text-center active">                   
                    <strong>Agregar producto al pedido</strong>
                    <br>
                    <a class="fa-2x text-green fas fa-plus-circle">     
                    </a>
                  </div>

                  <div id='div_confirmar' class="btn btn-default text-center active">
                    <strong>Confirmar y cerrar pedido</strong>
                    <br>
                    <a class="fa-2x text-black fas fa-check-circle">     
                    </a>
                  </div> 
                  
                </div>
                
                <hr>
                <ol class="ol_menu px-3">
                  <li class='li_menu mb-2' style='padding: 0 10px !important;'>
                      <div class='row'>
                          <div class='col-sm-7'>
                            <select style='width: 100%' name='productos[]->id_producto' placeholder='.col-7' class='form-control js-example-basic-single2 form-control form-control-sm'>
                              <?php foreach ($productos_lista as $row) 
                                {
                                  echo '<option value='.$row->id_producto.'>🛒'.strtoupper($row->nombre_producto)." - ".$row->descrip_producto." • ".' 💲'.$row->precio_producto." • "." 📦Stock: ".$row->stock.'</option>';
                                }; 
                              ?>                                       
                            </select>
                          </div>
                          
                          <div class='col-sm-3'>
                            <div class="input-group mb-3">
                              <div style='width: 100%' class="input-group-prepend">          
                                <span class='input-group-text'>📦 n°: </span>
                                <input  type='number' min='1' pattern='^[0-9]+' name='cantidad[]' value='1' class='form-control' placeholder='Cantidad'>
                              </div>
                            </div>
                          </div>
                                  
                          <div class='col-sm-2'>
                            <button type='button' placeholder='.col-2' class='form-control  remover btn btn-danger btn-sm'><i class='fas fa-minus-circle'></i></button>
                          </div>
                      </div> 
                  </li>
                </ol>

              </div>
            </div>
          </div>
        </div>
      </section>
    </form>
  </div> 
</div>

<script>
$(document).ready(function()
  {   
    $(".btn1").click(function()
      {                      
      $('.ol_menu').prepend("\
      <li class='li_menu mb-2' style='padding: 0 10px !important;'>\
          \
          <div class='row'>\
              <div class='col-sm-7'>\
                \
                <select style='width: 100%' name='productos[]->id_producto' placeholder='.col-7' class='form-control js-example-basic-single2 form-control form-control-sm'>\
                                                      \
                  <?php foreach ($productos_lista as $row) 
                    {
                      echo '<option value='.$row->id_producto.'>🛒'.strtoupper($row->nombre_producto)." • ".' 💲'.$row->precio_producto." • "." 📦Stock: ".$row->stock.'</option>';
                    }; 
                  ?>
                \
                </select>\
              </div>\
                  \
              <div class='col-sm-3'>\
                <div class='input-group mb-3'>\
                  <div style='width: 100%' class='input-group-prepend'>\
                    <span class='input-group-text'>📦 n°: </span>\
                    <input  type='number' min='1' pattern='^[0-9]+' name='cantidad[]' value='1' class='form-control' placeholder='Cantidad'>\
                  </div>\
                </div>\
              </div>\
              \
              <div class='col-sm-2'>\
              \
                <button type='button' placeholder='.col-2' class='form-control  remover btn btn-danger btn-sm'><i class='fas fa-minus-circle'></i></button>\
              </div>\
          </div>\
          \
      </li>");
      
      $('.js-example-basic-single2').select2();
      $(".remover").click(function() 
        {
          $(this).closest(".li_menu").remove();
        });
      }); 
                      
  $('.js-example-basic-single2').select2(
    {
      width: 'resolve' // need to override the changed default
    }); 

  });     
</script>

<script type="text/javascript">

  $(document).ready(function()
  {
    
    $('#div_confirmar').click(function()
    {
      $('#form_id').submit();
    });

  });

</script>

