<div class="Wrapper">
  <div class="content-wrapper margenResponsive">
    
    <form id='form_id' form action="<?php echo base_url('Ventas/confirmar_venta')?>" method="post">

      <?php if ($this->session->flashdata('mensaje_ventas')){?>
                <br>
                <div class="alert alert-danger"><?php echo $this->session->flashdata('mensaje_ventas');?></div>
      <?php }?>
      <?php if ($this->session->flashdata('exito_user_nuevo')){?>
              <br>
              <div class="alert alert-success"><?php echo $this->session->flashdata('exito_user_nuevo');?></div>
      <?php }?>
      <?php if ($this->session->flashdata('exito_venta')){?>
                <br>
                <div class="alert alert-success"><?php echo $this->session->flashdata('exito_venta');?></div>
      <?php }?>
      <br>

      <section class="content">
        <div class="">
          <div class="">
            <div class="row">
              <div class="col-12 col-sm-12">
                <h4><p class="mb-3 text-uppercase" style="background: #ffffff;border: 1px solid #007bff;color: #007bff;padding: 5px 0 5px 11px;border-radius: 5px;"><i class="fas fa-file-invoice-dollar"></i> Realizar pedido</p></h4>
                <hr>

                <div style="width: 100%" class="btn-group btn-group-toggle" data-toggle="buttons">

                  <div class="btn1 btn btn-default text-center active">                   
                    <strong>Agregar producto</strong>
                    <br>
                    <a class="fa-2x text-green fas fa-plus-circle">     
                    </a>
                  </div>

                  <div id='div_confirmar' class="btn btn-default text-center active">
                    <strong>Confirmar y cerrar pedido</strong>
                    <br>
                    <a class="fa-2x text-black fas fa-check-circle">     
                    </a>
                  </div> 

                  <div id='calcular_boton' class="btn btn-secondary text-center active">
                    <strong>Ver monto total actual</strong>
                    <br>
                    <a class="fa-2x text-black fas fa-calculator">     
                    </a>
                  </div> 
                  
                </div>

                <ol class="ol_menu px-3">
                  
                </ol>

              </div>
            </div>
          </div>
        </div>
      </section>
    </form>
  </div> 
</div>

<script>
$(document).ready ( function()
  {  
    var suma = 0;
    $(".btn1").click ( function()
    {                      
      $('.ol_menu').prepend("\
      <li class='li_menu mb-2' style='padding: 0 10px !important;'>\
      \
        <div class='row'>\
          <label class='badge badge-light'>🔍 Buscar y seleccionar producto:</label>\
        </div>\
        \
        <div class='row'>\
          <div class='col-sm-12'>\
            \
            <select style='width: 100%' name='productos[]->id_producto' placeholder='.col-7' class='form-control js-example-basic-single2 form-control form-control-sm'>\
                \
                <option disabled='disabled' selected='selected'>Escribe y encuentra el producto 🔽</option>\
                  <?php foreach ($productos_lista as $row) 
                  { 
                    echo '<option value='.$row->id_producto.'>🛒'.strtoupper($row->nombre_producto).' | '.strtoupper($row->descrip_producto).'</option>';
                  };
                  ?>\
            \
            </select>\
          \
          </div>\
        </div>\
        \
        <hr>\
        \
        <div style='display: none' class='productos_detalles'>\
        \
          <div class='row'>\
            <label id='label_inicial' class='badge badge-light'>🗨 Ningún producto seleccionado</label>\
          </div>\
          \
          <div class='row'>\
            <h5><label id='label_nombre' class='badge badge-light'></label></h5>\
          </div>\
          \
          <div class='row'>\
            <div class='col-sm-12'>\
              <div class='input-group mb-3'>\
                <div style='width: 100%' class='input-group-prepend'>\
                  <span class='input-group-text'>💲 precio x unidad: $</span>\
                  <input style='width: 100%'  type='number' readonly class='monto_individual' id='label_precio' >\
                </div>\
              </div>\
            </div>\
          </div>\
          <div class='row'>\
            <h5><label id='label_descripcion' class='badge badge-light'></label></h5>\
          </div>\
          \
          <div class='row'>\
            <h5><label id='label_catego' class='badge badge-light'></label></h5>\
          </div>\
          \
          <hr>\
          \
          <div class='row'>\
            <label class='badge badge-light'>📦 Ingresar cantidad de este producto:</label>\
          </div>\
          \
          <div class='row'>\
            <div class='col-sm-12'>\
              \
              <div class='row'>\
                <h5><label id='label_stock' class='badge badge-light'></label></h5>\
              </div>\
              \
              <div class='input-group mb-3'>\
                <div style='width: 100%' class='input-group-prepend'>\
                  <span class='input-group-text'>📦 Cantidad: </span>\
                  <input id='cantidad_btn' type='number' min='1' pattern='^[0-9]+' name='cantidad[]' value='1' class='cantidad_individual form-control' placeholder='Cantidad'>\
                </div>\
              </div>\
            </div>\
          </div>\
          \
          <hr>\
          \
          <div class='row'>\
            <label class='badge badge-light'><i class='fas fa-minus-circle'></i> Remover este producto del pedido</label>\
          </div>\
          \
          <div class='row'>\
            <div class='col-sm-12'>\
              <button type='button' placeholder='.col-2' class='form-control remover btn btn-danger btn-sm'><i class='fas fa-minus-circle'></i> Remover producto</button>\
            </div>\
          </div>\
        \
        </div>\
        \
        <hr>\
      \
      </li>");
      
      $('.js-example-basic-single2').select2();

      $(".js-example-basic-single2").change(function()
      { 

        var lista_de_productos_php = <?php echo json_encode($productos_lista); ?>;
        
        var opcion_seleccionada = $(".js-example-basic-single2 option:selected").val();

        const producto = lista_de_productos_php.find( articulo => articulo.id_producto === opcion_seleccionada);
        
        var nombre_producto = JSON.parse(JSON.stringify(producto,['nombre_producto']));
        var stock = JSON.parse(JSON.stringify(producto,['stock']));      
        var precio = JSON.parse(JSON.stringify(producto,['precio_producto']));
        var descrip = JSON.parse(JSON.stringify(producto,['descrip_producto']));
        var catego = JSON.parse(JSON.stringify(producto,['categoria_producto']));
      
        $(this).closest('li').find('#label_nombre').text('🛒 '+(nombre_producto)['nombre_producto']);
        $(this).closest('li').find('#label_stock').text('⚠ STOCK DISPONIBLE: '+(stock)['stock']);
        $(this).closest('li').find('#label_precio').val((precio)['precio_producto']);
        $(this).closest('li').find('#label_descripcion').text('💬 '+(descrip)['descrip_producto']);
        $(this).closest('li').find('#label_catego').text('📂 '+(catego)['categoria_producto']);  
        $(this).closest('li').find('#label_inicial').text('📋 Descripción:');
        
        document.getElementById("cantidad_btn").setAttribute("max",(stock)['stock']);

        $(this).closest('li').find('.productos_detalles').show();  
        //monto_individual = parseFloat((precio)['precio_producto']);
        //document.getElementById("monto_total").value=monto_individual;
      });
      $(".remover").click(function() 
      {
        $(this).closest(".li_menu").remove();
      });
    });  
                      
    $('.js-example-basic-single2').select2(
      {
        width: 'resolve' // need to override the changed default
      }); 
    
    $('#div_confirmar').click(function()
    {
      $('#form_id').submit();
    });

    $('#calcular_boton').click(function()
    { 
      suma = 0 ;

      $(".monto_individual").each(function() {

          if (isNaN(parseFloat($(this).val()))) {

            suma += 0;

          } else {
            

            valor = ( parseFloat($(this).val()) );

            suma += valor * $(this).closest('li').find('.cantidad_individual').val();

          }

      });
      Swal.fire('Monto actual: $'+suma)
    });

  });

</script>

