<div class="Wrapper">
  <div class="content-wrapper margenResponsive">
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-12 pl-3">
            
            <?php if ($this->session->flashdata('')){?>
              <br>
              <div class="alert alert-success"><?php echo $this->session->flashdata('');?></div>
            <?php }?>

            <h1><i class="fas fa-truck-moving"></i> Pedidos a entregar</h1>

            <br>
            <a href='entregas_pendientes'><h5><i class="fas fa-arrow-left"></i> Ver pedidos de la última semana <i class="fas fa-cubes"></i></h5></a>

          </div>    
        </div>
      </div>
    </section> 

    <section class="content">
      <div class="container-fluid">
        <div class="invoice p-2 mb-2">
          <div class="row">
            <div class="col-md-12 table-responsive"> 
              <table id="" class="table_id display table-striped">
                
                <thead>
                  <tr>
                    <th><h4><a class='badge badge-light'><i class="fas fa-user-tag"></i> Cliente | 🔖 ID</a></h4></th>
                    <th><h4><a class='badge badge-light'>📝 Estado</a></h4></th>  
                    <th style='display: none'>Carrito (sin mostrarse, oculto)</th> 
                  </tr>
                </thead>
                
                <tbody>
                  <?php              
                    foreach ($pedidos as $row) 
                    {              
                      echo "<tr>";
                  
                        echo "<input type='hidden' class='id_venta' value='".$row->id_venta."'</input>";
                        echo "<input type='hidden' class='id_id_comprador' value='".$row->id_comprador."'</input>";
                        
                        echo "<td><button style='width: 100%;' class='verPedido btn btn-light'><strong><i class='fas fa-user-tag'></i> ".$this->Authme_model->mostrar_nombre_x_id($row->id_comprador)." | 🔖".$row->id_venta."</strong></button></td>";

                        $persona=$this->Authme_model->mostrar_persona($row->id_comprador);
                        echo "<input type='hidden' class='id_latitud' value='".$persona[0]->latitud."'</input>";
                        echo "<input type='hidden' class='id_longitud' value='".$persona[0]->longitud."'</input>";
                        echo "<input type='hidden' class='id_calle' value='".$persona[0]->calle_direccion."'</input>";
                        echo "<input type='hidden' class='id_numero' value='".$persona[0]->numero_direccion."'</input>";
                        echo "<input type='hidden' class='id_ciudad' value='".$persona[0]->ciudad."'</input>";
                        echo "<input type='hidden' class='id_provincia' value='".$persona[0]->provincia."'</input>";
                                    
                        $telefono = "54".$this->Authme_model->mostrar_persona($row->id_comprador)[0]->area_telefono.$this->Authme_model->mostrar_persona($row->id_comprador)[0]->telefono;
                        echo "<input type='hidden' class='id_telefono' value='".$telefono."'</input>";

                        echo "<input type='hidden' class='id_monto_final_cobrado' value='".$row->monto_final_cobrado."'</input>";
                        
                        if ($row->estado_venta == 3)
                        { 
                          echo "<td class='td_entregado'><button style='width: 100%;' class='enviarParaEntregas btn btn-primary'><i class='fas fa-truck-moving'></i> Entrega pendiente</button></td>";
                        };
                        if ($row->estado_venta == 4)
                        { 
                          echo "<td class='td_sin_entregar'><button style='width: 100%;' class='entregaPendiente btn btn-success'>Pedido entregado <i class='fas fa-clipboard-check'></i></button></td>";
                        }; 

                        echo "<td style='display: none'>";
                        $carrito_model = $this->Ventas_model->mostrar_carrito($row->id_venta);                        
                        foreach ($carrito_model as $carrito)
                        {
                          $carrito_concat = "▶ <strong>".$this->Ventas_model->mostrar_producto_por_id($carrito->id_producto)[0]->nombre_producto."</strong> ($".$carrito->precio_del_producto.") • Cantidad: <strong>".$carrito->cantidad_producto."</strong> • Precio total: <strong>$".($carrito->precio_del_producto*$carrito->cantidad_producto)."</strong> <br> ";  
                          echo "<div><label style='display: none' class='id_carrito'>".$carrito_concat."</label></div>";
                        }
                        echo "</td>";      
                              
                      echo "</tr>"; 
                    }; 
                  
                  ?>      
                </tbody>
              </table>            
            </div>
          </div>
        </div>
      </div>

      <div class="modal fade" id="modal_ver_pedido">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">🔖 Ver pedido</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>

            <div class="btn btn-info" style="width: 100%">
              <i class='fas fa-globe-africa'></i> Geoubicación:
            </div>
            <div class="modal-body" id="modal_body_ver_pedido">
           
            </div>

            <div>
                
              <div class="btn btn-info" style="width: 100%">
                <i class='fab fa-whatsapp'></i> Whatsapp
              </div>
              <h4><a class="btn btn-light" id='whatsapp_modal' style="width: 100%" target='_blank' ></a></h4>

              <div class="btn btn-info" style="width: 100%">
                💲 Monto
              </div>
              <input type="text" readonly id='monto_modal' class="btn btn-light" style="width: 100%">
              </input>

              <div class="btn btn-info" style="width: 100%">
                🛒 Descripción
              </div>
              <label id='descrip_modal' class="btn btn-light" style="width: 100%">
              </label> 

            </div>
            
            <div class="modal-footer justify-content">
              <button type="button" class="btn btn-default" style="width: 100%" data-dismiss="modal">Cerrar</button>            
            </div>
          </div>
        </div>
      </div>

    </section>
    
  </div>
</div>

<script type="text/javascript">
  $(document).ready( function () 
  {
    
    $('table.table_id').DataTable({

        order: [[ 0, 'desc' ]],
        
        language: {

          "decimal": "",
          "emptyTable": "No hay información",
          "info": "Mostrando _START_ a _END_ de un total de _TOTAL_ Entradas",
          "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
          "infoFiltered": "(Filtrado de _MAX_ total entradas)",
          "infoPostFix": "",
          "thousands": ",",
          "lengthMenu": "Mostrar _MENU_ Entradas",
          "loadingRecords": "Cargando...",
          "processing": "Procesando...",
          "search": "Buscar:",
          "zeroRecords": "Sin resultados encontrados",
          "paginate": {
            "first": "Primero",
            "last": "Ultimo",
            "next": "Siguiente",
            "previous": "Anterior"

          }
        }
      });
  });     
</script>

<script>
    $(".table_id").on("click", ".verPedido", function(event)
    { 
      var tr = $(this).closest('tr');        
      var latitud = 0;
      var longitud = 0;
      latitud = $(tr).find('.id_latitud').val();
      longitud = $(tr).find('.id_longitud').val();

      var calle = "";
      var numero = "";
      var ciudad = "";
      var provincia = "";
      var pais = "Argentina";
      calle = $(tr).find('.id_calle').val();  
      numero = $(tr).find('.id_numero').val();
      ciudad = $(tr).find('.id_ciudad').val();
      provincia = $(tr).find('.id_provincia').val();

      var whatsapp = $(tr).find('.id_telefono').val();  
      var monto = $(tr).find('.id_monto_final_cobrado').val();  

      document.getElementById("whatsapp_modal").innerHTML=whatsapp;
      document.getElementById("whatsapp_modal").href='https://api.whatsapp.com/send?phone=+'+whatsapp+'&text=Hola!%20Soy%20el%20repartidor%20de%20Distribuidora%20Veida.%20Tengo%20su%20pedido.';
      document.getElementById("monto_modal").value="$"+monto;

      var carrito = $(tr).find('.id_carrito').text();
      document.getElementById("descrip_modal").innerHTML= carrito;

      var html = '';

      if (latitud != "" && longitud != "")
      {
        html += "<div id='map-container-google-1' class='z-depth-1-half map-container' style='width: 100%'><iframe width='100%' height='300' frameborder='0' scrolling='no' marginheight='0' marginwidth='0' src='https://maps.google.com/maps?width=100%25&amp;height=600&amp;hl=es&amp;q="+latitud+","+longitud+"%20+(Ubicacion)&amp;t=&amp;z=14&amp;ie=UTF8&amp;iwloc=B&amp;output=embed'></iframe></div>";
        $('#modal_body_ver_pedido').html(html);
        $('#modal_ver_pedido').modal('show');  
      }
      else 
      { 
        html += "<div id='map-container-google-1' class='z-depth-1-half map-container' style='width: 100%'><iframe width='100%' height='300' frameborder='0' scrolling='no' marginheight='0' marginwidth='0' src='https://maps.google.com/maps?width=100%25&amp;height=600&amp;hl=es&amp;q="+calle+",%20"+numero+"%20"+ciudad+"%20"+provincia+",%20"+pais+"+(Mi%20nombre%20de%20egocios)&amp;t=&amp;z=14&amp;ie=UTF8&amp;iwloc=B&amp;output=embed'></iframe></div>";
        $('#modal_body_ver_pedido').html(html);
        $('#modal_ver_pedido').modal('show'); 
      }
 
    });
</script>

<script type="text/javascript" > 
    $('.table_id').on('click', '.enviarParaEntregas', function(event)
    { 
      var tr = $(this).closest('tr');
      var id_venta =  $(tr).find('.id_venta').val();
      
      var valor = 4;
      $.ajax({
        type : 'POST',
        url : '<?php echo base_url();?>Ventas/actualizar_estado_venta',
        data: {
          id_venta: id_venta,
          valor : valor,
        },
        success : function(data){
          Swal.fire
          ({
            title: 'Muy bien!',
            text: 'El pedido se ha marcado como entregado',
            icon: 'success',
            confirmButtonText: 'Continuar',
            timer: 2000,
          });
          location.reload();
        },
      });
    })

    $('.table_id').on('click', '.entregaPendiente', function(event)
    { 
      var tr = $(this).closest('tr');
      var id_venta =  $(tr).find('.id_venta').val();
      
      var valor = 3;

      const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
          confirmButton: 'btn btn-success',
          cancelButton: 'btn btn-danger'
        },
        buttonsStyling: false
      })

      swalWithBootstrapButtons.fire({
        title: 'Estás segur@?',
        text: "Estás por cambiar el estado del pedido de entregado a sin entregar.",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Si, cambiar estado!',
        cancelButtonText: 'No, cancelar!',
        reverseButtons: true
      }).then((result) => {
        if (result.isConfirmed) {
          $.ajax({
            type : 'POST',
            url : '<?php echo base_url();?>Ventas/actualizar_estado_venta',
            data: {
              id_venta: id_venta,
              valor : valor,
            },
            success : function(data){
            Swal.fire
              ({
                title: 'Correcto',
                text: 'El pedido figura como pendiente de ser entregado :)',
                icon: 'info',
                confirmButtonText: 'Continuar',
                timer: 2000,
              });
              location.reload();
            },
          }); 
          
        } else if (
          result.dismiss === Swal.DismissReason.cancel
        ) {
          swalWithBootstrapButtons.fire(
            'Cancelado',
            'El pedido no cambió de estado :)',
            'error'
          )
        }
      })      
    });
</script>