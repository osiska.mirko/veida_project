<div class="Wrapper">
  <div class="content-wrapper margenResponsive">
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-12">
          
            <input type="hidden" id='uri_segment_3' value="<?php echo $ciudad; ?>">
            <input type="hidden" id='uri_segment_4' value="<?php echo $preventista; ?>">
            <?php if ($this->session->flashdata('')){?>
              <br>
              <div class="alert alert-success"><?php echo $this->session->flashdata('');?></div>
            <?php }?>

            <h1><i class="fas fa-shopping-basket"></i> Pedidos</h1>
            <br>
          
          </div> 

          <div class="col-md-6" >
            <a style='width: 100%;' type="button" class="btn btn-outline-info " onclick='imprimirTodosLosTickets()'>  
              <i class="fas fa-receipt"></i> Imprimir todos los tickets
            </a>   
          </div>

          <div class="col-md-6" >
            <a data-toggle="modal" data-target="#modal-filtrar-vendedor-ciudad" style='width: 100%;' type="button"  class="btn btn-outline-primary" >  
              <i class="fas fa-search-location"></i> Filtrar por ciudad y preventista
            </a>   
          </div>  
             
        </div>
      </div>
    </section> 

    <?php 
    /*
    <div class="row">
      <div class="col-12 col-md-12 form-group">
        <input type="button" name="pdf" id="pdf" class="next btn btn-info" value="Descargar como PDF"> 
      </div>
    </div>
    */
    ?>
    
    <section class="content">
      <div class="container-fluid">
        <div id="documento_pdf" class="invoice p-2 mb-2">
          <div class="row">
            <div class="col-md-12 table-responsive"> 
              <table id="table_id" class="display table-striped">
                
                <thead>
                  <tr>
                    <th><h4><a class='badge badge-light'>🔖 ID</a></h4></th>
                    <th><h4><a class='badge badge-light'>📅 Fecha</a></h4></th>
                    <th><h4><a class='badge badge-light'><i class="fas fa-user-tie"></i> Vendedor</a></h4></th>
                    <th><h4><a class='badge badge-light'><i class="fas fa-user-tag"></i> Cliente</a></h4></th>  
                    <th><h4><a class='badge badge-light'>🛒 Productos</a></h4></th>     
                    <th><h4><a class='badge badge-light'>💲 Monto</a></h4></th>
                    <th><h4><a class='badge badge-light'>📝 Estado</a></h4></th>
                    <th><h4><a class='badge badge-light'>❌ Cancelar</a></h4></th> 
                    <th><h4><a class='badge badge-light'><i class="fas fa-receipt"></i> Ticket</a></h4></th>  
                  </tr>
                </thead>
                
                <tbody>
                  <?php      
                    foreach ($pedidos as $row) 
                    {        
                      echo "<tr class = 'elemento_de_impresion'>";      
                        echo "<input type='hidden' class='id_user_actual' value='".$this->Authme_model->mostrar_id_persona_x_id_user(user('id_usuario'))."'</input>";

                        echo "<td>".$row->id_venta."</td>";
                        echo "<input type='hidden' class='id_venta' value='".$row->id_venta."'</input>";

                        echo "<td>".$row->fecha_venta."</td>";
                        echo "<input type='hidden' class='id_fecha_venta' value='".$row->fecha_venta."'</input>";

                        $nombre_del_vendedor = $this->Authme_model->mostrar_vendedor_x_id_user($row->id_vendedor);
                        echo "<td><strong>".$nombre_del_vendedor."</strong></td>";
                        echo "<input type='hidden' class='id_id_vendedor' value='".$row->id_vendedor."'</input>";
                        echo "<input type='hidden' class='nombre_vendedor' value='".$nombre_del_vendedor."'</input>";

                        $nombre_del_comprador = $this->Authme_model->mostrar_nombre_x_id($row->id_comprador);
                        echo "<td><strong>".$nombre_del_comprador."</strong></td>";
                        echo "<input type='hidden' class='id_id_comprador' value='".$row->id_comprador."'</input>";
                        echo "<input type='hidden' class='nombre_comprador' value='".$nombre_del_comprador."'</input>";
                        
                        $carrito_model = $this->Ventas_model->mostrar_carrito($row->id_venta);
                        
                        ?>
                        <td>
                          <a style='width: 100%;' class='verDescripcionCarrito btn btn-light'>
                            <strong>
                              🛒 Ver carrito | 
                              🔖<?php echo $row->id_venta; ?>
                            </strong>
                          </a>
                        <?php 

                        $carrito_model = $this->Ventas_model->mostrar_carrito($row->id_venta); 
                        $contador_impresion = 0; 
                        foreach ($carrito_model as $carrito)
                        {     
                          $contador_impresion = $contador_impresion + 1;
                          $carrito_concat = "<br/> ||| (".$contador_impresion.") ||| ".$this->Ventas_model->mostrar_producto_por_id($carrito->id_producto)[0]->nombre_producto." ($".$carrito->precio_del_producto.") | ".$this->Ventas_model->mostrar_producto_por_id($carrito->id_producto)[0]->descrip_producto."  |  Cantidad: <strong>".$carrito->cantidad_producto."</strong> | Precio: <strong>$".($carrito->precio_del_producto*$carrito->cantidad_producto)."</strong>";
                          echo "<div><textarea style='display: none' class='id_carrito_impresion '>".$carrito_concat."</textarea></div>";     
                        };                                     
                       
                        echo "</td>"; 

                        echo "<td><h3>$".$row->monto_final_cobrado."</h3></td>";
                        echo "<input type='hidden' class='id_monto_final_cobrado' value='".$row->monto_final_cobrado."'</input>";
                        
                        if ($row->estado_venta == 3)
                        {            
                          echo "<td class='td_sin_preparar' ><button class='preparacionPendiente btn btn-success'>Pedido preparado <i class='fas fa-box'></i></button></td>";
                        }
                        if ($row->estado_venta == 2)
                        { 
                          echo "<td class='td_preparado'><button class='pedidoPreparado btn btn-primary'>Pedido sin preparar <i class='fas fa-box-open'></i></button></td>";              
                        }

                        if ($row->estado_venta == 2)
                        {
                          echo "<td><button class='borrarPedido btn btn-danger'><i class='fas fa-window-close'></i> Cancelar pedido</button></td>";
                        }
                        if ($row->estado_venta == 3)
                        {
                          echo "<td><button class='NosSePuedeBorrarPedido btn btn-danger'><i class='fas fa-window-close'></i> Cancelar pedido</button></td>";
                        }

                        echo "<td><button class='imprimirTicket btn btn-info'><i class='fas fa-receipt'></i> Imprimir</button></td>";
                              
                      echo "</tr>"; 
                    }; 
                  ?>      
                </tbody>
              </table>            
            </div>
          </div>
        </div>
      </div>
    </section>

    <form id='form_id' form action="<?php echo base_url('Ventas/showPreventasByCityClient')?>" method="post">
      <div class="modal fade" id="modal-filtrar-vendedor-ciudad">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title"><i class="fas fa-search-location"></i> Filtrar pedidos por preventista y/o ciudad</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="col-md-12"> 
                <label name="" for="exampleInputEmail1"><i class="fas fa-city"></i> Seleccionar ciudad:</label>
                <div class="form-group">
                  <div name="div_select2" style="" id="div_select2" class="form-group div_select2">
                    <select style='width: 100%;'  class="mapa_argentino custom-select form-control form-control-border" name="id_city">
                      <option value="todos">🌆 TODAS LAS CIUDADES</option>
                      <?php  
                        foreach ($mapa_argentina as $row) 
                        {
                          echo "<option value=".$row->id_localidad.">🌆 ".$row->nombre_ciudad." •  ".$row->nombre_provincia." • "."📪 ".$row->codigo_postal."</option>";
                        }; 
                      ?>                        
                    </select>
                  </div>
                </div>
              </div>

              <div class="col-md-12"> 
                <label name="" for="exampleInputEmail1"><i class="fas fa-user-tie"></i> Seleccionar preventista:</label>
                <div class="form-group">
                  <div name="div_select2" style="" id="div_select2" class="form-group div_select2">
                    <select style='width: 100%;'  class="preventistas custom-select form-control form-control-border" name="seller">
                      <option value="todos">🤵 TODOS LOS PREVENTISTAS</option>
                      <?php  
                        foreach ($preventistas as $row) 
                        {
                          echo "<option value=".$row->id_usuario.">🤵 ".$row->Nombres." ".$row->Apellido."</option>";
                        }; 
                      ?>                        
                    </select>
                  </div>
                </div>
              </div>
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
              <button type="submit" class="btn btn-primary"><i class="fas fa-search-location"></i> Filtrar</button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->
    </form>

    <div class="modal fade" id="modal_ver_pedido">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">🔖 Ver pedido</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>

          <div class="modal-body" id="modal_body_ver_pedido">
         
          </div>

          <div>

            <div class="btn btn-info" style="width: 100%">
              🛒 Descripción
            </div>
            <label id='descrip_modal' class="text-left btn btn-light" style="width: 100%">
            </label> 

            <div class="btn btn-info" style="width: 100%">
              💲 Monto total
            </div>
            <input type="text" readonly id='monto_modal' class="btn btn-light" style="width: 100%">
            </input>
            
          </div>
          
          <div class="modal-footer justify-content">
            <button type="button" class="btn btn-default" style="width: 100%" data-dismiss="modal">Cerrar</button>            
          </div>
        </div>
      </div>
    </div>

  </div>
</div>

<script type="text/javascript">
  $(document).ready( function () 
  {
    
    $('#table_id').DataTable({

        order: [[ 1, 'desc' ]],
        "pageLength": 100,
        language: {

          "decimal": "",

          "emptyTable": "No hay información",
          "info": "Mostrando _START_ a _END_ de un total de _TOTAL_ Entradas",
          "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
          "infoFiltered": "(Filtrado de _MAX_ total entradas)",
          "infoPostFix": "",
          "thousands": ",",
          "lengthMenu": "Mostrar _MENU_ Entradas",
          "loadingRecords": "Cargando...",
          "processing": "Procesando...",
          "search": "Buscar:",
          "zeroRecords": "Sin resultados encontrados",
          "paginate": {
            "first": "Primero",
            "last": "Ultimo",
            "next": "Siguiente",
            "previous": "Anterior"

          }

        }
      });
  });     
</script>

<script>

    var preseleccion_mapa = document.getElementById("uri_segment_3").value;
    $('.mapa_argentino').select2();
    $('.mapa_argentino').val(preseleccion_mapa).trigger('change');

    var preventistas = document.getElementById("uri_segment_4").value;
    $('.preventistas').select2();
    $('.preventistas').val(preventistas).trigger('change');

    $('#table_id').on('click', '.pedidoPreparado', function(event)
    { 
      var tr = $(this).closest('tr');
      var id_venta =  $(tr).find('.id_venta').val();

      var valor = 3;
      $.ajax({
        type : 'POST',
        url : '<?php echo base_url();?>Ventas/actualizar_estado_venta',
        data: {
          id_venta: id_venta,
          valor : valor,
        },
        success : function(data){
          Swal.fire
          ({
            title: 'Muy bien!',
            text: 'El pedido figura como preparado y se encuentra visible para el repartidor',
            icon: 'success',
            confirmButtonText: 'Continuar',
            timer: 2000,
          });
          location.reload();
        },
      });   
    });

    $('#table_id').on('click', '.preparacionPendiente', function(event)
    { 
      var tr = $(this).closest('tr');
      var id_venta =  $(tr).find('.id_venta').val();

      var valor = 2;

      const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
          confirmButton: 'btn btn-success',
          cancelButton: 'btn btn-danger'
        },
        buttonsStyling: false
      })

      swalWithBootstrapButtons.fire({
        title: 'Estás segur@?',
        text: "Estás por cambiar el estado del pedido de <preparado> a <sin preparar>. Antes de realizar este cambio asegurate de que el pedido siga en el depósito y de que no haya sido despachado para la entrega con el repartidor. Es un paso a tener en cuenta para no ocasionar desbalances en el stock.",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Si, cambiar estado!',
        cancelButtonText: 'No, cancelar!',
        reverseButtons: true
      }).then((result) => {
        if (result.isConfirmed) {
          $.ajax({
            type : 'POST',
            url : '<?php echo base_url();?>Ventas/actualizar_estado_venta',
            data: {
              id_venta: id_venta,
              valor : valor,
            },
            success : function(data){
              Swal.fire
              ({
                title: 'Correcto',
                text: 'El pedido figura como pendiente de ser preparado y no es visible al repartidor',
                icon: 'info',
                confirmButtonText: 'Continuar',
                timer: 2000,
              });
              location.reload();
            },
          }); 
          
        } else if (
          result.dismiss === Swal.DismissReason.cancel
        ) {
          swalWithBootstrapButtons.fire(
            'Cancelado',
            'El pedido no cambió de estado :)',
            'error'
          )
        }
      })      
    });

    $('#table_id').on('click', '.borrarPedido', function(event)
    { 
      var tr = $(this).closest('tr');        
      var id_venta = $(tr).find('.id_venta').val();
      var user = $(tr).find('.id_user_actual').val();

      var valor = 5;
      const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
          confirmButton: 'btn btn-success',
          cancelButton: 'btn btn-danger'
        },
        buttonsStyling: false
      })

      swalWithBootstrapButtons.fire({
        title: 'Estás segur@?',
        text: "Está por borrar el pedido "+id_venta+". Esta acción no se puede deshacer!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Si, eliminar pedido!',
        cancelButtonText: 'No, cancelar!',
        reverseButtons: true
      }).then((result) => {
        if (result.isConfirmed) {
          $.ajax({
            type : 'POST',
            url : '<?php echo base_url();?>Ventas/eliminar_preventa',
            data: {
              id_venta: id_venta,
              valor : valor,
              user : user,
            },
            success : function(data){
              swalWithBootstrapButtons.fire(
                'Eliminado!',
                'Este pedido se ha borrado del sistema.',
                'success'
              );
              location.reload();
            },
          }); 
          
        } else if (
          result.dismiss === Swal.DismissReason.cancel
        ) {
          swalWithBootstrapButtons.fire(
            'Cancelado',
            'El pedido está a salvo :)',
            'error'
          )
        }
      }) 
    });

    $('#table_id').on('click', '.NosSePuedeBorrarPedido', function(event)
    {
      Swal.fire
      ({
        icon: 'error',
        title: 'Oops...',
        text: 'No se puede eliminar un pedido que ya está preparado!',
      })
    }); 
</script>

<script type="text/javascript">

  $(document).ready(function(){

    $("#pdf").on('click', function(){

      window.jsPDF = window.jspdf.jsPDF;
      var doc = new jsPDF();

      var width = doc.internal.pageSize.getWidth();
      var height = doc.internal.pageSize.getHeight();

      html2canvas(document.querySelector("#documento_pdf"), {scale: 5, scrollY: -window.scrollY}).then(canvas => {

        var imgData = canvas.toDataURL('image/jpeg');
        doc.addImage(imgData, 'JPEG', 0, 0, width, height);
      
        doc.save('Spec_Sheet.pdf');
      });
    });
  });
</script>

<script>
  $('#table_id').on('click', '.imprimirTicket', function(event) 
  {
    var tr = $(this).closest('tr');        
    var fecha = $(tr).find('.id_fecha_venta').val();
    var id_venta = $(tr).find('.id_venta').val();
    var comprador = $(tr).find('.nombre_comprador').val();
    var carrito = $(tr).find('.id_carrito_impresion').text();   
    var vendedor = $(tr).find('.nombre_vendedor').val();
    var monto_total_venta = $(tr).find('.id_monto_final_cobrado').val();

    var ticket = '<html>\
      -<br>\
      DISTRIBUIDORA VEIDA <br>\
      ID de la venta: '+id_venta+'<br>\
      Fecha de la venta: '+fecha+'<br>\
      Cliente: '+comprador+'<br>\
      Vendedor: '+vendedor+'<br>\
      -<br>\
      Descripción:<br>\
      -<br>\
      '+carrito+'\
      <br>\
      TOTAL: $'+monto_total_venta+'<br>\
      - <br>\
      -<br>\
      </html>\
      ';

    var openWindow = window.open(ticket);
    openWindow.document.write(ticket);
    openWindow.document.close();
    openWindow.focus();
    openWindow.print();
    openWindow.close();    
  });
</script>

<script>
  function imprimirTodosLosTickets() 
  {
    var ticket ="";
    var id_venta =0;
    var fecha =0;
    var comprador =0;
    var vendedor=0 ;
    var descripcion=0;
    var monto_total_venta=0;

    $(".elemento_de_impresion").each(function() 
    {

      id_venta = $(this).find('.id_venta').val();
      fecha = $(this).find('.id_fecha_venta').val();
      comprador = $(this).find('.nombre_comprador').val();
      descripcion = $(this).find('.id_carrito_impresion').text();   
      vendedor = $(this).find('.nombre_vendedor').val();
      monto_total_venta = $(this).find('.id_monto_final_cobrado').val();

      ticket += '<html>\
        -<br>\
        DISTRIBUIDORA VEIDA <br>\
        ID de la venta: '+id_venta+'<br>\
        Fecha de la venta: '+fecha+'<br>\
        Cliente: '+comprador+'<br>\
        Vendedor: '+vendedor+'<br>\
        -<br>\
        Descripción:<br>\
        -'+descripcion+'\
        <br>||| TOTAL: $'+monto_total_venta+' |||<br>\
        - <br>\
        -<br>\
        .<br>\
        .<br>\
        .<br>\
        </html>\
        ';
    });

    var openWindow = window.open(ticket);
    openWindow.document.write(ticket);
    openWindow.document.close();
    openWindow.focus();
    openWindow.print();
    openWindow.close();    
  };
</script>

<script>
  function printAllTickets()
  {
    
    $(".elemento_de_impresion").each(function() {

        //$(this).closest('li').find('.id_venta').val();
        alert($(this).find('.id_venta').val());

    });
    
  }
</script>

<script>
    $("#table_id").on("click", ".verDescripcionCarrito", function(event)
    { 
      var tr = $(this).closest('tr');          
      var monto = $(tr).find('.id_monto_final_cobrado').val();
      document.getElementById("monto_modal").value="$"+monto;

      var carrito = $(tr).find('.id_carrito_impresion').text();
      document.getElementById("descrip_modal").innerHTML= carrito;

      var html = '';

      $('#modal_body_ver_pedido').html(html);
      $('#modal_ver_pedido').modal('show'); 
    });
</script>