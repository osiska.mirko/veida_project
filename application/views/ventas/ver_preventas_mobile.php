<?php 
  $fechas = array
  (
    date('Y-m-d'),
    date('Y-m-d',strtotime("-1 days")),
    date('Y-m-d',strtotime("-2 days")),
    date('Y-m-d',strtotime("-3 days")),
    date('Y-m-d',strtotime("-4 days")),
    date('Y-m-d',strtotime("-5 days")),
    date('Y-m-d',strtotime("-6 days"))
  );               
?>

<div class="Wrapper">
  <div class="content-wrapper margenResponsive">
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-12 pl-3">
            
            <?php if ($this->session->flashdata('')){?>
              <br>
              <div class="alert alert-success"><?php echo $this->session->flashdata('');?></div>
            <?php }?>

            <h1><i class="fas fa-shopping-basket"></i> Ver preventas</h1>
          
          </div>    
        </div>
      </div>
    </section> 

    <?php 
    foreach ($fechas as $key => $value)
    {  
    ?>

      <hr>
      <label class='form-control button btn-info'> 
        <?php 
          if ($key==0){echo "<h5>📅 Pedidos de HOY (".date('d')." / ".date('m').")</h5>";};
          if ($key==1){echo "<h5>📅 Pedidos de AYER (".date('d',strtotime("-1 days"))." / ".date('m',strtotime("-1 days")).")</h5>";}; 
          if ($key==2){echo "<h5>📅 Pedidos de ANTEAYER (".date('d',strtotime("-2 days"))." / ".date('m',strtotime("-2 days")).")</h5>";};  
          if ($key==3){echo "<h5>📅 Pedidos de hace 3 días (".date('d',strtotime("-3 days"))." / ".date('m',strtotime("-3 days")).")</h5>";};  
          if ($key==4){echo "<h5>📅 Pedidos de hace 4 días (".date('d',strtotime("-4 days"))." / ".date('m',strtotime("-4 days")).")</h5>";}; 
          if ($key==5){echo "<h5>📅 Pedidos de hace 5 días (".date('d',strtotime("-5 days"))." / ".date('m',strtotime("-5 days")).")</h5>";}; 
          if ($key==6){echo "<h5>📅 Pedidos de hace 6 días (".date('d',strtotime("-6 days"))." / ".date('m',strtotime("-6 days")).")</h5>";}; 
        ?> 
      </label>
      <hr>
    
      <section class="content">
        <div class="container-fluid">
          <div class="invoice p-2 mb-2">
            <div class="row">
              <div class="col-md-12 table-responsive"> 
                <table id="" class="table_id display table-striped">
                  
                  <thead>
                    <tr>
                      <th><h4><a class='badge badge-light'><i class="fas fa-user-tag"></i> Cliente | 🔖 ID</a></h4></th> 
                      <th><h4><a class='badge badge-light'>❌ Cancelar</a></h4></th>  
                      <th style='display: none'>Carrito (sin mostrarse, oculto)</th> 
                    </tr>
                  </thead>
                  
                  <tbody>
                    <?php 
                      $id_user = user('id_usuario');  
                      $user = $this->Authme_model->tipo_usuario_por_id_user($id_user);
                      if ($user == 0)
                      {
                        $preventas = $this->Ventas_model->listar_preventas_x_fecha($value);
                      }
                      elseif ($user == 1)
                      {
                        $preventas = $this->Ventas_model->listarPreventasByFechaAndPreventista($value,$id_user);
                      };
                      
                      foreach ($preventas as $row)
                      {        
                        echo "<tr>";      
                          echo "<input type='hidden' class='id_user_actual' value='".$this->Authme_model->mostrar_id_persona_x_id_user(user('id_usuario'))."'</input>";

                          echo "<td><button style='width: 100%;' class='verPedido btn btn-light'><strong><i class='fas fa-user-tag'></i> ".$this->Authme_model->mostrar_nombre_x_id($row->id_comprador)." | 🔖".$row->id_venta."</strong></button></td>";

                          echo "<input type='hidden' class='id_venta' value='".$row->id_venta."'</input>";

                          echo "<input type='hidden' class='id_fecha_venta' value='".$row->fecha_venta."'</input>";

                          echo "<input type='hidden' class='id_nombre_comprador' value='".$this->Authme_model->mostrar_nombre_x_id($row->id_comprador)."'</input>";

                          $telefono = "54".$this->Authme_model->mostrar_persona($row->id_comprador)[0]->area_telefono.$this->Authme_model->mostrar_persona($row->id_comprador)[0]->telefono;
                          echo "<input type='hidden' class='id_telefono' value='".$telefono."'</input>";
                          
                          echo "<td style='display: none'>";
                          $carrito_model = $this->Ventas_model->mostrar_carrito($row->id_venta);                        
                          foreach ($carrito_model as $carrito)
                          { 
                            /* ver preventas, antiguo
                            $carrito_concat = "▶ <strong>".$this->Ventas_model->mostrar_producto_por_id($carrito->id_producto)[0]->nombre_producto."</strong> ($".$carrito->precio_del_producto.") • Cantidad: <strong>".$carrito->cantidad_producto."</strong> • Precio total: <strong>$".($carrito->precio_del_producto*$carrito->cantidad_producto)."</strong> <br> ";  
                            echo "<div><label style='display: none' class='id_carrito'>".$carrito_concat."</label></div>";
                            */
                            $carrito_concat = " ▶ <strong>".$this->Ventas_model->mostrar_producto_por_id($carrito->id_producto)[0]->nombre_producto."</strong> ($".$carrito->precio_del_producto.") • Cantidad: <strong>".$carrito->cantidad_producto."</strong> • Precio total: <strong>$".($carrito->precio_del_producto*$carrito->cantidad_producto)."</strong><br/> ";  
                            echo "<div><textarea style='display: none' class='text-left id_carrito'>".$carrito_concat."</textarea></div>";
                          }
                          echo "</td>";

                          echo "<input type='hidden' class='id_monto_final_cobrado' value='".$row->monto_final_cobrado."'</input>";

                          if ($row->estado_venta == 2)
                          {
                            echo "<td><button style='width: 100%;' class='borrarPedido btn btn-danger'><i class='fas fa-window-close'></i> Cancelar pedido</button></td>";
                          }
                          if ($row->estado_venta == 3)
                          {
                            echo "<td><button style='width: 100%;' class='NosSePuedeBorrarPedido btn btn-danger'><i class='fas fa-window-close'></i> Cancelar pedido</button></td>";
                          }
                                
                        echo "</tr>"; 
                      }; 
                    ?>      
                  </tbody>
                </table>            
              </div>
            </div>
          </div>
        </div>

        <div class="modal fade" id="modal_ver_pedido">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title">🔖 Ver pedido</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>

              <div>
                <div class="btn btn-info" style="width: 100%">
                  <i class="fas fa-user-tag"></i> Nombre del cliente
                </div>
                <input type="text" readonly id='nombre_modal' class="btn btn-light" style="width: 100%">
                </input>

                <div class="btn btn-info" style="width: 100%">
                  <i class='fab fa-whatsapp'></i> Whatsapp
                </div>
                <h4><a class="btn btn-light" id='whatsapp_modal' style="width: 100%" target='_blank' ></a></h4>

                <div class="btn btn-info" style="width: 100%">
                  📅 Fecha de la venta
                </div>
                <input type="text" readonly id='fecha_modal' class="btn btn-light" style="width: 100%">
                </input>

                <div class="btn btn-info" style="width: 100%">
                  💲 Monto
                </div>
                <input type="text" readonly id='monto_modal' class="btn btn-light" style="width: 100%">
                </input>

                <div class="btn btn-info" style="width: 100%">
                  🛒 Descripción
                </div>
                <label id='descrip_modal' class="text-left btn btn-light" style="width: 100%">
                </label> 

              </div>
              
              <div class="modal-footer justify-content">
                <button type="button" class="btn btn-default" style="width: 100%" data-dismiss="modal">Cerrar</button>            
              </div>
            </div>
          </div>
        </div>

      </section>

    <?php 
    }; 
    ?>

  </div>
</div>

<script type="text/javascript">
  $(document).ready( function () 
  {
    
    $('table.table_id').DataTable({

        order: [[ 0, 'desc' ]],
        paging: false,
        info: false,
        search: false,
        language: {

          "decimal": "",
          "emptyTable": "No hay información",
          "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
          "search": "Buscar:",
          "zeroRecords": "Sin resultados encontrados",
        }
      });
  });     
</script>

<script>
    $('.table_id').on('click', '.pedidoPreparado', function(event)
    { 
      var tr = $(this).closest('tr');
      var id_venta =  $(tr).find('.id_venta').val();

      var valor = 3;
      $.ajax({
        type : 'POST',
        url : '<?php echo base_url();?>Ventas/actualizar_estado_venta',
        data: {
          id_venta: id_venta,
          valor : valor,
        },
        success : function(data){
          Swal.fire
          ({
            title: 'Muy bien!',
            text: 'El pedido figura como preparado y se encuentra visible para el repartidor',
            icon: 'success',
            confirmButtonText: 'Continuar',
            timer: 2000,
          });
          location.reload();
        },
      });   
    });

    $('.table_id').on('click', '.preparacionPendiente', function(event)
    { 
      var tr = $(this).closest('tr');
      var id_venta =  $(tr).find('.id_venta').val();

      var valor = 2;

      const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
          confirmButton: 'btn btn-success',
          cancelButton: 'btn btn-danger'
        },
        buttonsStyling: false
      })

      swalWithBootstrapButtons.fire({
        title: 'Estás segur@?',
        text: "Estás por cambiar el estado del pedido de <preparado> a <sin preparar>. Antes de realizar este cambio asegurate de que el pedido siga en el depósito y de que no haya sido despachado para la entrega con el repartidor. Es un paso a tener en cuenta para no ocasionar desbalances en el stock.",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Si, cambiar estado!',
        cancelButtonText: 'No, cancelar!',
        reverseButtons: true
      }).then((result) => {
        if (result.isConfirmed) {
          $.ajax({
            type : 'POST',
            url : '<?php echo base_url();?>Ventas/actualizar_estado_venta',
            data: {
              id_venta: id_venta,
              valor : valor,
            },
            success : function(data){
              Swal.fire
              ({
                title: 'Correcto',
                text: 'El pedido figura como pendiente de ser preparado y no es visible al repartidor',
                icon: 'info',
                confirmButtonText: 'Continuar',
                timer: 2000,
              });
              location.reload();
            },
          }); 
          
        } else if (
          result.dismiss === Swal.DismissReason.cancel
        ) {
          swalWithBootstrapButtons.fire(
            'Cancelado',
            'El pedido no cambió de estado :)',
            'error'
          )
        }
      })      
    });

    $('.table_id').on('click', '.borrarPedido', function(event)
    { 
      var tr = $(this).closest('tr');        
      var id_venta = $(tr).find('.id_venta').val();
      var user = $(tr).find('.id_user_actual').val();

      var valor = 5;
      const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
          confirmButton: 'btn btn-success',
          cancelButton: 'btn btn-danger'
        },
        buttonsStyling: false
      })

      swalWithBootstrapButtons.fire({
        title: 'Estás segur@?',
        text: "Está por borrar el pedido "+id_venta+". Esta acción no se puede deshacer!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Si, eliminar pedido!',
        cancelButtonText: 'No, cancelar!',
        reverseButtons: true
      }).then((result) => {
        if (result.isConfirmed) {
          $.ajax({
            type : 'POST',
            url : '<?php echo base_url();?>Ventas/eliminar_preventa',
            data: {
              id_venta: id_venta,
              valor : valor,
              user : user,
            },
            success : function(data){
              swalWithBootstrapButtons.fire(
                'Eliminado!',
                'Este pedido se ha borrado del sistema.',
                'success'
              );
              location.reload();
            },
          }); 
          
        } else if (
          result.dismiss === Swal.DismissReason.cancel
        ) {
          swalWithBootstrapButtons.fire(
            'Cancelado',
            'El pedido está a salvo :)',
            'error'
          )
        }
      }) 
    });

    $('.table_id').on('click', '.NosSePuedeBorrarPedido', function(event)
    {
      Swal.fire
      ({
        icon: 'error',
        title: 'Oops...',
        text: 'No se puede eliminar un pedido que ya está preparado!',
      })
    }); 
</script>

<script>
    $(".table_id").on("click", ".verPedido", function(event)
    { 
      var tr = $(this).closest('tr');        
      
      var whatsapp = $(tr).find('.id_telefono').val();  
      var monto = $(tr).find('.id_monto_final_cobrado').val();  

      document.getElementById("whatsapp_modal").innerHTML=whatsapp;
      document.getElementById("whatsapp_modal").href='https://api.whatsapp.com/send?phone=+'+whatsapp+'&text=Hola!%20Soy%20el%20repartidor%20de%20Distribuidora%20Veida.%20Tengo%20su%20pedido.';
      document.getElementById("monto_modal").value="$"+monto;

      var carrito = $(tr).find('.id_carrito').text();
      document.getElementById("descrip_modal").innerHTML= carrito;

      var fecha = $(tr).find('.id_fecha_venta').val();
      var nombre_cliente = $(tr).find('.id_nombre_comprador').val();

      document.getElementById("nombre_modal").value=nombre_cliente;
      document.getElementById("fecha_modal").value=fecha;
      
      $('#modal_ver_pedido').modal('show'); 
    });
</script>