 <?php /* abajo se incluye lo necesario para PDF, EXCEL, ETC EN DATATABLE */ ?>
  <script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.7.0/js/dataTables.buttons.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.html5.min.js"></script>
  <script src="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css"></script>
  <script src="https://cdn.datatables.net/buttons/1.7.0/css/buttons.dataTables.min.css"></script>
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.25/css/jquery.dataTables.min.css">
  <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.7.1/css/buttons.dataTables.min.css">
<?php
  $desde = $this->uri->segment(3);
  $hasta = $this->uri->segment(4);
  $monto_total_entre_fechas = $this->Ventas_model->calcular_monto_ventas_por_fechas($desde,$hasta)[0]->monto_final;
?>

<div class="Wrapper">
  <div class="content-wrapper margenResponsive">
    <section class="content-header">
      <div class="container-fluid">
        <div class="">
          <div class="col-sm-12">
            
            <?php if ($this->session->flashdata('')){?>
              <br>
              <div class="alert alert-success"><?php echo $this->session->flashdata('');?></div>
            <?php }?>

            <h1><i class="fas fa-receipt"></i> Ver ventas finalizadas</h1>
          
          </div>    
        </div>
      </div>
    </section> 

    <section class="content">
      <div class="container-fluid">
        <div class="invoice p-2 mb-2">

          <div class="row">
            <label><i class="fas fa-calculator"></i> Total facturado:</label>
          </div>

          <div class="row">
            <div class="col-md-3">
              <label><i class="far fa-calendar"></i> Desde:</label>
              <input required="" readonly="" type="text" class="form-control" name="desde_mostrado" value='<?php 
                if ($desde <= '2021-06-01')
                {
                  echo "INICIO";
                }
                elseif ($desde == date('Y-m-d',strtotime("-7 days")))
                {
                  echo "SEMANA PASADA";
                }
                else
                {
                  echo $desde." a las 00:00 hs"; 
                }
              ?>'>
            </div>
            <div class="col-md-3">
              <label><i class="fas fa-calendar"></i> Hasta:</label>
              <input required="" readonly="" type="text"class="form-control" name="hasta_mostrado" value='<?php 
                if ($hasta == date('Y-m-d'))
                {
                  echo "HOY";
                }
                else
                {
                  echo $hasta." a las 23:59 hs"; 
                }
              ?>'>
            </div>
            <div class="col-md-3">
              <label><i class="fas fa-dollar-sign"></i> Monto total:</label>
              <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                    <i class="fas fa-dollar-sign"></i>
                  </span>
                </div>
                <input  required="" readonly="" type="text" class="form-control" name="monto_total"
                value="<?php 
                  echo $monto_total_entre_fechas;
                ?>">               
              </div>
            </div>

            <div class="col-md-3">
              <label><i class="fas fa-search-dollar"></i> Filtrar por fechas:</label>
              
              <button style='width: 100%;' type="button" class="btn btn-outline-info" data-toggle="modal" data-target="#modal-default">
                  Seleccionar fechas y buscar
              </button>

            </div>
          </div>
        </div>
      </div>
    </section>

    <form action="<?php echo base_url('Ventas/filtrar_ventas_finalizadas_por_fechas')?>" method="post">
      <div class="modal fade" id="modal-default">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title"><i class="fas fa-search-dollar"></i> Filtrar por fechas:</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="invoice p-2 mb-2">
                
                <div class="row">
                  <div class="col-md-12">
                    <label>Desde:</label>
                    <input required="" type="date" class="form-control" name="desde">
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <label>Hasta:</label>
                    <input required="" type="date" class="form-control" name="hasta">
                  </div>
                </div>
                <hr>
                <div class="row">
                  <div class="col-md-12">
                    <label>Aplicar filtro:</label>
                    <button type='submit' style='width: 100%;' class='btn btn-outline-primary' ><i class="fas fa-search-dollar"></i> Filtrar por fechas seleccionadas </button>
                  </div>
                </div>

              </div>
            </div>
            
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal --> 
    </form>
    
    <section class="content">
      <div class="container-fluid">
        <div class="invoice p-2 mb-2">
          <div class="row">
            <div class="col-md-12 table-responsive"> 
              <table id="table_id" class="display table-striped">
                
                <thead>
                  <tr>
                    <th><h4><a class='badge badge-light'><i class="far fa-file-alt"></i> ID</a></h4></th>
                    <th><h4><a class='badge badge-light'><i class="fas fa-calendar-week"></i> Fecha</a></h4></th>
                    <th><h4><a class='badge badge-light'><i class="fas fa-user-tag"></i> Cliente</a></h4></th> 
                    <th><h4><a class='badge badge-light'><i class="fas fa-shopping-cart"></i> Productos</a></h4></th>     
                    <th><h4><a class='badge badge-light'><i class="fas fa-dollar-sign"></i> Monto</a></h4></th>
                    <th><h4><a class='badge badge-light'><i class="far fa-edit"></i> Estado</a></h4></th>   
                  </tr>
                </thead>
                
                <tbody>
                  <?php      
                    foreach ($this->Ventas_model->listar_ventas_finalizadas($desde,$hasta) as $row) 
                    {              
                      echo "<tr>";
                      
                        echo "<td>".$row->id_venta."</td>";
                        echo "<input type='hidden' class='id_venta' value='".$row->id_venta."'</input>";

                        echo "<td>".$row->fecha_venta."</td>";
                        echo "<input type='hidden' class='id_fecha_venta' value='".$row->fecha_venta."'</input>";

                        echo "<td><strong>".$this->Authme_model->mostrar_nombre_x_id($row->id_comprador)."</strong></td>";
                        echo "<input type='hidden' class='id_id_comprador' value='".$row->id_comprador."'</input>";
                 
                        $persona=$this->Authme_model->mostrar_persona($row->id_comprador);
                        echo "<input type='hidden' class='id_latitud' value='".$persona[0]->latitud."'</input>";
                        echo "<input type='hidden' class='id_longitud' value='".$persona[0]->longitud."'</input>";
                        echo "<input type='hidden' class='id_calle' value='".$persona[0]->calle_direccion."'</input>";
                        echo "<input type='hidden' class='id_numero' value='".$persona[0]->numero_direccion."'</input>";
                        echo "<input type='hidden' class='id_ciudad' value='".$persona[0]->ciudad."'</input>";
                        echo "<input type='hidden' class='id_provincia' value='".$persona[0]->provincia."'</input>";        
                        $telefono = "54".$this->Authme_model->mostrar_persona($row->id_comprador)[0]->area_telefono.$this->Authme_model->mostrar_persona($row->id_comprador)[0]->telefono;
                        echo "<input type='hidden' class='id_telefono' value='".$telefono."'</input>";

                        $carrito_model = $this->Ventas_model->mostrar_carrito($row->id_venta);

                        ?>

                        <td>
                          <a style='width: 100%;' class='verDescripcionCarrito btn btn-light'>
                            <strong>
                              <i class="fas fa-shopping-cart"></i> Ver carrito | 
                              <i class="far fa-file-alt"></i><?php echo $row->id_venta; ?>
                            </strong>
                          </a>      

                        <?php
                        
                          $carrito_model = $this->Ventas_model->mostrar_carrito($row->id_venta);                        
                          foreach ($carrito_model as $carrito)
                          {
                            $carrito_concat = "••• <strong>".$this->Ventas_model->mostrar_producto_por_id($carrito->id_producto)[0]->nombre_producto."</strong> ($".$carrito->precio_del_producto.") • Cantidad: <strong>".$carrito->cantidad_producto."</strong> • Precio total: <strong>$".($carrito->precio_del_producto*$carrito->cantidad_producto)."</strong> <br/> ";  
                            echo "<div><label style='display: none' class='id_carrito'>".$carrito_concat."</label></div>";
                          }
                        echo "</td>"; 

                        echo "<td><h3>$".$row->monto_final_cobrado."</h3></td>";
                        echo "<input type='hidden' class='id_monto_final_cobrado' value='".$row->monto_final_cobrado."'</input>";
                        
                        echo "<td class=''><button style='width: 100%;' class='btn btn-success'>Finalizado <i class='fas fa-clipboard-check'></i></button></td>";      
                              
                      echo "</tr>"; 
                    }; 
                  ?>
                  <tr class="text-center">
                    <td>
                      <label >
                        <img class="img-thumbnail" width="40" height="25" src="<?php echo base_url("/assets/logo_veida.png") ?>" alt="">
                      <label>
                    </td>
                    <td>
                      <h6>
                      <label>
                        <b><i class="fas fa-calendar-day"></i> entre las fechas:</b>
                       </label>
                       </h6> 
                    </td>
                    <td>
                      <h6>
                        <label>
                          <i class="far fa-calendar-alt"></i>
                          <?php
                            echo $desde." a las 00:00 hs";
                          ?>
                        </label>
                      </h6>
                    </td>
                    <td>
                      <h6>
                        <label>
                          <i class="fas fa-calendar-alt"></i>
                          <?php
                            echo $hasta." a las 23:59 hs";
                          ?>
                        </label>
                      </h6>
                    </td>

                    <td>
                      <h6>
                        <label>
                          <i class="fas fa-calculator"></i> Monto total:
                        </label>
                      </h6>
                    </td>
                    <td>
                      <h5>
                        <label>
                          <?php
                            echo "$".$monto_total_entre_fechas;
                          ?>
                        </label>
                      </h5>
                    </td>
                    
                  </tr>      
                </tbody>
              </table>            
            </div>
          </div>
        </div>
      </div>

      <div class="modal fade" id="modal_ver_mapa">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Ver ubicación <i class='fas fa-globe-africa'></i></h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            
            <div class="modal-body" id="modal_body_ver_mapa">
           
            </div>
            
            <div class="modal-footer justify-content">
              <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>            
            </div>
          </div>
        </div>
      </div>

    </section>

    <div class="modal fade" id="modal_ver_pedido">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">🔖 Ver pedido</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>

          <div class="modal-body" id="modal_body_ver_pedido">
         
          </div>

          <div>

            <div class="btn btn-info" style="width: 100%">
              🛒 Descripción
            </div>
            <label id='descrip_modal' class="text-left btn btn-light" style="width: 100%">
            </label> 

            <div class="btn btn-info" style="width: 100%">
              💲 Monto total
            </div>
            <input type="text" readonly id='monto_modal' class="btn btn-light" style="width: 100%">
            </input>
            
          </div>
          
          <div class="modal-footer justify-content">
            <button type="button" class="btn btn-default" style="width: 100%" data-dismiss="modal">Cerrar</button>            
          </div>
        </div>
      </div>
    </div>

  </div>
</div>

<script type="text/javascript">
  $(document).ready( function () 
  {
    
    $('#table_id').DataTable({
        pageLength: 999999999,
        order: [[ 1, 'desc' ]],

        language: {
        "decimal": "",
        "emptyTable": "No hay información",
        "info": "Mostrando _START_ a _END_ de un total de _TOTAL_ Entradas",
        "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
        "infoFiltered": "(Filtrado de _MAX_ total entradas)",
        "infoPostFix": "",
        "thousands": ",",
        "lengthMenu": "Mostrar _MENU_ Entradas",
        "loadingRecords": "Cargando...",
        "processing": "Procesando...",
        "search": "Buscar:",
        "zeroRecords": "Sin resultados encontrados",
        "paginate": {
          "first": "Primero",
          "last": "Ultimo",
          "next": "Siguiente",
          "previous": "Anterior"
        }
      },
      dom: 'Bfrtip',
        buttons: [
        
        {
          extend: 'excelHtml5',
          text: '<button class="btn btn-link"><i class="fas fa-file-excel"></i> Exportar a Excel</button>'
        },
        {
          extend: 'pdfHtml5',
          text: '<button class=" btn btn-link"><i class="fas fa-file-pdf"></i> Exportar en PDF</button>'
        }
      ]
    });
  });
</script>

<script>
    $("#table_id").on("click", ".verMapaGoogle", function(event)
    { 
      var tr = $(this).closest('tr');        
      var latitud = 0;
      var longitud = 0;
      latitud = $(tr).find('.id_latitud').val();
      longitud = $(tr).find('.id_longitud').val();

      var calle = "";
      var numero = "";
      var ciudad = "";
      var provincia = "";
      var pais = "Argentina";
      calle = $(tr).find('.id_calle').val();  
      numero = $(tr).find('.id_numero').val();
      ciudad = $(tr).find('.id_ciudad').val();
      provincia = $(tr).find('.id_provincia').val();

      var html = '';

      if (latitud != "" && longitud != "")
      {
        html += "<div id='map-container-google-1' class='z-depth-1-half map-container' style='width: 100%'><iframe width='100%' height='300' frameborder='0' scrolling='no' marginheight='0' marginwidth='0' src='https://maps.google.com/maps?width=100%25&amp;height=600&amp;hl=es&amp;q="+latitud+","+longitud+"%20+(Ubicacion)&amp;t=&amp;z=14&amp;ie=UTF8&amp;iwloc=B&amp;output=embed'></iframe></div>";
        $('#modal_body_ver_mapa').html(html);
        $('#modal_ver_mapa').modal('show');  
      }
      else 
      { 
        html += "<div id='map-container-google-1' class='z-depth-1-half map-container' style='width: 100%'><iframe width='100%' height='300' frameborder='0' scrolling='no' marginheight='0' marginwidth='0' src='https://maps.google.com/maps?width=100%25&amp;height=600&amp;hl=es&amp;q="+calle+",%20"+numero+"%20"+ciudad+"%20"+provincia+",%20"+pais+"+(Mi%20nombre%20de%20egocios)&amp;t=&amp;z=14&amp;ie=UTF8&amp;iwloc=B&amp;output=embed'></iframe></div>";
        $('#modal_body_ver_mapa').html(html);
        $('#modal_ver_mapa').modal('show'); 
      }
 
    });
</script>

<script>
  $("#table_id").on("click", ".verDescripcionCarrito", function(event)
  { 
    var tr = $(this).closest('tr');          
    var monto = $(tr).find('.id_monto_final_cobrado').val();
    document.getElementById("monto_modal").value="$"+monto;

    var carrito = $(tr).find('.id_carrito').text();
    document.getElementById("descrip_modal").innerHTML= carrito;

    var html = '';

    $('#modal_body_ver_pedido').html(html);
    $('#modal_ver_pedido').modal('show'); 
  });
</script>

