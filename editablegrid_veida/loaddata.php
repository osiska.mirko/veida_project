<?php     
  
require_once('config.php');      
require_once('EditableGrid.php');  
/**
 * fetch_pairs is a simple method that transforms a mysqli_result object in an array.
 * It will be used to generate possible values for some columns.
*/
function fetch_pairs($mysqli,$query){
	if (!($res = $mysqli->query($query)))return FALSE;
	$rows = array();
	while ($row = $res->fetch_assoc()) {
		$first = true;
		$key = $value = null;
		foreach ($row as $val) {
			if ($first) { $key = $val; $first = false; }
			else { $value = $val; break; } 
		}
		$rows[$key] = $value;
	}
	return $rows;
}

// Database connection
$mysqli = mysqli_init();
$mysqli->options(MYSQLI_OPT_CONNECT_TIMEOUT, 5);
$mysqli->real_connect($config['db_host'],$config['db_user'],$config['db_password'],$config['db_name']); 
                    
// create a new EditableGrid object
$grid = new EditableGrid();

/* 
*  Add columns. The first argument of addColumn is the name of the field in the databse. 
*  The second argument is the label that will be displayed in the header
*/
$grid->addColumn('id', '🔖 ID', 'float', NULL, false); 
$grid->addColumn('nombre_producto', '🛒 PRODUCTO', 'string');  

$grid->addColumn('costo', '📉 COSTO', 'float');    
$grid->addColumn('marcado', '<i class="fa fa-percent" aria-hidden="true"></i>', 'html', NULL); 
$grid->addColumn('ganancia', '📈 GANANCIA', 'float', NULL);   
$grid->addColumn('precio', '💲 PRECIO', 'float', NULL, false);                                                 

$grid->addColumn('stock', '📦 STOCK', 'html',NULL);
$grid->addColumn('action', '➕📦', 'html', NULL, false, 'id'); 

$grid->addColumn('descrip_producto', '💬 DESCRIPCIÓN', 'string');  
$grid->addColumn('categoria_producto', '📂 CATEGORÍA', 'string' , fetch_pairs($mysqli,'SELECT id_categoria, nombre_catego FROM categorias_de_productos'),true);                                               

$mydb_tablename = 'productos';

error_log(print_r($_GET,true));

$query = 'SELECT * FROM '.$mydb_tablename;
$queryCount = 'SELECT count(id) as nb FROM '.$mydb_tablename;

$totalUnfiltered =$mysqli->query($queryCount)->fetch_row()[0];
$total = $totalUnfiltered;

$page=0;
if ( isset($_GET['page']) && is_numeric($_GET['page'])  )
  $page =  (int) $_GET['page'];

$rowByPage=50;

$from= ($page-1) * $rowByPage;

if ( isset($_GET['filter']) && $_GET['filter'] != "" ) {
  $filter =  $_GET['filter'];
  $query .= '  WHERE nombre_producto like "%'.$filter.'%" OR descrip_producto like "%'.$filter.'%"';
  $queryCount .= '  WHERE nombre_producto like "%'.$filter.'%" OR descrip_producto like "%'.$filter.'%"';
  $total =$mysqli->query($queryCount)->fetch_row()[0];
}

if ( isset($_GET['sort']) && $_GET['sort'] != "" )
  $query .= " ORDER BY " . $_GET['sort'] . (  $_GET['asc'] == "0" ? " DESC " : "" );

$query .= " LIMIT ". $from. ", ". $rowByPage;

error_log("pageCount = " . ceil($total/$rowByPage));
error_log("total = " .$total);
error_log("totalUnfiltered = " .$totalUnfiltered);

$grid->setPaginator(ceil($total/$rowByPage), (int) $total, (int) $totalUnfiltered, null);

/* END SERVER SIDE */ 

error_log($query);
                                                                   
$result = $mysqli->query($query );

$mysqli->close();

$grid->renderJSON($result,false, false, !isset($_GET['data_only']));

