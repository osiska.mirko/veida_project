<?php
/*
 * 
 * http://editablegrid.net
 *
 * Copyright (c) 2011 Webismymind SPRL
 * Dual licensed under the MIT or GPL Version 2 licenses.
 * http://editablegrid.net/license
 */
      
require_once('config.php');         

// Database connection                                   
$mysqli = mysqli_init();
$mysqli->options(MYSQLI_OPT_CONNECT_TIMEOUT, 5);
$mysqli->real_connect($config['db_host'],$config['db_user'],$config['db_password'],$config['db_name']); 

// Get all parameter provided by the javascript
$id = $mysqli->real_escape_string(strip_tags($_POST['id']));
$stock = $mysqli->real_escape_string(strip_tags($_POST['stock']));
$tablename = "productos";

$stock_final = $stock;

$servername = $config['db_host'];
$username = $config['db_user'];
$password = $config['db_password'];
$dbname = $config['db_name'];

$conn = new mysqli($servername, $username, $password, $dbname);
$sql = "SELECT productos.stock from productos where productos.id = '$id'";
$result = $conn->query($sql);
$stock_result = $result->fetch_row();
$stock_actual = $stock_result[0];

$conn->close();

$stock_final = $stock_actual + $stock; 
// This very generic. So this script can be used to update several tables.
$return=false;
if ( $stmt = $mysqli->prepare("UPDATE productos SET stock = '$stock_final' WHERE id = ?")) {
	$stmt->bind_param("i", $id);
	$return = $stmt->execute();
	$stmt->close();
}             
$mysqli->close();        

echo $return ? "ok" : "error";

      

